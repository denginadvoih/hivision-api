/**
 * Created by AlexJ on 03.05.16.
 */
function load_child_select(child_id, input_name) {
    $.ajax({
            method: "GET",
            url: "/site-construction/ajax/references/items/" + child_id,
            dataType: 'json'
        }
    )
        .done(function (data) {
            var select = $('select[name="' + input_name + '"]');
            console.log(input_name);
            select.empty();
            select.append('<option value="">--</option>');
            for (var key in data) {
                select.append('<option value="' + data[key].id + '">' + data[key].title + '</option>');
            }
        });
}

function load_child_city(region_id) {
    var select = $('select[name="city"]');
    if (select) {
        $.ajax({
                method: "GET",
                url: "/ajax/cities/" + region_id,
                dataType: 'json'
            }
        )
            .done(function (data) {
                select.empty();
                select.append('<option value="">--</option>');
                for (var key in data) {
                    select.append('<option value="' + data[key].id + '">' + data[key].title + '</option>');
                }
            });
    }
}

function preventSelection(element){
    var preventSelection = false;

    function addHandler(element, event, handler){
        if (element.attachEvent)
            element.attachEvent('on' + event, handler);
        else
        if (element.addEventListener)
            element.addEventListener(event, handler, false);
    }
    function removeSelection(){
        if (window.getSelection) { window.getSelection().removeAllRanges(); }
        else if (document.selection && document.selection.clear)
            document.selection.clear();
    }
    function killCtrlA(event){
        var event = event || window.event;
        var sender = event.target || event.srcElement;
        if (sender.tagName.match(/INPUT|TEXTAREA/i))
            return;
        var key = event.keyCode || event.which;
        if (event.ctrlKey && key == 'A'.charCodeAt(0))
        {
            removeSelection();
            if (event.preventDefault)
                event.preventDefault();
            else
                event.returnValue = false;
        }
    }
    addHandler(element, 'mousemove', function(){
        if(preventSelection)
            removeSelection();
    });
    addHandler(element, 'mousedown', function(event){
        var event = event || window.event;
        var sender = event.target || event.srcElement;
        preventSelection = !sender.tagName.match(/INPUT|TEXTAREA/i);
    });
    addHandler(element, 'mouseup', function(){
        if (preventSelection)
            removeSelection();
        preventSelection = false;
    });
    addHandler(element, 'keydown', killCtrlA);
    addHandler(element, 'keyup', killCtrlA);
}

$(document).ready(function () {
    //запрет копирования
    //preventSelection(document);
    //document.ondragstart = test;
    //document.onselectstart = test;
    //document.oncontextmenu = test;
    function test() {
        return false
    }



    $('.toggle-mobile-hidden').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $(this).siblings('div').toggleClass('mobile-hidden');
        $('#screen-in-shadow').fadeToggle();
    });
    $('#mobile-menu-switcher').click(function (e) {
        $('.hmenu').toggleClass('open');
    });
    $('input[data-type="phone"]').mask("+7 (999) 999-9999", {placeholder: "+7 (___) ___-____"});

    $('a.upload-button').click(function (e) {
        e.preventDefault();
        $(this).closest('form').submit();
    });

    $('ul a.expand').click(function (e) {
        e.preventDefault();
        $(e.currentTarget).toggleClass('expanded');
        $(e.currentTarget).parents('li').find('ul').eq(0).toggleClass('hidden');
    });

    $('.tabs-block .tabs a').click(function (e) {
        e.preventDefault();
        var blockNumber = $(this).index();
        $(e.currentTarget).parents('.tabs-block').find('.tabs a').removeClass('active').eq(blockNumber).addClass('active');
        $(e.currentTarget).parents('.tabs-block').find('.tab-content').removeClass('active').eq(blockNumber).addClass('active');
    });
    $('.item-full .quantity-plus').click(function (e) {
        e.preventDefault();
        var siInput = $(this).parent().find('input').eq(0),
            siInputValue = parseInt($(siInput).val());
        siInputValue++;
        $(siInput).val(siInputValue);
    });
    $('.item-full .quantity-minus').click(function (e) {
        e.preventDefault();
        var siInput = $(this).parent().find('input').eq(0),
            siInputValue = parseInt($(siInput).val());
        siInputValue--;
        if (siInputValue>0) {
            $(siInput).val(siInputValue);
        }
    });
    $('.item a.readon.to-basket').click(function(e) {
        e.preventDefault();
        $('#basket').addClass('active');
        $.ajax({
            url: "/ajax/basket/" + $(e.currentTarget).data('id'),
            method: "POST",
            data: {quantity: 1},
            dataType: 'json'
        }).done(function (res) {
            setTimeout(function () {
                $('#basket').removeClass('active');
            }, 200);
            $('#basket .counter span').text(res.quantity_count);
            $('#basket .sum span').text(res.sum);
        });
    });

});