<?php //defined('SYSPATH') or die('No direct script access.');

class Amodules_form
{
    public static function tree_select($catalog_id = null, $parent_id = null) {
        $twig = Twig::get_instance();
        $catalog = Models_catalogue::full_catalogue_tree(1, null, 0);
        return $twig->template
            ->loadTemplate('construction/modules/catalogtreeselect.twig')
            ->render(array('tree' => $catalog, 'catalog_id' => $catalog_id, 'parent_id' => $parent_id));
    }

    public static function template_select($template) {
        $twig = Twig::get_instance();
        $templates = [];
        if ($handle = opendir('./templates')) {
            while (false !== ($file = readdir($handle))) {
                if (preg_match('/ss_.*(htm|html)$/', $file))
                    $templates[] = $file;
            }
            closedir($handle);
        }
        return $twig->template
            ->loadTemplate('construction/modules/templateselect.twig')
            ->render(array('templates' => $templates, 'current_template' => $template));
    }
}
