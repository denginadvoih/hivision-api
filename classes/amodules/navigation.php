<?php //defined('SYSPATH') or die('No direct script access.');

class Amodules_navigation
{
    public static function sysmenu()
    {
        $twig = Twig::get_instance();
        $sys_menu = array(
            array('title' => 'Опции сайта', 'url' => '/site-construction/settings', 'sub' => [
                ['title' => 'Опции сайта', 'url' => '/site-construction/settings'],
                ['title' => 'Сброс файловых превью', 'url' => '/site-construction/files/reset'],
                ['title' => 'Реиндексация каталога', 'url' => '/site-construction/staff_catalogue'],

            ]),
            [
                'title' => 'Поля',
                'url' => '/site-construction/fields',
                'sub' => [
                    ['title' => 'Поля', 'url' => '/site-construction/fields'],
                    ['title' => 'Новое поле', 'url' => '/site-construction/fields/add'],
                    'divider',
                    ['title' => 'Справочники', 'url' => '/site-construction/references'],
                    ['title' => 'Новый справочник', 'url' => '/site-construction/references/add'],
                    'divider',
                    ['title' => 'Элементы справочников', 'url' => '/site-construction/references/items'],
                    ['title' => 'Новый элемент', 'url' => '/site-construction/references/items/add']
                ]
            ],
            [
                'title' => 'Баннеры и рассылки',
                'url' => '/site-construction/adblock',
                'sub' => [
                    ['title' => 'Новый баннер', 'url' => '/site-construction/adblock/new'],
                    ['title' => 'Блоки/баннеры', 'url' => '/site-construction/adblocks'],
                    ['title' => 'Рассылки', 'url' => '/site-construction/mailings'],
                    ['title' => 'Новая рассылка', 'url' => '/site-construction/mailing/new']
                ]
            ],
            [
                'title' => 'Интернет магазин',
                'url' => '',
                'sub' => [
                    ['title' => 'Заказы', 'url' => '/site-construction/orders'],
                    ['title' => 'Покупатели', 'url' => '/site-construction/customers'],
                    ['title' => 'Способы доставки', 'url' => '/site-construction/shipping'],
                    ['title' => 'Новый способ доставки', 'url' => '/site-construction/shipping/new'],
                    ['title' => 'Массовое изменение цены', 'url' => '/site-construction/price'],
                    ['title' => 'Скидки', 'url' => '/site-construction/discount']
                ]
            ],
            [
                'title' => 'Содержимое',
                'url' => '',
                'sub' => [
                    ['title' => 'Список групп', 'url' => '/site-construction/groups'],
                    ['title' => 'Новая группа', 'url' => '/site-construction/groups/add'],
                    ['title' => 'Отзывы', 'url' => '/site-construction/reviews']
                ]
            ],
            array('title' => 'Техническая поддержка', 'url' => '/site-construction/tech-support'),
            array('title' => 'Перейти на сайт', 'url' => '/'),
            array('title' => 'Выход', 'url' => '/site-construction/logout')
        );
        return $twig->template
            ->loadTemplate('construction/modules/sysmenu.twig')
            ->render(array('sysmenu' => $sys_menu));

    }

    public static function catalog($catalog_id = null)
    {
        $twig = Twig::get_instance();
        if (isset($_SESSION['is_deleted']) && $_SESSION['is_deleted'] == 'show') {
            $catalog = Models_catalogue::full_catalogue_tree($catalog_id);
        } else {
            $catalog = Models_catalogue::full_catalogue_tree($catalog_id, null, 0);
        }
        return $twig->template
            ->loadTemplate('construction/modules/catalogtree.twig')
            ->render(array('level' => 0, 'tree' => $catalog, 'catalog_id' => $catalog_id));
    }

    /**
     * Вывод пэйджинатора
     */
    public static function paginator($pages_total, $current_page)
    {
        $request_comps = parse_url($_SERVER['REQUEST_URI']);
        $url = $request_comps['path'];

        $url_params = [];
        foreach ($_GET as $key => $value) {
            if ($value != '' && $key != 'route' && $key != 'page') {
                $url_params[] = $key . '=' . $value;
            }
        }
        $url_string = implode('&', $url_params);
        $twig = Twig::get_instance();
        return $twig->template
            ->loadTemplate('construction/modules/paginator.twig')
            ->render(['url' => $url,
                'url_string' => $url_string,
                'pages_count' => $pages_total,
                'current_page' => $current_page]);
    }
}
