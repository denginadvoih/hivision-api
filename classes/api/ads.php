<?php

class Api_Ads {

    /**
     * Информация о рекламном блоке
     * @param int $id
     * @return array|bool
     */
    public static function get_block($id)
    {
        $db = DataBase::getDB();
        $query = "select * from ad_blocks where id={$id}";
        return $db->selectRow($query);
    }

    /**
     * Список рекламных блоков (title и id)
     * @param int $id
     * @return array|bool
     */
    public static function get_blocks()
    {
        $db = DataBase::getDB();
        $query = "select * from ad_blocks";
        return $db->select($query);
    }


    /**
     * Содержимое рекламного блока
     * @param int $id
     * @return array|bool
     */
    public static function get_staff_by_block($id, $schemes = ['admin_prev'])
    {
        $db = DataBase::getDB();
        $query = "select * from ad_staff where block_id={$id} order by sorting";

        $staff = $db->select($query);
        $files_ids = Helpers_common::column_as_value($staff, 'file_id');
        if (!empty($files_ids)) {
            $fileModel = new Api_files();
            $files = $fileModel->get_files_by_ids($files_ids, $schemes);
            $files = Helpers_common::columnAsKey($files, 'id');
        }
        foreach ($staff as &$value) {
            if (isset($files[$value['file_id']])) {
                foreach ($schemes as $scheme) {
                    if (isset($files[$value['file_id']][$scheme])) {
                        $value[$scheme] = $files[$value['file_id']][$scheme];
                    }
                }
            }
        }
        return $staff;
    }

    /**
     * Получение отдельного баннера
     * @param int $id
     * @return array|bool
     */
    public static function get_staff_by_id($id, $schemes = ['admin_prev'])
    {
        $db = DataBase::getDB();
        $query = "select * from ad_staff where id={$id}";
        $staff = $db->selectRow($query);
        if (!empty($staff['file_id'])) {
            $fileModel = new Api_files();
            $files = $fileModel->get_files_by_ids([$staff['file_id']], $schemes);
        }
        if (!empty($files) && is_array($files)) {
            reset($files);
            $file = current($files);
            foreach ($schemes as $scheme) {
                if (isset($file[$scheme])) {
                    $staff[$scheme] = $file[$scheme];
                }
            }
        }
        return $staff;
    }

    /**
     * Рекламные блоки с содержим
     * @return array|bool
     */
    public static function get_blocks_with_staff($schemes = ['admin_prev'])
    {
        $db = DataBase::getDB();
        $query = "select * from ad_blocks";
        $ad_blocks = $db->select($query);
        $ad_blocks = Helpers_common::columnAsKey($ad_blocks, 'id');
        $query = "select * from ad_staff";
        $ad_staff = $db->select($query);
        foreach ($ad_staff as $value) {
            $ad_blocks[$value['block_id']]['staff'][] = $value;
        }
        return $ad_blocks;
    }

    /**
     * Получение баннера по id
     * @param int $id
     * @return array|bool
     */
    public static function get_ad_by_id($id)
    {
        $db = DataBase::getDB();
        $query = "select * from ad_staff where `id`={$id}";
        return $db->selectRow($query);
    }

    /**
     * Удаление баннера по id
     * @param int $id
     * @return array|bool
     */
    public static function delete($id)
    {
        $db = DataBase::getDB();
        return $db->query("delete from ad_staff where `id`={$id}");
    }
}