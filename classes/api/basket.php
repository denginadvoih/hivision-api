<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 08.02.15
 * Time: 0:20
 */
class Api_Basket
{
    public static function basket_state()
    {
        $auth = Auth_auth::get_instance();
        $user = $auth->getUser();
        $db = DataBase::getDB();
        $query = "
        SELECT
            IFNULL(sum(`b`.`quantity` * `s`.`price`), 0) AS `sum`,
            count(*) AS `count`,
            sum(`b`.`quantity`) AS `quantity_count`
        FROM `basket` AS `b`
        LEFT JOIN `staff` AS `s` ON (`s`.`id` = `b`.`staff_id`)
        WHERE `user_id` = {?} AND `user_type` = {?}
            ";
        return $db->selectRow($query, [$user['id'], $user['type']]);
    }

    public static function current_customer()
    {
        $auth = Auth_auth::get_instance();
        $user = $auth->getUser();
        $db = DataBase::getDB();
        $query = "SELECT * FROM basket WHERE `user_id` = " . $user['id'] . " AND `user_type` = '" . $user['type'] . "'";
        $bstaff = $db->select($query);
        $ids = Helpers_common::column_as_value($bstaff, 'staff_id');
        $staff = Api_staff::get_staff_by_ids($ids);
        $staff = Helpers_common::columnAsKey($staff, 'id');
        foreach ($bstaff as &$bitem) {
            if (isset($staff[$bitem['staff_id']])) {
                $bitem['staff_item'] = $staff[$bitem['staff_id']];
            }
        }
        return $bstaff;
    }

    /**
     * @param $user
     * @param $params
     */
    public static function create_order($user, $shippingId, $params)
    {
        $db = DataBase::getDB();
        //получаем метод доставки
        $shipping = Api_Shipping::get_by_id($shippingId);
        //получаем текущие покупки из корзины
        $query = "SELECT * FROM basket WHERE `user_id` = " . $user['id'] . " AND `user_type` = '" . $user['type'] . "'";
        $bstaff = $db->select($query);
        $ids = Helpers_common::column_as_value($bstaff, 'staff_id');
        $staff = Api_staff::get_staff_by_ids($ids);
        $staff = Helpers_common::columnAsKey($staff, 'id');
        $sum = $discountSum = 0;
        $discount = Models_shop::discount();
        $params['discount'] = $discount;
        if (empty($discount['user']) && empty($discount['global']) && empty($discount['size'])) {
            $currentDiscount = ['type' => 'none', 'value' => 0];
        } else {
            $currentDiscount = $discount['user'] > $discount['global']
                ? ['type' => 'user', 'value' => $discount['user']]
                : ['type' => 'global', 'value' => $discount['global']];
        }
        //высчитываем сумму заказа и прикрепляем объекты
        foreach ($bstaff as &$bitem) {
            if (isset($staff[$bitem['staff_id']])) {
                $bitem['staff_item'] = $staff[$bitem['staff_id']];
                $sum += $bitem['quantity'] * $bitem['staff_item']['price'];
            }
        }
        unset($bitem);
        //вычисляем возможную скидку по размеру
        if (is_array($discount['size'])) {
            foreach ($discount['size'] as $d_value) {
                if ($sum > $d_value['sum']) {
                    $_discount_size = $d_value['value'];
                }
            }
        }
        if ($_discount_size > $currentDiscount['value']) {
            $currentDiscount = ['type' => 'size', 'value' => $_discount_size];
        }
        $params['current_discount'] = $currentDiscount;
        $params['sum'] = $sum;
        $params['discountSum'] = 0;
        if ($currentDiscount['type'] == 'size') {
            $params['discountSum'] = $params['sum'] - ($params['sum'] / 100 * $params['current_discount']['value']);
        } else {
            foreach ($bstaff as $bitem) {
                if ($bitem['staff_item']) {
                    if ($bitem['staff_item']['price'] < $bitem['staff_item']['old_price']) {
                        $params['discountSum'] += $bitem['quantity'] * $bitem['staff_item']['price'];
                    } else {
                        $params['discountSum'] += $bitem['quantity'] * $bitem['staff_item']['price'] - ($bitem['quantity'] * $bitem['staff_item']['price'] / 100 * $currentDiscount['value']);
                    }
                }
            }
        }

        //создаем заказ
        $query = "INSERT INTO orders (`date`, `user_id`, `user_type`, `info`, `shipping`, `status`) VALUES({?}, {?}, {?}, {?}, {?}, {?})";
        $order_id = $db->query($query, [date('Y-m-d H:i:s', time()), $user['id'], $user['type'], json_encode($params), $shippingId, 'unchecked']);
        //вставляем покупки в таблицу заказов
        foreach ($bstaff as $bitem) {
            $query = "INSERT INTO orders_items
            (`order_id`, `staff_id`, `stock_id`, `quantity`, `price`, `old_price`)
            VALUES ({?}, {?}, {?}, {?}, {?}, {?})";
            $db->query($query, [$order_id, $bitem['staff_id'], $bitem['stock_id'], $bitem['quantity'], $bitem['staff_item']['price'], $bitem['staff_item']['old_price']]);
        }
        //очищаем таблицу с покупками
        $query = "DELETE FROM `basket`  WHERE `user_id` = " . $user['id'] . " AND `user_type` = '" . $user['type'] . "'";
        $db->query($query);
        return $order_id;
    }
}