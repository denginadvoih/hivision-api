<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 08.02.15
 * Time: 0:20
 */
class Api_files
{
    public $path = 'staff_files';
    public $trash_path = 'removed_files';
    public $allowed_types = ['image' => ['png', 'gif', 'jpg', 'jpeg'], 'other' => ['psd', 'doc', 'xls', 'docx', 'pdf']];
    public $schemes = [
        'admin_prev' => ['img' => 'jpg', 'width' => 196, 'height' => 286, 'type' => 'nofull', 'logo' => false],
        'staff_prev' => ['img' => 'jpg', 'width' => 290, 'height' => 220, 'type' => 'crop', 'logo' => false],
        'staff_prev_4-3' => ['img' => 'jpg', 'width' => 222, 'height' => 154, 'type' => 'crop', 'logo' => false],
        'staff_prev_png' => ['img' => 'png', 'width' => 220, 'height' => 196, 'type' => 'full', 'logo' => false],
        'staff_prev_squad' => ['img' => 'png', 'width' => 300, 'height' => 300, 'type' => 'crop', 'logo' => false],
        'staff_medium' => ['img' => 'jpg', 'width' => 762, 'height' => 1100, 'type' => 'crop', 'logo' => false],
        'gallery_main' => ['img' => 'jpg', 'width' => 684, 'height' => 192, 'type' => 'crop', 'logo' => false],
        'rectangle_medium' => ['img' => 'jpg', 'width' => 596, 'height' => 596, 'type' => 'nofull', 'logo' => false],
        'rectangle_medium_crop' => ['img' => 'jpg', 'width' => 596, 'height' => 596, 'type' => 'crop', 'logo' => false],
        'rectangle_medium_16_9_crop' => ['img' => 'jpg', 'width' => 760, 'height' => 427, 'type' => 'crop', 'logo' => false],
        'rectangle_large_16_9_crop' => ['img' => 'jpg', 'width' => 1520, 'height' => 855, 'type' => 'crop', 'logo' => false],
        'rectangle_16_9_crop' => ['img' => 'jpg', 'width' => 760, 'height' => 427, 'type' => 'crop', 'logo' => false],
        'rectangle_long' => ['img' => 'jpg', 'width' => 684, 'height' => 192, 'type' => 'crop', 'logo' => false],
        'staff_a4' => ['img' => 'jpg', 'width' => 210, 'height' => 297, 'type' => 'crop', 'logo' => false],
        'catalogue_logo' => ['img' => 'jpg', 'width' => 290, 'height' => 240, 'type' => 'crop', 'logo' => false],
        'catalogue_header' => ['img' => 'jpg', 'width' => 1530, 'height' => 390, 'type' => 'nofull', 'logo' => false],
        'trash_origin' => ['img' => 'jpg', 'width' => 100, 'height' => 100, 'type' => 'nofull', 'logo' => false]
    ];

    /**
     * Выборка файлов из объектов каталога со сдвигом и размером выборки
     * @param $catalog_id
     * @param $offset
     * @param $limit
     * @return array|bool
     */
    public static function get_staff_files($catalog_id, $offset, $limit)
    {
        $db = DataBase::getDB();
        $query = "
        select
            `ss`.`sspicname1` as `file`
        from staff as ss
        where `ss`.`sspicname1`!='' and `ss`.`parent_id`={$catalog_id}
        order by `ss`.`sorting` desc
        limit {$offset}, {$limit}
        ";
        return $db->select($query);
    }

    /**
     * Получение файлов по $id
     */
    public function get_files_by_ids($ids, $schemes = ['admin_prev'])
    {
        $ids = (array)$ids;
        $ids = array_filter($ids, 'is_numeric');
        if (empty($ids)) {
            return [];
        }
        $db = DataBase::getDB();
        $query = "
        select * from `files`
        where `id` in (" . implode(',', $ids) . ")
        ";
        $files = $db->select($query);
        foreach ($files as &$value) {
            if (!is_array($schemes)) {
                $schemes = [$schemes];
            }
            foreach ($schemes as $scheme) {

                $value[$scheme] = $this->get(
                    $value['path'] . '/' . $value['title'],
                    $scheme
                );

            }
        }
        return $files;
    }

    /**
     * генерит изображения по выбранным id
     * @param $ids
     * @param $scheme
     */
    public function get_staff_files_by_ids($ids, $is_main, $schemes)
    {
        if (empty($ids)) {
            return [];
        }
        $db = DataBase::getDB();
        $query = "
        select
            `f`.*,
            `sf`.*
        from `files` as `f`
        left join `staff_files` as `sf` on `sf`.`file_id` = `f`.`id`
        where " . ($is_main ? "`sf`.`is_main` = 1 and " : '') . "`sf`.`file_id` in (select `file_id` from `staff_files` where `staff_id` in (" . implode(',', $ids) . "))";
        $files = $db->select($query);
        foreach ($files as &$value) {
            if (!is_array($schemes)) {
                $schemes = [$schemes];
            }
            foreach ($schemes as $scheme) {
                $value[$scheme] = $this->get(
                    $value['path'] . '/' . $value['title'],
                    $scheme
                );
            }

        }
        return $files;
    }

    function getByScheme($file, $scheme)
    {
        return $this->get($file, $scheme, $this->schemes[$scheme]['width'], $this->schemes[$scheme]['height'], $this->schemes[$scheme]['type'], 'no');
    }

    function get($file, $schemeTitle)
    {
        $path = DOCROOT . $file;
        if (!file_exists(DOCROOT . $file)) {
            return '/assets/images/nofile.png';
        }
        $pathToScheme = DOCROOT . '/' . preg_replace('/\.[^\.]+$/Uis', '_' . $schemeTitle . '.' . $this->schemes[$schemeTitle]['img'], $file);
        $urlPathToScheme = '/' . preg_replace('/\.[^\.]+$/Uis', '_' . $schemeTitle . '.' . $this->schemes[$schemeTitle]['img'], $file);
        if (!file_exists($pathToScheme)) {
            $t = getimagesize($path);
            if (!is_array($t)) {
                return '/assets/images/nofile.png';
            }
            $with = $t[0];
            $height = $t[1];
            if ($this->schemes[$schemeTitle]['logo']) {
                $logosize = getimagesize('./assets/images/watermark.png');
                $logo = imagecreatefrompng("./assets/images/watermark.png");
            }
            if ($this->schemes[$schemeTitle]['type'] == 'noresize') {
                $x = $with;
                $y = $height;
            } else {
                $x = $this->schemes[$schemeTitle]['width'];
                $y = $this->schemes[$schemeTitle]['height'];
            }
            if ($height < $y) {
                $need_y = $height;
                $need_x = $need_y * ($with / $height);
            } else {
                $need_y = $y;
                $need_x = $need_y * ($with / $height);
            }
            if ($need_x > $x) {
                $need_x = $x;
                $need_y = $need_x * ($height / $with);
            }
            $need_x = $need_x - 2;
            $need_y = $need_y - 2;
            switch ($t[2]) {
                case 1:
                    $img = imagecreatefromgif($path);
                    break;
                case 2:
                    $img = imagecreatefromjpeg($path);
                    break;
                case 3:
                    $img = imagecreatefrompng($path);
                    break;
            }
            if ($y == 0) {
                $y = $x * ($height / $with);
            }
            if ($this->schemes[$schemeTitle]['type'] == 'crop') {
                if (($x / $y) < ($with / $height)) {
                    $need_y = $height;
                    $need_x = $height * ($x / $y);
                } else {
                    $need_x = $with;
                    $need_y = $with * ($y / $x);
                }
                $thumb = imagecreatetruecolor($x, $y);
                //заливаем белым цветом, если не png
                if ($this->schemes[$schemeTitle]['img'] !== 'png') {
                    $white = imagecolorallocate($thumb, 255, 255, 255);
                    imagefill($thumb, 0, 0, $white);
                } else {
                    $transparent = imagecolorallocatealpha($thumb, 0, 0, 0, 127);
                    imagefill($thumb, 0, 0, $transparent);
                    imagesavealpha($thumb, true); // save alphablending setting (important);
                }
                $pos_x = round(($with - $need_x) / 2);
                $pos_y = round(($height - $need_y) / 2);
                imagecopyresampled($thumb, $img, 0, 0, $pos_x, $pos_y, $x, $y, round($need_x), round($need_y));
                if ($this->schemes[$schemeTitle]['logo']) {
                    imagecopyresampled($thumb, $logo, 5, 5, 0, 0, $logosize[0], $logosize[1], $logosize[0], $logosize[1]);
                }
            }

            if ($this->schemes[$schemeTitle]['type'] == 'full'
                or $this->schemes[$schemeTitle]['type'] == 'noresize' or
                $this->schemes[$schemeTitle]['type'] == 'nofull'
            ) {
                if ($this->schemes[$schemeTitle]['type'] == 'full' or $this->schemes[$schemeTitle]['type'] == 'noresize') {
                    $thumb = imagecreatetruecolor($x, $y);
                    $pos_x = round(($x - $need_x) / 2);
                    $pos_y = round(($y - $need_y) / 2);

                }
                if ($this->schemes[$schemeTitle]['type'] == 'nofull') {
                    $thumb = imagecreatetruecolor(ceil($need_x), ceil($need_y));
                    $pos_x = 0;
                    $pos_y = 0;
                }

                //можно выяснить средний цвет картинки для заливки
                //$color = imagecolorat($img, 1, 1);
                //$needcolor = imagecolorallocate($thumb, $color['red'], $color['blue'], $color['green']);
                //заливаем белым цветом, если не png
                if ($this->schemes[$schemeTitle]['img'] !== 'png') {
                    $white = imagecolorallocate($thumb, 255, 255, 255);
                    imagefill($thumb, 0, 0, $white);
                } else {
                    $transparent = imagecolorallocatealpha($thumb, 0, 0, 0, 127);
                    imagefill($thumb, 0, 0, $transparent);
                    imagesavealpha($thumb, true); // save alphablending setting (important);
                }
                imagecopyresampled($thumb, $img, $pos_x, $pos_y, 0, 0, $need_x, $need_y, $with, $height);
                //imagerectangle($thumb, 0, 0, $x - 1, $y - 1, $white);
                //imagerectangle($thumb, 1, 1, $x - 2, $y - 2, $white);
                if ($this->schemes[$schemeTitle]['logo']) {
                    imagecopyresampled($thumb, $logo, 0, 0, 0, 0, $need_x, $need_y, $logosize[0], $logosize[1]);
                }
            }

            imagepng($thumb, DOCROOT . "staff_files/buffer.png");
            imagejpeg($thumb, DOCROOT . "staff_files/buffer.jpg", 100);
            if ($this->schemes[$schemeTitle]['img'] == 'png') {
                copy(DOCROOT . "staff_files/buffer.png", $pathToScheme);
            } else {
                copy(DOCROOT . "staff_files/buffer.jpg", $pathToScheme);
            }
            unlink(DOCROOT . "staff_files/buffer.png");
            unlink(DOCROOT . "staff_files/buffer.jpg");
        }
        return $urlPathToScheme;
    }

    /**
     * полное удаление файла из базы
     * @param $id
     */
    public function delete($id)
    {
        if (!empty($id)) {
            $db = DataBase::getDB();
            $file = $db->selectRow("select * from files where `id` = {?}", [$id]);
            if (!empty($file)) {
                if ($file['type'] == 'image') {
                    foreach ($this->schemes as $scheme => $value) {
                        $pathToScheme = DOCROOT . $file['path'] . '/' . preg_replace('/\.[^\.]+$/Uis', '_' . $scheme . '.jpg', $file['title']);
                        if (file_exists($pathToScheme)) {
                            unlink($pathToScheme);
                        }
                    }
                }
                $pathToFile = DOCROOT . $file['path'] . '/' . $file['title'];
                if (file_exists($pathToFile)) {
                    unlink($pathToFile);
                }
            }

            $query = "delete from `files` where `id` = {?}";
            $db->query($query, [$id]);
            $query = "delete from `staff_files` where `file_id` = {?}";
            $db->query($query, [$id]);
        }
    }

    public function reset_schemes()
    {
        foreach ($this->schemes as $scheme => $value) {
            $path = DOCROOT . '/staff_files/*_' . $scheme . '.jpg';
            $files = glob($path);
            array_map('unlink', $files);
        }
    }

    public function upload_that_file($file_path)
    {
        $insertId = '';
        $http_path = '';
        $error = '';
        $preview = '';
        $file_type = null;
        $file_parts = pathinfo($file_path);

        /*echo $path_parts['dirname'], "\n";
        echo $path_parts['basename'], "\n";
        echo $path_parts['extension'], "\n";
        echo $path_parts['filename'], "\n"; // PHP 5.2.0'dan beri.*/

        $file_name = Helpers_common::getSafeFileName($file_parts['basename']);
        $file_res = $file_parts['extension'];
        foreach ($this->allowed_types as $allowed_type => $sub_types) {
            if (in_array($file_res, $sub_types)) {
                $file_type = $allowed_type;
                break;
            }
        }
        if ($file_type) {
            $full_path = './' . $this->path . '/' . $file_name;
            $http_path = '/' . $this->path . '/' . $file_name; // адрес изображения для обращения через http
            if (copy($file_path, $full_path)) {
                $db = DataBase::getDB();
                $query = "insert into `files` (`title`, `path`, `type`) values ({?}, '{$this->path}', '{$file_type}')";
                $insertId = $db->query($query, [$file_name]);
                //генерим превьювшу для имаджа
                if ($file_type == 'image') {
                    $file_model = new Api_files();
                    $preview = $file_model->getByScheme($this->path . '/' . $file_name, 'admin_prev');
                } else {
                    $preview = '/assets/images/spacer.gif';
                }
            } else {
                $error = 'Не удалось загрузить файл';
            }
        } else {
            $error = 'Не удалось загрузить файл';
        }
        return ['id' => $insertId, 'type' => $file_type, 'path' => $http_path, 'preview' => $preview, 'error' => $error];
    }

    public function to_be_removed($ids)
    {
        if (!empty($ids)) {
            $db = DataBase::getDB();
            foreach ($ids as $id) {
                $file = $db->selectRow("select * from files where `id` = {?}", [$id]);
                if (!empty($file)) {
                    if ($file['type'] == 'image') {
                        foreach ($this->schemes as $scheme => $value) {
                            $pathToScheme = DOCROOT . $file['path'] . '/' . preg_replace('/\.[^\.]+$/Uis', '_' . $scheme . '.jpg', $file['title']);
                            if (file_exists($pathToScheme)) {
                                unlink($pathToScheme);
                            }
                        }
                    }
                    $pathToFile = DOCROOT . $file['path'] . '/' . $file['title'];
                    if (file_exists($pathToFile)) {
                        $trash = $this->get($file['path'] . '/' . $file['title'], 'trash_origin');
                        copy($pathToFile, DOCROOT . $this->trash_path . '/' . $file['title']);
                        unlink($pathToFile);
                        rename(DOCROOT . $trash, $pathToFile);
                    }
                }
            }
        }
    }


} 