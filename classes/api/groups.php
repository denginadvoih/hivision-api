<?php

class Api_Groups {

    /**
     * Список групп
     * @return array|bool
     */
    public static function get()
    {
        $db = DataBase::getDB();
        $query = "select * from groups";
        return $db->select($query);
    }

    /**
     * Группа по id
     * @param $id int
     * @return array|bool
     */
    public static function get_by_id($id)
    {
        $db = DataBase::getDB();
        $query = "select * from `groups` where `id` = " . (int)$id;
        return $db->selectRow($query);
    }

    /**
     * Группа по label
     * @param $label string
     * @return array|bool
     */
    public static function get_by_label($label)
    {
        $db = DataBase::getDB();
        $query = "select * from `groups` where `label` = {?}";
        return $db->selectRow($query, [$label]);
    }


    /**
     * Удаление баннера по id
     * @param int $id
     * @return array|bool
     */
    public static function delete($id)
    {
        $db = DataBase::getDB();
        return $db->query("update `groups` set `is_deleted` = 1 where `id` = ".(int)$id);
    }

    /**
     * @param $params
     * @return int|bool
     */
    public static function create($params)
    {
        $db = DataBase::getDB();
        return $db->query("insert into `groups` (`title`, `label`, `description`) values ({?}, {?}, {?})",
        [$params['title'], $params['label'], $params['description']]
        );
    }

    /**
     * @param $params
     * @return int|bool
     */
    public static function update($params)
    {
        $db = DataBase::getDB();
        return $db->query("update `groups` set `title` = {?}, `label` = {?}, `description` = {?} where `id` = {?}",
            [$params['title'], $params['label'], $params['description'], $params['id']]
        );
    }
}