<?php
/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 07.02.15
 * Time: 17:21
 */

class Api_option {

    /*
     * Получение всех опций
     */
    public static function get() {
        $db = DataBase::getDB();
        $query = 'select * from `siteoptions` as `options`';
        return $db->select($query);
    }

    /*
     * Получение опции по метке
     * @param string $tag
     * @return Array
     */
    public static function get_by_tag($tag, $value = null) {
        $db = DataBase::getDB();

        $query = '
        select
            `options`.`info` as `info`,
            `options`.`content` as `content`
        from `siteoptions` as `options`
        where label={?}';
        $result = $db->selectRow($query, [$tag]);
        if (empty($result)) {
            $result = $value;
        }
        return $result;
    }

    public static function update($tag, $value)
    {
        $db = DataBase::getDB();
        $option = self::get_by_tag($tag, false);
        if ($option) {
            $query = "update `siteoptions` set `content`={?} where `label`={?}";
        } else {
            $query = "insert into `siteoptions` (`content`, `label`) values ({?}, {?})";
        }
        return $db->query($query, [$value, $tag]);
    }
} 