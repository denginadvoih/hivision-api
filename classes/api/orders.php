<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 08.02.15
 * Time: 0:20
 */
class Api_Orders
{
    public static $status_sorting = ['unchecked', 'checked', 'payed', 'completed'];
    public static $status_message = [
        'checked' => 'Ваш заказ обработан',
        'payed' => 'Ваш заказ оплачен',
        'completed' => 'Ваш заказ выполнен'
    ];

    public static function get($params, $offset = 0, $limit = 1)
    {
        $db = DataBase::getDB();
        $orders = $staff = [];
        //обрабатываем параметры
        $where = [];
        if (!empty($params['status'])) {
            $where[] = '`o`.`status` = ' . "'" . $params['status'] . "'";
        }
        if (!empty($params['user_id'])) {
            $where[] = '`o`.`user_id` = ' . $params['user_id'] . " and `o`.`user_type` = 'registered'";
        }

        if (!empty($params['id'])) {
            $where[] = '`o`.`id` = ' . (int)$params['id'];
        }

        if (!$params['show_is_deleted']) {
            $where[] = '`o`.`is_deleted` = 0';
        }
        //вычисляем общее количество заказов по заданным параметрам
        $query = "
        select
            count(*) as `count`
        from `orders` as `o`
        " . (!empty($where) ? 'where ' . implode(' and ', $where) : '') . "
        ";
        $total = $db->selectCell($query);
        if (!empty($total)) {
            //вычисляем заказы
            $query = "select `id` from `orders` as `o` " . (!empty($where) ? 'where ' . implode(' and ', $where) : '') . " order by `date` desc limit {$offset}, {$limit}";
            $ids = Helpers_common::column_as_value($db->select($query), 'id');
            $query = "
        select
            `o`.*,
            `oi`.`id` as `item_id`,
            `oi`.`staff_id`,
            `oi`.`stock_id`,
            `oi`.`quantity`,
            `oi`.`price`,
            `oi`.`old_price`
        from `orders` as `o`
        left join `orders_items` as `oi` on (`o`.`id` = `oi`.`order_id`)
        where `o`.`id` in (" . implode(',', $ids) . ")
        ";
            $res = $db->select($query);
            //обрабатываем полученный результат
            $shipping = Api_Shipping::get();
            $shipping = Helpers_common::columnAsKey($shipping, 'id');
            $orders = [];
            $staff_ids = [];
            foreach ($res as $v) {
                if (!isset($orders[$v['id']])) {
                    $orders[$v['id']] = Helpers_arr::extract($v, [
                        'id',
                        'user_id',
                        'user_type',
                        'info',
                        'status',
                        'shipping',
                        'post_id',
                        'date'
                    ]);
                    $orders[$v['id']]['formatedDate'] = Helpers_common::convert_date($orders[$v['id']]['date']);
                    $orders[$v['id']]['shipping'] = [
                        'id' => $shipping[$orders[$v['id']]['shipping']]['id'],
                        'title' => $shipping[$orders[$v['id']]['shipping']]['title'],
                        'descripton' => $shipping[$orders[$v['id']]['shipping']]['descriptio'],
                        'mail_message' => $shipping[$orders[$v['id']]['shipping']]['mail_message'],
                        'price' => $shipping[$orders[$v['id']]['shipping']]['price']
                    ];
                    $orders[$v['id']]['info'] = json_decode($orders[$v['id']]['info'], true);
                    $orders[$v['id']]['staff'] = [];
                }
                if (!isset($orders[$v['id']]['staff'][$v['item_id']])) {
                    $staff_ids[] = $v['staff_id'];
                    $orders[$v['id']]['staff'][$v['item_id']] = Helpers_arr::extract($v, [
                        'item_id',
                        'staff_id',
                        'stock_id',
                        'quantity',
                        'price',
                        'old_price'
                    ]);
                }
            }
            
            $staff = Api_staff::get_staff_by_ids($staff_ids);
            foreach ($orders as &$order) {
                $order['sum'] = 0;
                foreach ($order['staff'] as $item) {
                    if (isset($staff[$item['staff_id']])) {
                        $order['sum'] += $staff[$item['staff_id']]['price'] * $item['quantity'];
                    }
                }
            }
        }

        return ['orders' => $orders, 'staff' => $staff, 'total' => $total];
    }

    public static function change_status($id, $status)
    {
        $db = DataBase::getDB();
        $allowed_status = array_slice(self::$status_sorting, 0, array_search($status, self::$status_sorting));
        if (empty($allowed_status)) {
            return ['id' => $id, 'message' => 'not changed, not allowed status'];
        }
        $query = "select * from `orders` where `id` = {?} and `status` in (" . implode(',', array_map(function ($status) {
                return "'" . $status . "'";
            }, $allowed_status)) . ")";
        $order = $db->selectRow($query, [$id]);
        if (empty($order)) {
            return ['id' => $id, 'message' => 'not changed, not found'];
        }
        $order = self::get(['id' => $id]);
        $twig = Twig::get_instance();
        $message = $twig->template
            ->loadTemplate('orders/order_is_' . $status . '.twig')
            ->render([
                'order' => $order['orders'][$id],
                'staff' => $order['staff'],
                'domain' => SITE_NAME
            ]);
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=utf-8\r\n";
        $headers .= "From: " . SITE_NAME . "<mail-service@" . SITE_NAME . ">\r\n";
        $subject = 'Покупки на сайте ' . SITE_NAME . '. ' . self::$status_message[$status];
        mail($order['orders'][$id]['info']['email'], $subject, $message, $headers);
        $query = "update `orders` set `status` = {?} where `id` = {?}";
        $db->query($query, [$status, $id]);
        return ['id' => $id, 'message' => 'changed'];
    }
    
    public static function delete($order_id)
    {
        $db = DataBase::getDB();
        $query = "update `orders` set `is_deleted`=1 where `id`={?}";
        return $db->query($query, [$order_id]);
    }

    public static function update_post_id($order_id, $post_id)
    {
        $db = DataBase::getDB();
        $query = "update `orders` set `post_id`={?} where `id`={?}";
        return $db->query($query, [$post_id, $order_id]);
    }

    public static function send_post_id($order_id, $post_id)
    {
        $order = self::get(['id' => $order_id]);
        $twig = Twig::get_instance();
        $message = $twig->template
            ->loadTemplate('orders/order_post_id.twig')
            ->render([
                'post_id' => $post_id,
                'order' => $order['orders'][$order_id],
                'staff' => $order['staff'],
                'domain' => SITE_NAME
            ]);
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=utf-8\r\n";
        $headers .= "From: " . SITE_NAME . "<mail-service@" . SITE_NAME . ">\r\n";
        $subject = 'Номер почтового отправления вашего заказа на сайте ' . SITE_NAME;
        mail($order['orders'][$order_id]['info']['email'], $subject, $message, $headers);
    }

    /**
     * Отправка письма клиенту
     */
    public static function send_customer_mail($id)
    {
        $twig = Twig::get_instance();
        $orderRes = Api_Orders::get(['id' => $id]);
        if (isset($orderRes['orders'][$id])) {
            $order = $orderRes['orders'][$id];
            $ids = Helpers_common::column_as_value($order['staff'], 'staff_id');
            $staff = Api_staff::get_staff_by_ids($ids);
            $staff = Helpers_common::columnAsKey($staff, 'id');
            $order['staff_info'] = $staff;
            $message = $twig->template
                ->loadTemplate('basket/send_order.twig')
                ->render([
                    'order' => $order,
                    'domain' => SITE_NAME
                ]);
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=utf-8\r\n";
            $headers .= "From: " . SITE_NAME . "<mail-service@" . SITE_NAME . ">\r\n";
            $subject = 'Ваш заказ на сайте ' . SITE_NAME;
            mail($order['info']['email'], $subject, $message, $headers);
        }
    }

    /**
     * Отправка письма авдминистратору
     */
    public static function send_admin_mail($id)
    {
        $twig = Twig::get_instance();
        $orderRes = Api_Orders::get(['id' => $id]);
        if (isset($orderRes['orders'][$id])) {
            $order = $orderRes['orders'][$id];
            $ids = Helpers_common::column_as_value($order['staff'], 'staff_id');
            $staff = Api_staff::get_staff_by_ids($ids);
            $staff = Helpers_common::columnAsKey($staff, 'id');
            $order['staff_info'] = $staff;
            $message = $twig->template
                ->loadTemplate('basket/send_order_administrator.twig')
                ->render([
                    'order' => $order,
                    'domain' => SITE_NAME
                ]);
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=utf-8\r\n";
            $headers .= "From: " . SITE_NAME . "<mail-service@" . SITE_NAME . ">\r\n";
            $subject = 'Новый заказ на сайте ' . SITE_NAME;
            $admin_email = Api_option::get_by_tag('email');
            mail(trim($admin_email['content']), $subject, $message, $headers);
        }
    }
}