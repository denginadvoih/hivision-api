<?php
/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 07.02.15
 * Time: 17:21
 */

class Api_regions
{

    /*
     * Получение всех регионов
     */
    public static function get_regions()
    {
        $db = DataBase::getDB();
        $query = "select * from `regions` where `type`='region'";
        return Helpers_common::columnAsKey($db->select($query), 'id');
    }

    /*
    * Получение региона
    */
    public static function get_region($id)
    {
        $db = DataBase::getDB();
        $query = "select * from `regions` where `type`='region' and `id`={?}";
        return $db->selectRow($query, [$id]);
    }

    /*
    * Получение городов
    */
    public static function get_cities()
    {
        $db = DataBase::getDB();
        $query = "select * from `regions` where `type`='city'";
        return Helpers_common::columnAsKey($db->select($query), 'id');
    }

    /*
    * Получение региона
    */
    public static function get_city($id)
    {
        $db = DataBase::getDB();
        $query = "select * from `regions` where `type`='city' and `id`={?}";
        return $db->selectRow($query, [$id]);
    }

    /*
    * Получение городов по региону
    */
    public static function get_cities_by_region_id($region_id)
    {
        $db = DataBase::getDB();
        $query = "select * from `regions` where `type`='city' and `parent_id`={?}";
        return Helpers_common::columnAsKey($db->select($query, [$region_id]), 'id');
    }

}