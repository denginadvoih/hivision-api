<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 07.02.15
 * Time: 17:21
 */
class Api_requests
{

    /**
     * Возвращает расширенную информацию о рубрике каталога с указанным ID
     * @param int $catalog_id \d+
     * @return Array
     */
    public static function get_request($request_id, $selected_fields = null)
    {
        $db = DataBase::getDB();
        $query = "
        select
            `request`.*,
            `f`.`id` as `field_id`,
            `f`.`title` as `field_title`,
            `f`.`type` as `field_type`,
            `r`.`id` as `reference_id`,
            `r`.`title` as `reference_title`,
            `rc`.`id` as `second_reference_id`,
            `rc`.`title` as `second_reference_title`,
            `ri`.`id` as `item_id`,
            `ri`.`title` as `item_title`
        from `requests` as `request`
        left join `requests_fields` as `rf` on `rf`.`request_id` = `request`.`id`
        left join `fields` as `f` on `f`.`id` = `rf`.`field_id`
        left join `references` as `r` on (`f`.`type` in ('reference', 'double_reference', 'multi_reference') and `r`.`id` = `f`.`reference_id`)
        left join `reference_items` as `ri` on (`ri`.`reference_id` = `r`.`id`)
        left join `references` as `rc` on (`f`.`type` = 'double_reference' and `rc`.`parent_id` = `r`.`id`)
        where `request`.`id`='{$request_id}'
        ";
        $res = $db->select($query);
        $catalog = [];
        if (is_array($res)) {
            $c_res = $res[0];
            $main_keys = [
                'id',
                'title',
                'header',
                'mail'
            ];
            foreach ($main_keys as $value) {
                $catalog[$value] = $c_res[$value];
            }
            $catalog['fields'] = [];
            foreach ($res as $value) {
                if ($value['field_id']) {
                    if (!isset($catalog['fields'][$value['field_id']])) {
                        $catalog['fields'][$value['field_id']] = [
                            'id' => $value['field_id'],
                            'title' => $value['field_title'],
                            'type' => $value['field_type'],
                            'order_check' => $value['field_order_check']
                        ];
                    }
                    $field = & $catalog['fields'][$value['field_id']];
                    if ($field['type'] == 'reference' or $field['type'] == 'double_reference' or $field['type'] == 'multi_reference') {
                        if (!isset($field['reference'])) {
                            $field['reference'] = [
                                'id' => $value['reference_id'],
                                'title' => $value['reference_title'],
                                'items' => []
                            ];
                        }
                        $reference = & $field['reference'];
                        if ($value['item_id']) {
                            if (!isset($reference['items'][$value['item_id']])) {
                                $reference['items'][$value['item_id']] = [
                                    'id' => $value['item_id'],
                                    'title' => $value['item_title']
                                ];
                            }
                        }
                        if ($field['type'] == 'double_reference') {
                            if (!isset($field['second_reference'])) {
                                $field['second_reference'] = [
                                    'id' => $value['second_reference_id'],
                                    'title' => $value['second_reference_title'],
                                    'items' => []
                                ];
                                //получаем дочерние элементы справочника, если родительский выделен
                                if (isset($selected_fields[$field['id']])) {
                                    foreach ($selected_fields[$field['id']] as $selected_field) {
                                        $_items = Api_fields::get_child_reference_items($selected_field);
                                        if (!empty($_items)) {
                                            $field['second_reference']['items'][$selected_field] = $_items;
                                        }
                                    }


                                }
                            }
                        }
                    }
                }
            }
        }
        return $catalog;
    }

}