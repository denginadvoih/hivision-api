<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 08.02.15
 * Time: 0:20
 */
class Api_Shipping
{
    public static function get($params = [])
    {
        $db = DataBase::getDB();
        $where = [];
        $whereParams = [];
        foreach ($params as $key => $param) {
            if (in_array($key, ['status', 'is_deleted'])) {
                $where[] = '`' . $key . '` = {?}';
                $whereParams[] = $param;
            }
        }
        $sql = "select * from `shipping`" . (!empty($where) ? ' where ' . implode(' and ', $where) : '');
        return $db->select($sql, $whereParams);
    }

    public static function get_by_id($id)
    {
        $db = DataBase::getDB();
        $sql = "select * from `shipping` where `id`={?}";
        return $db->selectRow($sql, [$id]);
    }

    public static function create($value)
    {
        $db = DataBase::getDB();
        return $db->query(
            "insert into `shipping` (`title`, `description`, `mail_message`, `price`) values ({?},{?}, {?}, {?})",
            [$value['title'], $value['description'], $value['mail_message'], $value['price']]
        );
    }

    public static function save($value)
    {
        $db = DataBase::getDB();
        return $db->query(
            "update `shipping` set `title`={?}, `description`={?}, `mail_message`={?}, `price`={?}, `status`={?}, `is_deleted`={?} where `id`={?}",
            [$value['title'], $value['description'], $value['mail_message'], $value['price'], $value['status'], $value['is_deleted'], $value['id']]
        );
    }

    public static function delete($id)
    {
        $db = DataBase::getDB();
        return $db->query("update `shipping` set `is_deleted`=1 where `id`={?}", [$id]);
    }
}