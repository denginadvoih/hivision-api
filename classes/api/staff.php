<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 08.02.15
 * Time: 0:20
 */
class Api_staff
{
    /**
     * Выборка файлов из объектов каталога со сдвигом и размером выборки
     * @param $catalog_id
     * @param $offset
     * @param $limit
     * @return array|bool
     */
    public static function get_staff_files($catalog_id, $offset, $limit, $scheme = null)
    {
        $db = DataBase::getDB();
        $query = "
        select
            `s`.`id`
        from staff as `s`
        inner join `staff_files` as `sf` on (`s`.`id`=`sf`.`staff_id` and `sf`.`is_main`=1)
        where `s`.`catalog_id`={$catalog_id}
        order by `s`.`id` desc
        limit {$offset}, {$limit}
        ";
        $_s = $db->select($query);
        $ids = Helpers_common::column_as_value($_s, 'id');
        $fileModel = new Api_files();
        $files = $fileModel->get_staff_files_by_ids($ids, true, $scheme ? $scheme : 'staff_prev');
        return $files;

    }

    /**
     * Выборка элементов из рубрики каталога и его дочерних элементов
     * @param array $params
     * @param int $offset
     * @param int $limit
     * @return array|bool
     */
    public static function get_staff_by($params, $offset = 0, $limit = null)
    {
        $db = DataBase::getDB();
        $where = [];
        if (isset($params['is_deleted']) && ($params['is_deleted'] == 0 || $params['is_deleted'] == 1)) {
            $where[] = "`ss`.`is_deleted`=" . $params['is_deleted'];
        }
        if (!empty($params['catalog_id'])) {
            $all_child = Models_catalogue::all_child($params['catalog_id']);
            array_push($all_child, $params['catalog_id']);
            $where[] = "`ss`.`catalog_id` in (" . implode(',', $all_child) . ")";
        }

        if (!empty($params['group_id'])) {
            $where[] = "`ss`.`id` in (select staff_id from `staff_groups` where `group_id` = '" . (int)$params['group_id'] . "')";
        }

        if (!empty($params['search'])) {
            $sWords = explode(' ', $params['search']);
            $sWordsWhere = [];
            foreach($sWords as $value) {
                $sWordsWhere[] =  "`ss`.`title` like '%" . trim($value) . "%' ";
            }
            if (!empty($sWordsWhere)) {
                $where[] = implode(' and ', $sWordsWhere);
            }
        }

        //отработка обычных полей
        if (!empty($params['fields'])) {
            foreach ($params['fields'] as $field_id => $field) {
                if (!empty($field)) {
                    if (is_array($field)) {
                        $where[] = "exists (select * from `staff_fields_values` as `sfv` where `sfv`.`staff_id`=`ss`.`id` and `sfv`.`value_reference` in (" . implode(',', $field) . "))";
                    } else {
                        $where[] = "exists (select * from `staff_fields_values` as `sfv` where `sfv`.`staff_id`=`ss`.`id` and `sfv`.`field_id`=" . (int)$field_id . " and `sfv`.`value_reference` = " . (int)$field . ")";
                    }
                }
            }
        }
        //отработка приходных полей, учитываемых при заказе
        if (!empty($params['cfields'])) {
            foreach ($params['cfields'] as $field) {
                if (!empty($field)) {
                    $where[] = "exists (
                    select * from `staff_capitalized` as `sc`
                    inner join `staff_fields_values_capitalized` as `sfvc` on (`sc`.`id` = `sfvc`.`capitalized_staff_id` and `sfvc`.`value` = " . (int)$field . ")
                    where `sc`.`staff_id`=`ss`.`id`)";
                }
            }
        }

        if (!empty($params['ids'])) {
            $where[] = "`ss`.`id` in (" . implode(',', $params['ids']) . ")";
        }

        if (!empty($params['search'])) {
            $where[] = "`ss`.`title` like '%" . $params['search'] . "%'";
        }

        $query_total = "
        select
            count(*)
            from staff as ss
            " . (empty($where) ? '' : 'where ' . implode(' and ', $where));
        $total = $db->selectCell($query_total);


        $query = "
        select
            `ss`.`id` as `id`,
            `ss`.`title` as `title`,
            `ss`.`second_title` as `second_title`,
            `ss`.`label` as `label`,
            `ss`.`date_insert` as `date_insert`,
            `ss`.`date_update` as `date_update`,
            `ss`.`description` as `description`,
            `ss`.`content` as `content`,
            `ss`.`price` as `price`,
            `ss`.`cashtype` as `cashtype`,
            `ss`.`catalog_id` as `catalog_id`,
            `c`.`type` as `catalog_type`,
            `c`.`path` as `catalog_path`
        from staff as ss
        left join `catalog` as `c` on (`c`.`id` = `ss`.`catalog_id`)
        " . (empty($where) ? '' : 'where ' . implode(' and ', $where)) . "
        order by `ss`.`sorting` desc" .
            ((!empty($limit)) ?
                " limit {$offset}, {$limit}"
                : '');
        $result = $db->select($query);
        $ids = Helpers_common::column_as_value($result, 'id');
        if (!empty($result)) {
            $query = "
            select
                `sg`.`staff_id` as `id`,
                `sg`.`group_id` as `group_id`,
                `g`.`title` as `title`
            from `staff_groups` as `sg`
            left join `groups` as `g` on `g`.`id`=`sg`.`group_id`
            where `sg`.`staff_id` in (" . implode(',', $ids) . ")
            ";
            $groupRes = $db->select($query);
            foreach ($groupRes as $row) {
                $groups[$row['id']][$row['group_id']] = ['id' => $row['group_id'], 'title' => $row['title']];
            }
            $files = new Api_files();
            $_itemFiles = $files->get_staff_files_by_ids($ids, $params['is_main'] ? $params['is_main'] : false, $params['scheme'] ? $params['scheme'] : 'staff_prev');
            $item_files = [];
            foreach ($_itemFiles as $file) {
                $item_files[$file['staff_id']][] = $file;
            }
        }


        foreach ($result as &$value) {
            if (isset($groups[$value['id']])) {
                $value['groups'] = $groups[$value['id']];
            } else {
                $value['groups'] = [];
            }
            if (isset($item_files[$value['id']])) {
                $value['file'] = $item_files[$value['id']];
            }
        }

        /*        $ids = Helpers_common::column_as_value($result, 'parent_id');
                if (!empty($ids)) {
                    $fields  = Api_fields::get_fields_by_catalog_ids($ids);
                    foreach ($result as &$value) {
                        $value['fields'] = isset($fields[$value['parent_id']]) ? $fields[$value['parent_id']] : array();
                    }
                }*/
        return ['items' => $result, 'total' => $total];
    }

    /**
     * Выборка элементов из рубрики каталога и его дочерних элементов
     * @param int $catalog_id
     * @param int $offset
     * @param int $limit
     * @return array|bool
     */
    public static function get_staff_by_catalog_id($catalog_id, $offset, $limit)
    {
        $db = DataBase::getDB();
        $all_child = Models_catalogue::all_child($catalog_id);
        array_push($all_child, $catalog_id);
        $query = "
        select
            `ss`.`id` as `id`,
            `ss`.`title` as `title`,
            `ss`.`label` as `label`,
            `ss`.`sspicname1` as `file`,
            `ss`.`date` as `date`,
            `ss`.`description` as `description`,
            `ss`.`price` as `price`,
            `ss`.`cashtype` as `cashtype`,
            `ss`.`parent_id` as `parent_id`
        from staff as ss
        where `ss`.`parent_id` in (" . implode(',', $all_child) . ")
        order by `ss`.`id` desc
        limit {$offset}, {$limit}
        ";
        return $db->select($query);
    }

    /**
     * Подсчитывает число объектов внутри рубрики каталога
     * @param $catalog_id
     * @return array|bool
     */
    public static function get_staff_count_by_catalog_id($catalog_id)
    {
        $db = DataBase::getDB();
        $all_child = Models_catalogue::all_child($catalog_id);
        array_push($all_child, $catalog_id);
        $query = "
        select
            count(*)
        from staff as ss
        where `ss`.`parent_id` in (" . implode(',', $all_child) . ")
        ";
        return $db->selectCell($query);
    }

    /**
     * Выбираем объект из базы по лэбелу (или id если цифровой)
     * @param $label
     * @return array|bool
     */
    public static function get_one_of_staff_by_label($label, $schemes = 'staff_prev')
    {
        $db = DataBase::getDB();
        if (is_numeric($label) && (int)$label != 0) {
            $query = "
            select
                `id`
            from staff
            where id = {?}
            limit 0, 1
            ";
        } else {
            $query = "
            select
                `id`
            from staff
            where label = {?}
            limit 0, 1
            ";
        }
        $id = $db->selectCell($query, [$label]);
        $result = current(self::get_staff_by_ids([$id]));
        if (is_array($result)) {
            $result['date'] = Helpers_common::convert_date($result['date_insert']);
            $filesModel = new Api_files();
            $_files = $filesModel->get_staff_files_by_ids([$result['id']], false, $schemes);
            $result['files'] = $_files;;
        }
        $result['reviews'] = Models_reviews::get(['staff_id' => $id, 'approved' => 1]);
        return $result;
    }

    public static function delete($id)
    {
        $db = DataBase::getDB();
        $query = "update `staff` set `is_deleted`=1 where `id`=" . (int)$id;
        $result = $db->query($query);
        return $result;
    }

    /**
     *
     */
    public static function get_staff_by_ids($ids)
    {
        $ids = array_map(function($id) {return (int)$id;}, $ids);
        $db = DataBase::getDB();
        $query = "
        select
            `s`.*,
            `scon`.`condition` as `condition`, 
            `fi`.`title` as `field_title`,
            `fi`.`type` as `field_type`,
            `fi`.`label` as `field_label`,
            `sfv`.`field_id`,
            `sfv`.value_integer,
            `sfv`.value_float,
            `sfv`.value_reference,
            `sfv`.value_string,
            `ri`.`title` as `value_reference_title`,
            `sc`.`id` as `capi_id`,
            `sc`.`value` as `capi_value`,
            `sfvc`.`field_id` as `capi_field_id`,
            `sfvc`.`value` as `capi_field_value`,
            `fic`.`type` as `capi_field_type`,
            `fic`.`title` as `capi_field_title`,
            `risfvc`.`title` as `capi_field_value_reference_title`,
            `sf`.`file_id`,
            `sf`.`is_main`,
            `sf`.`linked_in`,
            `sf`.`sorting` as `file_sorting`,
            `f`.`path` as `file_path`,
            `f`.`title` as `file_title`,
            `f`.`type`	as `file_type`
        from `staff` as `s`
            left join `staff_condition` as `scon` on (`s`.`id` = `scon`.`staff_id`)
            left join `staff_fields_values` as `sfv` on (`s`.`id` = `sfv`.`staff_id`)
            left join `fields` as `fi` on (`sfv`.`field_id` = `fi`.`id`)
            left join `reference_items` as `ri` on (`sfv`.`value_reference` is not null and `sfv`.`value_reference` = `ri`.`id`)
            left join `staff_capitalized` as `sc` on (`s`.`id` = `sc`.staff_id)
            left join `staff_fields_values_capitalized` as `sfvc` on (`sc`.`id` = `sfvc`.capitalized_staff_id)
            left join `fields` as `fic` on (`sfvc`.`field_id` = `fic`.`id`)
            left join `reference_items` as `risfvc` on (`sfvc`.`value` = `risfvc`.`id` )
            left join `staff_files` as `sf` on (`s`.`id` = `sf`.`staff_id`)
            left join `files` as `f` on (`sf`.`file_id` = `f`.`id`)
        where `s`.`id` in (" . implode(',', $ids) . ")
        ";
        $result = [];
        $res = $db->select($query);
        if (!empty($res) and is_array($res)) {
            foreach ($res as $v) {
                if (!isset($result[$v['id']])) {
                    $result[$v['id']] = Helpers_arr::extract($v,
                        [
                            'id',
                            'sorting',
                            'catalog_id',
                            'title',
                            'second_title',
                            'label',
                            'price',
                            'old_price',
                            'cashtype',
                            'description',
                            'content',
                            'condition',
                            'link',
                            'discuss',
                            'date_insert',
                            'date_update',
                            'price_for',
                            'custom_id'
                        ]
                    );
                    $result[$v['id']]['files'] = [];
                    $result[$v['id']]['fields'] = [];
                    $result[$v['id']]['capitalized'] = [];
                }
                //устанавливаем дополнительные поля
                $fields = & $result[$v['id']]['fields'];
                if (!empty($v['field_id'])) {
                    if (!isset($fields[$v['field_id']])) {
                        $fields[$v['field_id']] = [
                            'id' => $v['field_id'],
                            'type' => $v['field_type'],
                            'label' => $v['field_label'],
                            'title' => $v['field_title'],
                            'value_ids' => [],
                            'values' => []
                        ];
                    }
                    $_value = current(array_filter(Helpers_arr::extract($v,
                        [
                            'value_integer',
                            'value_float',
                            'value_reference',
                            'value_string'],
                        null)));
                    if (empty($v['value_reference_title'])) {
                        $_value = $_value;
                    } else {
                        $fields[$v['field_id']]['value_ids'][] = $_value;
                        $_value = [
                            'id' => $_value,
                            'title' => $v['value_reference_title']
                        ];
                    }
                    if (!in_array($_value, $fields[$v['field_id']]['values'])) {
                        $fields[$v['field_id']]['values'][] = $_value;
                    }
                }

                //устанавлваем наличие по полям, указанных для заказа
                $capi_fields = & $result[$v['id']]['capitalized'];
                if (!empty($v['capi_id'])) {
                    if (!isset($capi_fields[$v['capi_id']])) {
                        $capi_fields[$v['capi_id']] = [
                            'id' => $v['capi_id'],
                            'value' => $v['capi_value'],
                            'fields' => []
                        ];
                    }
                    if (!empty($v['capi_field_id'])) {
                        if (!isset($capi_fields[$v['capi_id']]['fields'][$v['capi_field_id']])) {
                            $capi_fields[$v['capi_id']]['fields'][$v['capi_field_id']] =
                                [
                                    'id' => $v['capi_field_id'],
                                    'type' => $v['capi_field_type'],
                                    'title' => $v['capi_field_title'],
                                    'value_ids' => [],
                                    'values' => []
                                ];
                        }
                        $_value = empty($v['capi_field_value_reference_title']) ? $v['capi_field_value'] :
                            ['id' => $v['capi_field_value'], 'title' => $v['capi_field_value_reference_title']];

                        if (!in_array($_value, $capi_fields[$v['capi_id']]['fields'][$v['capi_field_id']]['values'])) {
                            if (isset($_value['id'])) {
                                $capi_fields[$v['capi_id']]['fields'][$v['capi_field_id']]['value_ids'][] = $_value['id'];
                            }
                            $capi_fields[$v['capi_id']]['fields'][$v['capi_field_id']]['values'][] = $_value;
                        }
                    }

                }

                //выбираем файлы
                $files = & $result[$v['id']]['files'];;
                if (!isset($files[$v['file_id']]) && !empty($v['file_id'])) {
                    $files[$v['file_id']] = Helpers_arr::extract($v,
                        [
                            'file_id',
                            'is_main',
                            'linked_in',
                            'file_sorting',
                            'file_path',
                            'file_title',
                            'file_type'
                        ]
                    );
                    $files[$v['file_id']] = Helpers_arr::array_rename($files[$v['file_id']],
                        [
                            'file_id' => 'id',
                            'file_sorting' => 'sorting',
                            'file_path' => 'path',
                            'file_title' => 'title',
                            'file_type' => 'type'
                        ]
                    );
                    if ($files[$v['file_id']]['type'] == 'image') {
                        $file_model = new Api_files();
                        $files[$v['file_id']]['preview'] = [
                            'admin_prev' => $file_model->getByScheme($files[$v['file_id']]['path'] . '/' . $files[$v['file_id']]['title'], 'admin_prev'),
                            'staff_prev' => $file_model->getByScheme($files[$v['file_id']]['path'] . '/' . $files[$v['file_id']]['title'], 'staff_prev')
                        ];
                    } else {
                        $files[$v['file_id']]['preview'] = [
                            'admin_prev' => '/assets/images/spacer.gif',
                            'staff_prev' => '/assets/images/spacer.gif'
                        ];
                    }
                }

            }
            //генерим урлы к выборке
            $catalogues_ids = Helpers_common::column_as_value($result, 'catalog_id');
            $query = "select `id`,`path` from  `catalog` where `id` in (" . implode(',', $catalogues_ids) . ")";
            $urls = $db->select($query);
            $urls = Helpers_common::columnAsKey($urls, 'id');
            foreach ($result as &$item) {
                $item['catalogue_path'] = $urls[$item['catalog_id']]['path'];
            }
            //получаем отзывы к выборке
        }

        return $result;
    }

    public static function remove($id)
    {
        $db = DataBase::getDB();
        $files = $db->select("select * from `files` where `id` in (select `file_id` from `staff_files` where `staff_id`=" . $id . ")");
        $filesModel = new Api_files();
        foreach ($files as $file) {
            $filesModel->delete($file['id']);
        }
        $db->query("delete from `staff` where `id` = " . (int) $id);
        $db->query("delete from `staff_fields_values` where `staff_id` = " . (int) $id);
        $db->query("delete from `staff_fields_values_capitalized` where `capitalized_staff_id` in (SELECT `id` from `staff_capitalized` where `staff_id`=" . (int)$id) . ")";
        $db->query("delete from `staff_capitalized` where `staff_id`=" . (int)$id);
        $db->query("delete from `staff_files` where `staff_id`=" . (int)$id);
    }

    public static function group($id, $group_id)
    {
        $db = DataBase::getDB();
        $query = "select count(*) from `staff_groups` where `group_id` = {?} and `staff_id` = {?}";
        $is_group = $db->selectCell($query, [$group_id, $id]);
        if ($is_group > 0) {
            $query = "delete from `staff_groups` where `group_id` = {?} and `staff_id` = {?}";
            $result = false;
        } else {
            $query = "insert into `staff_groups` (`group_id`, `staff_id`) values ({?}, {?})";
            $result = true;
        }
        $db->query($query, [$group_id, $id]);
        return $result;
    }

    //изменение сортировки
    public static function sortChange($id, $catalogue_id, $to)
    {
        $db = DataBase::getDB();
        $currentSorting = $db->selectCell("select `sorting` from `staff` where `id` = {?}", [$id]);

        $query = ($to == 'up') ? "select `id`, `sorting` from `staff` where `sorting` > {?} and `catalog_id`={?} order by `sorting` asc limit 0,1"
            : "select `id`, `sorting` from `staff` where `sorting` < {?} and `catalog_id`={?} order by `sorting` desc limit 0,1";
        $neighbor = $db->selectRow($query, [$currentSorting, $catalogue_id]);

        $updateQuery = "update staff set `sorting`={?} where `id`={?}";

        $db->query($updateQuery, [$currentSorting, $neighbor['id']]);
        $db->query($updateQuery, [$neighbor['sorting'], $id]);
    }
}