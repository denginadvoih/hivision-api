<?php
/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 07.02.15
 * Time: 18:55
 */

class Config {
    public function get($name) {
        if (is_file($config_file = DOCROOT.'classes/config/'.$name.'.php')) {
            return include $config_file;
        } else {
            die('Не найден конфигурационнай файл - '.$config_file);
        }
    }
} 