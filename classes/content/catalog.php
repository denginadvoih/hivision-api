<?php //defined('SYSPATH') or die('No direct script access.');

class Content_catalog
{
    public $catalog_info;
    function __construct($catalog_info)
    {
        $this->catalog_info = $catalog_info;
    }


    /*
     * Выводит полную информацию о рубрике
     * @param int $catalog_id
     * @return string
     */
    public function full_description()
    {
        $twig = Twig::get_instance();
        return $twig->template
            ->loadTemplate('catalog-full-description.twig')
            ->render(array('catalog' => $this->catalog_info));
    }
}
