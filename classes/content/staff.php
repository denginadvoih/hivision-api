<?php //defined('SYSPATH') or die('No direct script access.');

class Content_staff
{
    public $catalog_info;
    public $page_size = 10;
    public $params;
    public $url;
    public $entity;

    function __construct($catalog_info, $params, $url, $entity)
    {
        $this->catalog_info = $catalog_info;
        $this->params = $params;
        $this->url = $url;
        $this->entity = $entity;
    }

    /*
     * @param array $item
     * @return string
     */
    public function staff_preview($item)
    {
        $twig = Twig::get_instance();
        $view_type[0] = 'type-0-common';
        $view_type[8] = 'type-8-product';
        $view_type[1] = 'type-1-photo';
        $view_type[16] = 'type-16-file';
        $view_type[17] = 'type-17-news';
        $view_type[18] = 'type-18-table';

        $view = $view_type[$this->catalog_info['type']];

        return $twig->template
            ->loadTemplate($view . '.twig')
            ->render(array('catalog' => $this->catalog_info, 'item' => $item));

    }

    public function content_of_staff()
    {
        $twig = Twig::get_instance();
        $item_template = 'item-full-type'.$this->catalog_info['type'].'.twig';
        $item = Api_staff::get_one_of_staff_by_label($this->entity);
        return $twig->template
            ->loadTemplate($item_template)
            ->render(array(
                'catalog' => $this->catalog_info,
                'url' => $this->url,
                'item' => $item
            ));
    }

    public function paginator ()
    {
        $twig = Twig::get_instance();
        $total = Api_staff::get_staff_count_by_catalog_id($this->catalog_info['id']);
        $page_count = ceil($total/$this->page_size);
        return $twig->template
            ->loadTemplate('paginator.twig')
            ->render(array(
                'url' => $this->url,
                'params' => $this->params,
                'current_page' => $this->params['page'],
                'page_size' => $this->page_size,
                'total' => $total,
                'page_count' => $page_count
            ));
    }
}
