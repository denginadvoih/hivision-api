<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin
{
    public $url; //текущий урл
    public $params; //параметры search, page и прочее
    public $twig;
    public $structure_template;
    public $content;
    public $perm_modules;
    public $catalog_id = 1;
    public $auth_error = 0;//1 - неверная пара пароль/логин, 2 - нет доступа к панели управления

    function __construct($url_array, $params, $url, $object)
    {
        session_start();
        $this->params = $params;
        $this->params['catalog_id'] = empty($this->params['catalog_id']) ? 1 : $this->params['catalog_id'];
        $this->url = $url;
        $this->twig = Twig::get_instance();
        $this->twig->admin_replacer();
        $this->structure_template = '/construction/admin.twig';
    }

    /**
     * форма входа в панель управления
     */
    public function login()
    {
        $this->structure_template = '/construction/admin-login.twig';
    }

    /**
     * записывает в сессии введенные данные из авторизационной формы
     */
    public function auth()
    {
        if (isset($_POST['email']) and isset($_POST['password'])) {
            $auth = Auth_auth::get_instance();
            if ($auth->login($_POST['email'], $_POST['password'], ($_POST['remember'] == 1))) {
                if (isset($_POST['url'])) {
                    header("Location: ".$_POST['url']);
                } else {
                    header("Location: /" . ADMIN_SUB_URL);
                }
                exit;
            } else {
                $this->structure_template = '/construction/admin-login.twig';
                $this->auth_error = 1;
            }
        } else {
            $this->structure_template = '/construction/admin-login.twig';
            $this->auth_error = 1;
        }
        exit;
    }

    /**
     * выход из авторизации
     */
    public function logout()
    {
        $auth = Auth_auth::get_instance();
        $auth->logout();
        header('HTTP/1.1 200 OK');
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/'. ADMIN_SUB_URL);
        exit;
    }

    function __destruct()
    {
            echo $this->twig->template
                ->loadTemplate($this->structure_template)
                ->render(array(
                    'catalog_id' => $this->params['catalog_id'],
                    'content' => $this->content,
                    'url' => isset($_POST['url']) ? $_POST['url'] : $this->url
                ));
        exit;
    }
} 