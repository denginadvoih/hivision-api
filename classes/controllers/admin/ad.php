<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_ad extends Controllers_admin
{
    public $templates = array(
        'add' => 'construction/controllers/ad/add.twig',
        'ad_edit' => 'construction/controllers/ad/ad_edit.twig',
        'blocks' => 'construction/controllers/ad/blocks.twig',
        'block' => 'construction/controllers/ad/block.twig'
    );
    public $types = ['block', 'slide'];

    //форма создания баннера
    public function add()
    {
        $ad_blocks = Api_Ads::get_blocks();
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['add'])
            ->render([
                'ads' => $ad_blocks
            ]);
    }

    //создание баннера
    public function create()
    {
        $db = DataBase::getDB();
        $query = "
        insert into `ad_staff`
        (`file_id`, `title`, `info`, `link`, `block_id`)
        values
        ({?},{?},{?},{?},{?})
        ";
        $db->query($query, [$_POST['file'], $_POST['title'], $_POST['info'], $_POST['url'], $_POST['parent_block']]);
        header('HTTP/1.1 200 OK');
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/adblocks');
    }

    //форма редактирования баннера
    public function edit()
    {
        $ad_blocks = Api_Ads::get_blocks();
        $banner = Api_Ads::get_staff_by_id($this->params['id']);
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['ad_edit'])
            ->render([
                'ads' => $ad_blocks,
                'banner' => $banner
            ]);
    }

    //сохранение баннера
    public function save()
    {
        $db = DataBase::getDB();
        $query = "
        update `ad_staff`
        set `file_id`={?}, `title`={?}, `info`={?}, `link`={?}, `block_id`={?}
        where `id`={?}
        ";
        $db->query($query, [$_POST['file'], $_POST['title'], $_POST['info'], $_POST['url'], $_POST['parent_block'], $this->params['id']]);
        header('HTTP/1.1 200 OK');
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/adblocks');
    }


    //список баннеров блока
    public function block()
    {
        $block = Api_Ads::get_block($this->params['id']);
        $staff = Api_Ads::get_staff_by_block($this->params['id']);
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['block'])
            ->render(['block' => $block, 'staff' => $staff]);
    }

    //список блоков
    public function blocks()
    {
        $blocks = Api_Ads::get_blocks();//\Pr::p($blocks);
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['blocks'])
            ->render(['ads' => $blocks, 'types' => $this->types]);
    }

    //сохранение блоков
    public function blocks_save()
    {
        $db = DataBase::getDB();
        foreach ($_POST['ad'] as $key => $ad) {
            $query = "
            update `ad_blocks` set
            `title` = {?},
            `rnd` = {?},
            `rnd_num` = {?},
            `type` = {?}
            where `id` = {?}
            ";
            $db->query($query, [$ad['title'], isset($ad['rnd']) ? 1 : 0, $ad['rnd_num'], $ad['type'], $key]);
        }
        header('HTTP/1.1 200 OK');
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/adblocks');
    }


}