<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_ajax_catalog
{
    public $params = [];
    function __construct($url_array, $params, $url, $object)
    {
        $this->params = $params;
    }

    public function get()
    {
        $id = $this->params['id'];
        $catalog = Models_catalogue::get_catalog($id);
        echo json_encode($catalog);
    }

    public function delete()
    {
        $id = $this->params['id'];
        $catalog = Models_catalogue::delete($id);
        echo json_encode($id);
    }

    public function get_expand()
    {
        $id = $this->params['id'];
        $catalog = Models_catalogue::get_catalogue_expand($id);
        echo json_encode($catalog);
    }

    public function get_expand_with_field_form()
    {
        $id = $this->params['id'];
        $catalog = Models_catalogue::get_catalogue_expand($id);
        $catalog['field_form'] = Twig::get_instance()->template
            ->loadTemplate('construction/modules/field-form.twig')
            ->render([
                'fields' => $catalog['fields']
            ]);
        echo json_encode($catalog);
    }

    public function sort()
    {
        $id = (int)$this->params['id'];
        $to = $this->params['to'];
        Models_catalogue::sortChange($id, $to);
        echo json_encode($id);
    }
}