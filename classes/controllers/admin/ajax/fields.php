<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_ajax_fields
{
    public $params = [];
    function __construct($url_array, $params, $url, $object)
    {
        $this->params = $params;
    }

    public function get_reference()
    {
        $id = $this->params['id'];
        $reference = Models_fields::get_reference($id);
        echo json_encode($reference);
    }

    public function get_child_reference()
    {
        $child_id = $this->params['child_id'];
        $reference_items = Models_fields::get_child_reference_items($child_id);
        echo json_encode($reference_items);
    }

    //функция возвращает набор html полей для заполнения прихода товара
    public function add_staff_value()
    {
        $id = $this->params['id'];
        $catalog = Models_catalogue::get_catalogue_expand($id);
        echo Twig::get_instance()->template
            ->loadTemplate('construction/modules/value-fields.twig')
            ->render([
                'unique' => '_' . time(),
                'fields' => array_filter($catalog['fields'], function($field) {
                    return $field['order_check'] == "Y";
                })
            ]);
    }

    public function delete_reference_item()
    {
        $id = $this->params['id'];
        return Models_fields::mark_deleted_ref_item($id);
    }
}