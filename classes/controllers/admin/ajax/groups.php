<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_ajax_groups
{
    public $params = [];
    function __construct($url_array, $params, $url, $object)
    {
        $this->params = $params;
    }

    public function linkstaff()
    {
        $id = $this->params['id'];
        $group_id = $this->params['group_id'];
        $result = Api_staff::group($id, $group_id);
        echo json_encode([$id, $group_id, $result]);
    }

    public function delete()
    {
        $id = $this->params['id'];
        return Models_groups::delete($id);
    }
}