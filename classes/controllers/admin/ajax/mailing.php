<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_ajax_mailing
{
    public $params = [];
    function __construct($url_array, $params, $url, $object)
    {
        $this->params = $params;
    }

    public function cancel()
    {
        $id = $this->params['id'];
        echo json_encode(Models_mailings::cancel($id));
    }

    public function start()
    {
        $id = $this->params['id'];
        echo json_encode(Models_mailings::start($id));
    }
}