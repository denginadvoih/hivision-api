<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_ajax_orders
{
    public $params = [];
    function __construct($url_array, $params, $url, $object)
    {
        $this->params = $params;
    }

    public function update_post_id()
    {
        $order_id = $_POST['orderId'];
        $post_id = $_POST['postId'];
        $result = Api_Orders::update_post_id($order_id, $post_id);
        if ($result) {
            Api_Orders::send_post_id($order_id, $post_id);
        }
        echo json_encode(['order_id' => $order_id, 'post_id' => $post_id]);
    }

    public function mark_deleted()
    {
        $order_id = (int)$this->params['id'];
        Api_Orders::delete($order_id);
        echo json_encode(['id' => $order_id, 'is_deleted' => 1]);
    }
}