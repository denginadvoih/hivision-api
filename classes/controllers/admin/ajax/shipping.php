<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_ajax_shipping
{
    public $params = [];
    function __construct($url_array, $params, $url, $object)
    {
        $this->params = $params;
    }

    /**
     *
     * Удаление методов доставки
     */
    public function delete()
    {
        $id = $this->params['id'];
        Api_shipping::delete($id);
        echo json_encode($id);
    }
}