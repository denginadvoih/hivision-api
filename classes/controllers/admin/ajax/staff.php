<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_ajax_staff
{
    public $params = [];
    function __construct($url_array, $params, $url, $object)
    {
        $this->params = $params;
    }

    public function delete()
    {
        $id = $this->params['id'];
        $result = Api_staff::delete($id);
        echo json_encode($id);
    }

    public function sort()
    {
        $id = (int)$this->params['id'];
        $catalogue_id = (int)$this->params['catalog_id'];
        $to = $this->params['to'];
        Api_staff::sortChange($id, $catalogue_id, $to);
        echo json_encode($id);
    }
}