<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_catalog extends Controllers_admin
{
    public $templates = array(
        'staff_page' => 'construction/controllers/catalog/content-page.twig',
        'item' => 'construction/controllers/catalog/item.twig',
        'add' => 'construction/controllers/catalog/add.twig',
        'edit' => 'construction/controllers/catalog/edit.twig'
    );

    //дефалтовый метод
    public function index()
    {
        $catalog_info = Models_catalogue::get_catalogue($this->params['catalog_id']);
        $page = empty($_GET['page']) ? 1 : $_GET['page'];
        $items = Api_staff::get_staff_by(
            [
                'catalog_id' => $this->params['catalog_id'],
                'scheme' => 'admin_prev',
                'group_id' => !empty($_GET['group_id']) ? $_GET['group_id'] : null,
                'search' => !empty($_GET['search']) ? $_GET['search'] : null,
                'is_deleted' => (isset($_SESSION['is_deleted']) && $_SESSION['is_deleted'] == 'show') ? null : 0
            ],
            ($page - 1) * PAGE_SIZE, PAGE_SIZE
        );
        //переадресаци на редактирование каталога, если каталог пустой
        if ($items['total'] == 0) {
            Header('Location: /site-construction/catalog/' . $this->params['catalog_id'] . '/edit');
            exit;
        }
        foreach ($items['items'] as &$value) {
            $value['date'] = Helpers_common::convert_date($value['date_insert'], 'en');
        }
        unset($value);
        $url_params = [];
        foreach ($_GET as $key => $value) {
            if ($key != 'page' && $key != 'route') {
                $url_params[] = $key . '=' . $value;
            }
        }
        $groups = Api_Groups::get();
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['staff_page'])
            ->render(array(
                'catalog' => $catalog_info,
                'url' => $this->url,
                'items' => $items,
                'groups' => $groups,
                'current_group' => !empty($_GET['group_id']) ? $_GET['group_id'] : null,
                'search' => !empty($_GET['search']) ? $_GET['search'] : '',
                'item_template' => $this->templates['item'],
                'paginator' => 'construction/modules/paginator.twig',
                'current_page' => $page,
                'pages_count' => ceil($items['total'] / PAGE_SIZE),
                'url_params' => implode('&', $url_params)
            ));
    }

    //форма создания каталога
    public function add()
    {
        $this->catalog_id = empty($this->params['section']) ? 1 : $this->params['section'];
        $catalog_info = Models_catalogue::get_catalogue($this->catalog_id);

        $this->content = $this->twig->template
            ->loadTemplate($this->templates['add'])
            ->render(array(
                'catalog' => $catalog_info,
                'catalog_id' => $this->catalog_id,
                'url' => $this->url
            ));
    }

    //создание каталога
    public function create()
    {
        $db = DataBase::getDB();
        $query = "insert into catalog (`title`, `label`, `parent_id`) values ({?},{?},{?})";
        $id = $db->query($query, [$_POST['title'], Helpers_common::getSafeUrlName($_POST['title']), $_POST['parent']]);
        Models_catalogue::generate_path();
        if (is_numeric($id)) {
            header("Location: /site-construction/catalog/" . $id);
            exit;
        } else {

        }
    }

    //форма редактирования каталога
    public function edit()
    {
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['edit'])
            ->render([
                'catalog' => Models_catalogue::get_catalogue_expand($this->params['catalog_id']),
                'fields' => Models_fields::get_fields()
            ]);
    }

    //сохрание каталога
    public function save()
    {
        $db = DataBase::getDB();
        $query = "
        update `catalog`
        set
        `title` = {?},
        `label` = {?},
        `parent_id` = {?},
        `type` = {?},
        `second_title` = {?},
        `description` = {?},
        `content` = {?},
        `template` = {?},
        `hmenu` = {?},
        `is_visible` = {?},
        `root` = {?},
        `in_branch` = {?},
        `meta` = {?},
        `cols` = {?},
        `rows` = {?}
        where `id` = {?}
        ";
        $params = [
            $_POST['title'],
            empty($_POST['label']) ? Helpers_common::getSafeUrlName($_POST['title']) : $_POST['label'],
            $_POST['parent'],
            $_POST['type'],
            $_POST['second_title'],
            $_POST['description'],
            $_POST['content'],
            $_POST['template'],
            isset($_POST['hmenu']) ? 1 : 0,
            isset($_POST['is_visible']) ? 1 : 0,
            isset($_POST['root']) ? 1 : 0,
            isset($_POST['in_branch']) ? 1 : 0,
            json_encode([
                'title' => $_POST['meta_title'],
                'description' => $_POST['meta_description'],
                'keywords' => $_POST['meta_keywords']
            ]),
            isset($_POST['cols']) ? (int)$_POST['cols'] : 4,
            isset($_POST['rows']) ? (int)$_POST['rows'] : 10,
            $this->params['catalog_id']
        ];
        Models_catalogue::generate_path();
        $db->query($query, $params);
        //вставляем файлы коли есть таковые
        $query = "delete from `catalogue_files` where `catalogue_id` = " . (int)$this->params['catalog_id'];
        $db->query($query);
        if (!empty($_POST['files'])) {
            $files = json_decode($_POST['files'], true);
            $file_values = [];
            foreach ($files as $key => $value) {
                    $file_values[] = '(' . $this->params['catalog_id'] . ', ' . $value['file_id'] . ',\'' . $value['file_appointment'] . '\')';
            }
            $query = "insert into `catalogue_files` (`catalogue_id`, `file_id`, `appointment`) values " . implode(',', $file_values);
            $db->query($query);
        }

        //цепляем поля
        $query = "delete from `catalog_fields` where `catalog_id` = " . (int)$this->params['catalog_id'];
        $db->query($query);
        if (!empty($_POST['fields'])) {
            $f_values = [];
            foreach ($_POST['fields'] as $key => $value) {
                if (isset($value['checked']) and $value['checked'] == 'Y') {
                    $f_values[] = '(' . $this->params['catalog_id'] . ', ' . (int)$key . ', ' . (isset($value['order_check']) ? '\'' . $value['order_check'] . '\'' : '\'N\'') . ')';
                }
            }
            $query = "insert into `catalog_fields` (`catalog_id`, `field_id`, `order_check`) values " . implode(',', $f_values);
            $db->query($query);
        }
        header("Location: /site-construction/catalog/" . $this->params['catalog_id']);
        exit;
    }
}