<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_customers extends Controllers_admin
{
    public $templates = array(
        'list' => 'construction/controllers/customers/list.twig'
    );
    public $limit = 20;

    /**
     * Вывод списка пользователей
     */
    public function get_list()
    {
        $params = [];
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $params['offset'] = ($page - 1) * $this->limit;
        $params['limit'] = $this->limit;
        $params['search'] = isset($_GET['search']) ? $_GET['search'] : null;
        $resCustomers = Models_users::get($params);
        krsort($resCustomers['items']);
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['list'])
            ->render(array(
                'users' => $resCustomers['items'],
                'total' => $resCustomers['total'],
                'search' => isset($_GET['search']) ? $_GET['search'] : '',
                'current_page' => $page,
                'pages_count' => ceil($resCustomers['total'] / $this->limit)
            ));
    }
}