<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_fields extends Controllers_admin
{
    public $types = ['string', 'integer', 'float', 'reference'];
    public $templates = array(
        'fields' => 'construction/controllers/fields/fields.twig',
        'add' => 'construction/controllers/fields/add.twig',
        'edit' => 'construction/controllers/fields/edit.twig',
        'references' => 'construction/controllers/fields/references.twig',
        'references_items' => 'construction/controllers/fields/references_items.twig',
        'add_reference' => 'construction/controllers/fields/add_reference.twig',
        'edit_reference' => 'construction/controllers/fields/edit_reference.twig',
        'add_reference_items' => 'construction/controllers/fields/add_reference_items.twig'
    );

    const DELIMITER = '|';

    //список справочников
    public function get()
    {
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['fields'])
            ->render(array(
                'fields' => Models_fields::get_fields()
            ));
    }

    //форма создания поля
    public function add()
    {
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['add'])
            ->render(array(
                'types' => $this->types,
                'parent_refs' => Models_fields::get_parent_references()
            ));
    }

    //создание поля
    public function create()
    {
        //TODO сделать валидацию
        $title = $_POST['title'];
        $label = $_POST['label'];
        $type = $_POST['type'];
        $reference_id = isset($_POST['reference']) ? $_POST['reference'] : (isset($_POST['double_reference']) ? $_POST['double_reference'] : 0);
        $db = DataBase::getDB();
        $query = "insert into `fields` (`title`, `label`, `type`, `reference_id`) values ({?}, {?}, {?}, {?})";
        $created_id = $db->query($query, [$title, $label, $type, $reference_id]);
        if ($created_id) {
            header('HTTP/1.1 200 OK');
            header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/fields');
        }
        exit;
    }

    //форма редактирования поля
    public function edit()
    {
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['edit'])
            ->render(array(
                'types' => $this->types,
                'field' => Models_fields::get_field($this->params['id']),
                'parent_refs' => Models_fields::get_parent_references()
            ));
    }

    //создание поля
    public function save()
    {
        //TODO сделать валидацию
        $title = $_POST['title'];
        $label = $_POST['label'];
        $type = $_POST['type'];
        $reference_id = isset($_POST['reference']) ? $_POST['reference'] : (isset($_POST['double_reference']) ? $_POST['double_reference'] : 0);
        $db = DataBase::getDB();
        $query = "update `fields` set `title` = {?}, `label` = {?}, `type` = {?}, `reference_id` = {?} where `id` = '" . (int)$this->params['id'] . "'";
        $created_id = $db->query($query, [$title, $label, $type, $reference_id]);
        if ($created_id) {
            header('HTTP/1.1 200 OK');
            header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/fields');
        }
        exit;
    }

    //создание поля
    public function delete()
    {
        $db = DataBase::getDB();
        $query = "delete from `fields` where `id` = '" . (int)$this->params['id'] . "'";
        $created_id = $db->query($query);
        if ($created_id) {
            header('HTTP/1.1 200 OK');
            header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/fields');
        }
        exit;
    }

    //форма создания справочника
    public function add_reference()
    {
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['add_reference'])
            ->render(array(
                'parent_refs' => Models_fields::get_parent_references()
            ));
    }

    //создание справочника
    public function create_reference()
    {
        $db = DataBase::getDB();
        $query = "insert into `references` (`title`, `label`, `parent_id`) values ({?}, {?}, {?})";
        $created_id = $db->query($query, [$_POST['title'], $_POST['label'], $_POST['parent_id']]);
        if ($created_id) {
            header('HTTP/1.1 200 OK');
            header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/references');
        }
        exit;
    }

    //форма редактирования справочника
    public function edit_reference()
    {
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['edit_reference'])
            ->render(array(
                'reference' => Models_fields::get_reference($this->params['id']),
                'parent_refs' => Models_fields::get_parent_references()
            ));
    }

    //создание справочника
    public function save_reference()
    {
        $db = DataBase::getDB();
        $query = "update `references` set `title`={?}, `label`={?}, `parent_id`={?} where `id` = {?}";
        $db->query($query, [$_POST['title'], $_POST['label'], $_POST['parent_id'], $this->params['id']]);
        header('HTTP/1.1 200 OK');
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/references');
        exit;
    }

    //список справочников
    public function get_references()
    {
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['references'])
            ->render(array(
                'references' => Models_fields::get_references()
            ));
    }

    //форма добавления элемента справочника
    public function add_reference_items()
    {
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['add_reference_items'])
            ->render([
                'parent_refs' => Models_fields::get_references()
            ]);
    }

    //создание элемента справочника
    public function create_reference_items()
    {
        $db = DataBase::getDB();
        if (substr_count($_POST['title'], self::DELIMITER, 1) !== 0) {
            $item_titles = explode(self::DELIMITER, $_POST['title']);
            foreach ($item_titles as $item_title) {
                $query = "insert into `reference_items` (`title`, `label`, `parent_id`, `reference_id`) values ({?},{?},{?},{?})";
                $db->query($query, [$item_title, Helpers_common::getSafeUrlName($item_title), isset($_POST['parent_id']) ? $_POST['parent_id'] : 0, $_POST['reference_id']]);
            }
        } else {
            $query = "insert into `reference_items` (`title`, `label`, `parent_id`, `reference_id`) values ({?},{?},{?},{?})";
            $db->query($query, [$_POST['title'], empty($_POST['label']) ? Helpers_common::getSafeUrlName($_POST['title']) : $_POST['label'], isset($_POST['parent_id']) ? $_POST['parent_id'] : 0, $_POST['reference_id']]);
        }
        header('HTTP/1.1 200 OK');
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/references');
    }

    public function get_references_items()
    {
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['references_items'])
            ->render(array(
                'items' => Models_fields::get_references_items(['is_deleted' => 0])
            ));
    }
}