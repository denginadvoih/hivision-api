<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_files
{
    public $path = 'staff_files';
    public $params = [];
    public $allowed_types = ['image' => ['png', 'gif', 'jpg', 'jpeg'], 'document' => ['psd', 'doc', 'xls', 'docx', 'pdf']];

    function __construct($url_array, $params, $url, $object)
    {
        $this->params = $params;
    }


    //загрузка image файлов из ckeditor
    public function ckupload()
    {
        $callback = $_GET['CKEditorFuncNum'];
        $file_name = $_FILES['upload']['name'];
        $file_name_tmp = $_FILES['upload']['tmp_name'];

        $full_path = './' . $this->path . '/' . $file_name;
        $http_path = '/' . $this->path . '/' . $file_name; // адрес изображения для обращения через http
        $error = '';
        if (move_uploaded_file($file_name_tmp, $full_path)) {
            $db = DataBase::getDB();
            $query = "insert into `files` (`title`, `path`, `type`) values ({?}, '{$this->path}', 'image')";
            $db->query($query, [$file_name]);
        } else {
            $error = 'Ошибка, повторите попытку позже';
            $http_path = '';
        }
        echo "
        <script>
        window . parent . CKEDITOR . tools . callFunction(" . $callback . ",\"" . $http_path . "\", \"" . $error . "\" );
        </script>

        ";
    }

    public function formfilesupload()
    {
        $file = $this->upload('file');
        echo json_encode($file);
    }

    /**
     * удаление файла
     */
    public function delete()
    {
        $id = $this->params['id'];
        $files = new Api_files();
        $files->delete($id);
    }

    /**
     * пометка файла главным
     */
    public function makemain()
    {
        $id = $this->params['id'];
        $staff_id = $this->params['staff_id'];
        if (!empty($id) && !empty($staff_id)) {
            $db = DataBase::getDB();
            $query = "update `staff_files` set `is_main` = 0 where `staff_id` = {?}";
            $db->query($query, [$staff_id]);
            $query = "update `staff_files` set `is_main` = 1 where `file_id` = {?} and `staff_id` = {?}";
            $db->query($query, [$id, $staff_id]);
        }
    }
//TODO перенести в апифайлез
    /**
     * @param $key
     * @return array
     */
    private function upload($key)
    {
        $insertId = '';
        $http_path = '';
        $error = '';
        $preview = '';
        $file_type = null;
        $file_name = Helpers_common::getSafeFileName($_FILES[$key]['name']);
        $file_res = substr($file_name, strrpos($file_name, '.')+1);
        foreach ($this->allowed_types as $allowed_type => $sub_types) {
            if (in_array($file_res, $sub_types)) {
                $file_type = $allowed_type;
                break;
            }
        }
        if ($file_type) {
            $file_name_tmp = $_FILES[$key]['tmp_name'];
            $full_path = './' . $this->path . '/' . $file_name;
            $http_path = '/' . $this->path . '/' . $file_name; // адрес изображения для обращения через http
            if (move_uploaded_file($file_name_tmp, $full_path)) {
                $db = DataBase::getDB();
                $query = "insert into `files` (`title`, `path`, `type`) values ({?}, '{$this->path}', '{$file_type}')";
                $insertId = $db->query($query, [$file_name]);
                //генерим превьювшу для имаджа
                if ($file_type == 'image') {
                    $file_model = new Api_files();
                    $preview = $file_model->getByScheme($this->path . '/' . $file_name, 'admin_prev');
                } else {
                    $preview = '/assets/images/spacer.gif';
                }
            } else {
                $error = 'Не удалось загрузить файл';
            }
        } else {
            $error = 'Не удалось загрузить файл';
        }
        return ['id' => $insertId, 'type' => $file_type, 'path' => $http_path, 'preview' => $preview, 'error' => $error];
    }

    public function reset()
    {
        $files = new Api_files();
        $files->reset_schemes();
        header("Location: " . $_SERVER['HTTP_REFERER']);
        exit;
    }

}