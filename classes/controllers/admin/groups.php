<?php

class Controllers_admin_groups extends Controllers_admin
{
    public $templates = array(
        'create' => 'construction/controllers/groups/create.twig',
        'list' => 'construction/controllers/groups/list.twig'
    );

    //форма создания группы
    public function create_form()
    {
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['create'])
            ->render([]);
    }

    //создание группы
    public function create()
    {
        $params = [
            'title' => isset($_POST['title']) ? $_POST['title'] : '',
            'label' => !empty($_POST['label']) ? $_POST['label'] : Helpers_common::getSafeUrlName($_POST['title']),
            'description' => isset($_POST['description']) ? $_POST['description'] : ''
        ];
        Api_Groups::create($params);
        header('HTTP/1.1 200 OK');
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/groups');
    }

    //список групп
    public function get()
    {
        $groups = Api_Groups::get();
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['list'])
            ->render(['groups' => $groups]);
    }

    //сохранение групп
    public function save()
    {
        foreach ($_POST['group'] as $key => $value) {
            $params = [
                'id' => (int)$key,
                'title' => isset($value['title']) ? $value['title'] : '',
                'label' => !empty($value['label']) ? $value['label'] : Helpers_common::getSafeUrlName($value['title']),
                'description' => isset($value['description']) ? $value['description'] : ''
            ];
            Api_Groups::update($params);
        }
        header('HTTP/1.1 200 OK');
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/groups');
    }
    
}