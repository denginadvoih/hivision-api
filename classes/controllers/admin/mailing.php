<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_mailing extends Controllers_admin
{
    const LIMIT = 20;

    public $templates = array(
        'list' => 'construction/controllers/mailing/list.twig',
        'view' => 'construction/controllers/mailing/view.twig',
        'create' => 'construction/controllers/mailing/create.twig'
    );

    public function mailing () {
        $id = $this->params['id'];
        $mailing = Models_mailings::get_by_id($id);
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['view'])
            ->render(array(
                'mailing' => $mailing
            ));
    }

    public function save_mailing () {
        list($year, $month, $day) = explode('-', $_POST['date_start']);
        $params = [
            'id' => $this->params['id'],
            'date_start' => date('Y-m-d H:i:s', strtotime($year . '-' . $month . '-' . $day)),
            'subject' => $_POST['subject'],
            'message' => $_POST['message']
        ];
        Models_mailings::update($params);
        header('HTTP/1.1 200 OK');
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/mailings');
    }

    public function mailings ()
    {
        $page = empty($_GET['page']) ? 1 : (int)$_GET['page'];
        $params['limit'] = self::LIMIT;
        $params['offset'] = ($page - 1) * $params['limit'];
        $mailings = Models_mailings::get($params);
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['list'])
            ->render(array(
                'mailings' => $mailings['items'],
                'total' => $mailings['total'],
                'current_page' => $page,
                'pages_count' => ceil($mailings['total'] / self::LIMIT)
            ));
    }
    
    public function save_mailings () {

    }

    public function add()
    {
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['create'])
            ->render([]);
    }

    public function create()
    {
        list($year, $month, $day) = explode('-', $_POST['date_start']);
        $params = [
            'date_start' => date('Y-m-d H:i:s', strtotime($year . '-' . $month . '-' . $day)),
            'subject' => $_POST['subject'],
            'message' => $_POST['message']
        ];
        Models_mailings::create($params);
        header('HTTP/1.1 200 OK');
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/mailings');
    }

}