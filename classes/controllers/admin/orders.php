<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_orders extends Controllers_admin
{
    public $templates = array(
        'list' => 'construction/controllers/orders/list.twig',
        'show' => 'construction/controllers/orders/show.twig'
    );
    public $limit = 10;
    public $status = [
        'unchecked' => 'не обработан',
        'checked' => 'обработан',
        'payed' => 'оплачен',
        'completed' => 'выполнен'
    ];

    /**
     * Вывод списка заказов
     */
    public function get()
    {
        $params = [
            'status' => !empty($_GET['status']) ? $_GET['status'] : null,
            'user_id' => isset($_GET['user_id']) ? $_GET['user_id'] : null,
            'show_is_deleted' => isset($_GET['show_is_deleted']) ? true : false
        ];
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $resOrders = Api_Orders::get($params, ($page - 1) * $this->limit, $this->limit);
        $url_params = [];
        krsort($resOrders['orders']);
        foreach ($_GET as $key => $value) {
            if ($key != 'page' && $key != 'route') {
                $url_params[] = $key . '=' . $value;
            }
        }
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['list'])
            ->render(array(
                'orders' => $resOrders['orders'],
                'total' => $resOrders['total'],
                'staff' => $resOrders['staff'],
                'status' => $this->status,
                'current_status' => $params['status'],
                'paginator' => 'construction/modules/paginator.twig',
                'current_page' => $page,
                'pages_count' => ceil($resOrders['total'] / $this->limit)
            ));
    }

    /**
     * Вывод одного заказа
     */
    public function show()
    {
        $order_id = (int)$this->params['id'];
        $orderRes = Api_Orders::get(['id' => $order_id]);
        $order = $orderRes['orders'][$order_id];
        $staff = $orderRes['staff'];
        $referer = $_SERVER['HTTP_REFERER'];
        if (substr_count($referer, SITE_NAME) == 0) {
            $referer = '/site-construction/orders';
        }
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['show'])
            ->render([
                'order' => $order,
                'staff' => $staff,
                'referer' => $referer

            ]);
    }

    /**
     * Вывод одного заказа
     */
    public function update()
    {
        $order_id = (int)$this->params['id'];
        $orderRes = Api_Orders::get(['id' => $order_id]);
        $order = $orderRes['orders'][$order_id];
        if ($_POST['checkOrder'] && $_POST['checkOrder'] == 1 && $order['status'] == 'unchecked') {
            Api_Orders::change_status($order_id, 'checked');
        }
        if (isset($_POST['referer'])) {
            header("Location: " . $_POST['referer']);
        } else {
            header("Location: /site-construction/orders");
        }
    }

    /**
     * Изменение заказов
     */
    public function save()
    {
        foreach ($_POST['status'] as $key => $value) {
            Api_Orders::change_status($key, $value);
        }
        $url_params = [];
        foreach ($_POST['url_params'] as $key => $value) {
            $url_params[] = $key . '=' . $value;
        }
        header("Location: /site-construction/orders?" . implode('&', $url_params));
        exit;
    }


}