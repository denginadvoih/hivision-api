<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_reviews extends Controllers_admin
{
    const LIMIT = 20;

    public $templates = array(
        'reviews' => 'construction/controllers/reviews/reviews.twig'
    );

    //список настроек
    public function get()
    {
        $reviews = Models_reviews::get(['limit' => self::LIMIT, 'offset' => empty($_GET['page']) ? 0 : ((int)$_GET['page']-1) * self::LIMIT]);
        foreach ($reviews['items'] as $review) {
            if (!empty($review['staff_id'])) {
                $staffIds[] = $review['staff_id'];
            }
        }
        $staff = Api_staff::get_staff_by_ids($staffIds);
        //\Pr::p($staff);
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['reviews'])
            ->render(array(
                'reviews' => $reviews,
                'staff' => empty($staff) ? [] : $staff,
                'pages_count' => ceil($reviews['total']/self::LIMIT),
                'current_page' => isset($_GET['page']) ? $_GET['page'] : 1
            ));
    }

    //сохранение настроек
    public function save() {
        $db = DataBase::getDB();
        foreach ($_POST['setting'] as $id => $setting) {
            $query = "update `siteoptions` set `content` = {?} where `id` = {?}";
            $db->query($query, [trim($setting), $id]);
        }
        header('HTTP/1.1 200 OK');
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/settings');
        exit;
    }
}