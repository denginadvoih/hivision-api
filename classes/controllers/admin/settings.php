<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_settings extends Controllers_admin
{
    public $templates = array(
        'settings' => 'construction/controllers/settings/settings.twig',
        'discount' => 'construction/controllers/settings/discount.twig'
    );

    //список настроек
    public function index()
    {
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['settings'])
            ->render(array(
                'settings' => Api_option::get()
            ));
    }

    //сохранение настроек
    public function save()
    {
        $db = DataBase::getDB();
        foreach ($_POST['setting'] as $id => $setting) {
            $query = "UPDATE `siteoptions` SET `content` = {?} WHERE `id` = {?}";
            $db->query($query, [trim($setting), $id]);
        }
        header('HTTP/1.1 200 OK');
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/settings');
        exit;
    }

    //вывод формы со скидками

    public function discount_edit()
    {
        $global_discount = Api_option::get_by_tag('global_discount', 0);
        $size_discount = Api_option::get_by_tag('size_discount', null);
        if (empty($size_discount)) {
            $size_discount = [
                ['sum' => 3000, 'value' => 0],
                ['sum' => 5000, 'value' => 0],
                ['sum' => 10000, 'value' => 0]
            ];
        } else {
            $size_discount = json_decode($size_discount['content'], true);
        }
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['discount'])
            ->render(array(
                'global_discount' => $global_discount,
                'size_discount' => $size_discount
            ));
    }

    //сохранение скидок
    public function discount_save()
    {
        $size_discount = [];
        if (isset($_POST['size_discount'])) {
            foreach ($_POST['size_discount'] as $_discount) {
                $size_discount[] = ['sum' => (int)$_discount['sum'], 'value' => (int)$_discount['value']];
            }
        }
        Api_option::update('size_discount', json_encode($size_discount));
        if (isset($_POST['global_discount'])) {
            Api_option::update('global_discount', $_POST['global_discount']);
        }
        header('HTTP/1.1 200 OK');
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/discount');
    }
}