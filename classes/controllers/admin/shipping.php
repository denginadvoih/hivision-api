<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_shipping extends Controllers_admin
{
    public $templates = array(
        'list' => 'construction/controllers/shipping/list.twig',
        'add' => 'construction/controllers/shipping/add.twig'
    );

    /**
     * Вывод методов доставки
     */
    public function get()
    {
        $methods = Api_Shipping::get();
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['list'])
            ->render(array(
                'methods' => $methods
            ));
    }

    /**
     * Изменение методов доставки
     */
    public function save()
    {
        foreach ($_POST['methods'] as $value) {
            Api_Shipping::save($value);
        }
        header("Location: /site-construction/shipping");
        exit;
    }

    /**
     * Форма добавления метода доставки
     */
    public function add()
    {
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['add'])
            ->render([]);
    }

    /**
     * Добавление метода доставки
     */
    public function create()
    {
        $method = [
            'title' => $_POST['title'],
            'description' => $_POST['description'],
            'mail_message' => $_POST['mail_message'],
            'price' => $_POST['price']
        ];
        Api_Shipping::create($method);
        header("Location: /site-construction/shipping");
        exit;
    }


}