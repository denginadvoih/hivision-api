<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_admin_staff extends Controllers_admin
{
    public $templates = array(
        'add' => 'construction/controllers/staff/add.twig',
        'edit' => 'construction/controllers/staff/edit.twig',
        'price' => 'construction/controllers/staff/price.twig'
    );

    //форма создания объекта
    public function add()
    {
        $this->catalog_id = empty($this->params['section']) ? 1 : $this->params['section'];
        $catalog_info = Models_catalogue::get_catalogue($this->catalog_id);

        $this->content = $this->twig->template
            ->loadTemplate($this->templates['add'])
            ->render(array(
                'catalog' => $catalog_info,
                'catalog_id' => $this->catalog_id
            ));
    }

    //создание объекта
    public function create()
    {
        $db = DataBase::getDB();
        $query = "
        INSERT INTO `staff`
        (
          `sorting`,
          `catalog_id`,
          `title`,
          `second_title`,
          `label`,
          `price`,
          `cashtype`,
          `description`,
          `content`,
          `link`,
          `discuss`,
          `date_insert`,
          `custom_id`
        )
        VALUES
        ({?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?})
        ";
        $params = [
            'sorting' => time(),
            'parent' => isset($_POST['parent']) ? (int)$_POST['parent'] : 1,
            'title' => isset($_POST['title']) ? $_POST['title'] : '',
            'second_title' => isset($_POST['second_title']) ? $_POST['second_title'] : '',
            'label' => empty($_POST['label']) ? Helpers_common::getSafeUrlName($_POST['title']) : $_POST['label'],
            'price' => isset($_POST['price']) ? (int)$_POST['price'] : 0,
            'cashtype' => (!empty($_POST['cashtype']) and (in_array($_POST['cashtype'], ['rub', 'usd', 'yen']))) ? $_POST['cashtype'] : 'rub',
            'description' => isset($_POST['description']) ? trim($_POST['description']) : '',
            'content' => isset($_POST['content']) ? trim($_POST['content']) : '',
            'link' => isset($_POST['link']) ? $_POST['link'] : '',
            'discuss' => isset($_POST['discuss']) ? $_POST['discuss'] : 1,
            'date' => date('Y-m-d H:i:s', time()),
            'custom_id' => isset($_POST['custom_id']) ? (int)$_POST['custom_id'] : null
        ];

        $insert_id = $db->query($query, $params);
        //определяем/корректируем уникальность лэбела
        $query = "SELECT count(*) FROM staff WHERE `label` = '" . $params['label'] . "'";
        $is_unique = $db->selectCell($query);
        if ($is_unique) {
            $query = "UPDATE staff SET `label` = '" . $params['label'] . "-" . $insert_id . "' WHERE `id` = " . $insert_id;
            $db->query($query);
        }

        //работаем с полями
        if (!empty($_POST['fields'])) {
            $field_ids = array_keys($_POST['fields']);
            $fields = Helpers_common::columnAsKey(Models_fields::get_fields($field_ids), 'id');
            $table_field = [
                'integer' => 'value_integer',
                'float' => 'value_float',
                'string' => 'value_string',
                'reference' => 'value_reference',
                'multi_reference' => 'value_reference',
                'double_reference' => 'value_reference'
            ];
            foreach ($_POST['fields'] as $key => $field) {
                $key = (int)$key;
                if (is_array($field)) {
                    foreach ($field as $field_value) {
                        $query = "INSERT INTO `staff_fields_values` (`staff_id`, `field_id`, `" . $table_field[$fields[$key]['type']] . "`)
                VALUES (" . $insert_id . ", " . $key . ", " . $field_value . ") ";
                        $db->query($query);
                    }
                } else {
                    $query = "INSERT INTO `staff_fields_values` (`staff_id`, `field_id`, `" . $table_field[$fields[$key]['type']] . "`)
                VALUES (" . $insert_id . ", " . $key . ", '" . $field . "') ";
                    $db->query($query);
                }

            }
        }
        if (!empty($_POST['fvalue'])) {
            foreach ($_POST['fvalue'] as $fvalue) {
                $query = "INSERT INTO `staff_capitalized` (`staff_id`, `value`) VALUES (" . $insert_id . ", " . (int)$fvalue['value'] . ")";
                $insert_in_stock = $db->query($query);
                if (!empty($fvalue['fields'])) {
                    foreach ($fvalue['fields'] as $field_id => $field) {
                        if (is_array($field)) {
                            foreach ($field as $field_value) {
                                $query = "INSERT INTO `staff_fields_values_capitalized` (`capitalized_staff_id`, `field_id`, `value`) VALUES (" . $insert_in_stock . ", " . (int)$field_id . ", " . (int)$field_value . ")";
                                $db->query($query);
                            }
                        } else {
                            $query = "INSERT INTO `staff_fields_values_capitalized` (`capitalized_staff_id`, `field_id`, `value`) VALUES (" . $insert_in_stock . ", " . (int)$field_id . ", " . (int)$field . ")";
                            $db->query($query);
                        }
                    }
                }


            }
        }

        if (!empty($_POST['files'])) {
            $files = json_decode($_POST['files'], true);
            if (isset($files['items'])) {
                $files['main'] = isset($files['main']) ? $files['main'] : $files['items'][0]['id'];
                foreach ($files['items'] as $file) {
                    $query = "INSERT INTO `staff_files` (`file_id`, `staff_id`, `is_main`, `linked_in`) VALUES (
                " . (int)$file['id'] . ",
                 " . $insert_id . ",
                 " . (($files['main'] == $file['id']) ? 1 : 0) . ",
                 '" . $file['linkedIn'] . "'
                  )";
                    $db->query($query);
                }
            }
        }
        header('HTTP/1.1 200 OK');
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/catalog/' . $params['parent']);
    }

    //форма редактирования объекта
    public function edit()
    {
        $id = $this->params['id'];
        if (!empty($id)) {
            $item = current(Api_staff::get_staff_by_ids([$id]));
            //получаем селектированные id-шники полей справочников, дабы сразу загрузить содержимое дочернего справочника
            $selected_reference_fields = [];
            foreach ($item['fields'] as $key => $field) {
                if ($field['type'] == 'double_reference') {
                    foreach ($field['values'] as $field_value) {
                        if (isset($field_value['id'])) {
                            $selected_reference_fields[$field['id']][] = $field_value['id'];
                        }
                    }
                }
            }
            foreach ($item['capitalized'] as $capitalized) {
                foreach ($capitalized['fields'] as $field) {
                    if ($field['type'] == 'double_reference') {
                        foreach ($field['values'] as $field_value) {
                            if (isset($field_value['id'])) {
                                $selected_reference_fields[$field['id']][] = $field_value['id'];
                            }
                        }
                    }
                }
            }
            $catalog = Models_catalogue::get_catalogue_expand($item['catalog_id'], $selected_reference_fields);
            $this->content = $this->twig->template
                ->loadTemplate($this->templates['edit'])
                ->render(array(
                    'catalog' => $catalog,
                    'item' => $item,
                    'fields_template' => 'construction/modules/field-form.twig'
                ));
        }
    }

    //сохранение объекта
    public function save()
    {
        $id = (int)$this->params['id'];
        $db = DataBase::getDB();
        $query = "
        update `staff`
        set
          `sorting`={?},
          `catalog_id`={?},
          `title`={?},
          `second_title`={?},
          `label`={?},
          `price`={?},
          `cashtype`={?},
          `description`={?},
          `content`={?},
          `link`={?},
          `discuss`={?},
          `date_insert`={?},
          `custom_id`={?}
        where `id` =$id
        ";
        $params = [
            'sorting' => isset($_POST['sorting']) ? (int)$_POST['sorting'] : 0,
            'parent' => isset($_POST['parent']) ? (int)$_POST['parent'] : 1,
            'title' => isset($_POST['title']) ? $_POST['title'] : '',
            'second_title' => isset($_POST['second_title']) ? $_POST['second_title'] : '',
            'label' => empty($_POST['label']) ? Helpers_common::getSafeUrlName($_POST['title']) : $_POST['label'],
            'price' => isset($_POST['price']) ? (int)$_POST['price'] : 0,
            'cash' => (!empty($_POST['cash']) and (in_array($_POST['cash'], ['rub', 'usd', 'yen']))) ? $_POST['cash'] : 'rub',
            'description' => isset($_POST['description']) ? trim($_POST['description']) : '',
            'content' => isset($_POST['content']) ? trim($_POST['content']) : '',
            'link' => isset($_POST['link']) ? $_POST['link'] : '',
            'discuss' => isset($_POST['discuss']) ? $_POST['discuss'] : 1,
            'date' => date('Y-m-d H:i:s', time()),
            'custom_id' => isset($_POST['custom_id']) ? (int)$_POST['custom_id'] : null
        ];
        $db->query($query, $params);

        $query = "SELECT count(*) FROM staff WHERE `label` = '" . $params['label'] . "' AND `id`!=" . $id;
        $is_unique = $db->selectCell($query);
        if ($is_unique) {
            $query = "UPDATE staff SET `label` = '" . $params['label'] . "-" . $id . "' WHERE `id` = " . $id;
            $db->query($query);
        }

        //отрабатываем состояние объекта, наличие, отсутствует и прочее...
        if (!empty($_POST['condition'])) {
            $db->query("delete from `staff_condition` where `staff_id`=" . $id);
            $db->query("insert into `staff_condition` (`staff_id`, `condition`) values ({$id}, '" . $_POST['condition'] . "')");
        }
        //работаем с полями
        //очищаем таблицу
        $query = "delete from `staff_fields_values` where `staff_id`=$id";
        $db->query($query);
        //собираем, вставляем значения
        if (!empty($_POST['fields'])) {
            $field_ids = array_keys($_POST['fields']);
            $fields = Helpers_common::columnAsKey(Models_fields::get_fields($field_ids), 'id');
            $table_field = [
                'integer' => 'value_integer',
                'float' => 'value_float',
                'string' => 'value_string',
                'text' => 'value_string',
                'reference' => 'value_reference',
                'multi_reference' => 'value_reference',
                'double_reference' => 'value_reference'
            ];
            foreach ($_POST['fields'] as $key => $field) {
                $key = (int)$key;
                if (is_array($field)) {
                    foreach ($field as $field_value) {
                        $query = "INSERT INTO `staff_fields_values` (`staff_id`, `field_id`, `" . $table_field[$fields[$key]['type']] . "`)
                VALUES (" . $id . ", " . $key . ", " . $field_value . ") ";
                        $db->query($query);
                    }
                } else {
                    $query = "INSERT INTO `staff_fields_values` (`staff_id`, `field_id`, `" . $table_field[$fields[$key]['type']] . "`)
                VALUES (" . $id . ", " . $key . ", '" . $field . "') ";
                    $db->query($query);
                }

            }
        }
        //работаем с приходными позициями
        //очищаем таблицу с описанием полей приходов
        $query = "DELETE FROM `staff_fields_values_capitalized` WHERE `capitalized_staff_id`
        IN (SELECT `id` FROM `staff_capitalized` WHERE `staff_id`=" . $id . ")";
        $db->query($query);

        //собираем вставляем значения
        if (!empty($_POST['fvalue'])) {
            foreach ($_POST['fvalue'] as $fkey => $fvalue) {
                if (is_numeric($fkey)) {
                    $query = "UPDATE `staff_capitalized` SET `value`=" . (int)$fvalue['value'] . " WHERE `id` = " . $fkey;
                    $db->query($query);
                    $insert_in_stock = $fkey;
                } else {
                    $query = "INSERT INTO `staff_capitalized` (`staff_id`, `value`) VALUES (" . $id . ", " . (int)$fvalue['value'] . ")";
                    $insert_in_stock = $db->query($query);
                }
                if (!empty($fvalue['fields'])) {
                    foreach ($fvalue['fields'] as $field_id => $field) {
                        if (is_array($field)) {
                            foreach ($field as $field_value) {
                                $query = "INSERT INTO `staff_fields_values_capitalized` (`capitalized_staff_id`, `field_id`, `value`) VALUES (" . $insert_in_stock . ", " . (int)$field_id . ", " . (int)$field_value . ")";
                                $db->query($query);
                            }
                        } else {
                            $query = "INSERT INTO `staff_fields_values_capitalized` (`capitalized_staff_id`, `field_id`, `value`) VALUES (" . $insert_in_stock . ", " . (int)$field_id . ", " . (int)$field . ")";
                            $db->query($query);
                        }
                    }
                }
            }
        }
        //работаем с файлами
        //очищаем таблицы с приходами
        $query = "delete from `staff_files` where `staff_id`=$id";
        $db->query($query);
        if (!empty($_POST['files'])) {
            $files = json_decode($_POST['files'], true);
            if (isset($files['items'])) {
                $files['main'] = isset($files['main']) ? $files['main'] : 0;
                foreach ($files['items'] as $file) {
                    $query = "INSERT INTO `staff_files` (`file_id`, `staff_id`, `is_main`, `linked_in`) VALUES (
                " . (int)$file['id'] . ",
                 " . $id . ",
                 " . (($files['main'] == $file['id']) ? 1 : 0) . ",
                 '" . $file['linkedIn'] . "'
                  )";
                    $db->query($query);
                }
            }
        }
        header('HTTP/1.1 200 OK');
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction/catalog/' . $params['parent']);
    }

    private function getCatalogues($id)
    {
        $parents = [];
        $db = DataBase::getDB();
        $query = "SELECT `parent_id` FROM `catalog` WHERE `id`={?}";
        $parent_id = $db->selectCell($query);
        if (!empty($parent_id)) {
            $parents = $this->getCatalogues($parent_id);
        }
        $parents[] = $id;
        return $parents;
    }

    public function catalogue_indexing($id = null)
    {

        $db = DataBase::getDB();
        $parents = [];
        //очищаем таблицу
        $query = "";
        $db->query($query);
        $query = "SELECT `id`, `catalog_id` FROM staff";
        $items = $db->select($query);
        foreach ($items as $item) {
            if (!isset($parents[$item['catalog_id']])) {
                $parents[$item['catalog_id']] = $this->getCatalogues($item['catalog_id']);
            }
        }
    }

    /**
     * Форма массового изменения цены
     */
    public function price_edit()
    {
        $this->content = $this->twig->template
            ->loadTemplate($this->templates['price'])
            ->render([]);
    }

    /**
     * Изменение цены
     */
    public function price_save()
    {
        $db = DataBase::getDB();
        if (isset($_POST['price']) && $_POST['price'] == 0 && isset($_POST['oldPrice']) && $_POST['oldPrice'] == 1) {
            $query = "UPDATE `staff` SET `old_price` = 0";
        }
        if (!empty($_POST['price'])) {
            $query = "UPDATE `staff` SET " . (($_POST['oldPrice'] == 1) ? "`old_price` = `price`," : '');
            $query .= ' `price` = ROUND(`price` + `price`/100*' . (int)$_POST['price'] . ",0)";
        }

        if (isset($query)) {
            $db->query($query);
        }
        header('HTTP/1.1 200 OK');
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/site-construction');
    }


}