<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */

class Controllers_ajax
{
    public $branch; //массив элементов каталога, от текущего до корневого
    public $url; //текущий урл
    public $params; //параметры search, page и прочее
    public $current_catalog; //текущий каталог
    public $object; //url извлекаемого объекта

    const TIME_TO_PROCESS = 5;


    public function __construct($branch, $params, $url, $object)
    {
        $this->branch = $branch;
        $this->params = $params;
        $this->current_catalog = end($this->branch);
        $this->url = $url;
        $this->object = $object;
    }

    public static function backmail()
    {
        //TODO макет сообщения через twig
        //TODO что то сделать на предмет спама
        //TODO указывать с какой страницы пришло сообщение
        $mail = Api_option::get_by_tag('email');
        $sitename = Api_option::get_by_tag('sitename');
        $mailes = explode(',', $mail['status']);
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=utf-8\r\n";
        $headers .= "From: Форма обратной связи на сайте<mail-service@{$sitename}>\r\n";
        $mess = '<p>' . str_replace("\n", '<br>', $_POST['message']) . "</p>
            <p>Прислано со страницы сайта " . $sitename . "</p>
            <p>Контактное лицо: " . $_POST['name'] . '</p>
            <p>Ответ на это сообщение отправьте по адресу: ' . $_POST['mail'] . '</p>';

        foreach ($mailes as $value) {
            mail($value, 'Сообщение со страниц сайта', $mess, $headers);
        }
        echo json_encode(array('data' => array(), 'error' => 0));
    }

    public static function requestsend()
    {
        $id = (int)$_POST['id'];
        if (empty($id)) {
            die();
        }
        $request = Api_requests::get_request($id);
        $fields = [];
        foreach ($_POST['data'] as $value) {
            if (preg_match('/fields\[(\d+)\]/', $value['name'], $res)) {
                if (is_string($value['value'])) {
                    $fields[$res[1]]['value'] = $value['value'];
                }
            }
        }
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=utf-8\r\n";
        $headers .= "From: Форма обратной связи на сайте<mail-service@" . SITE_NAME . ">\r\n";
        $twig = Twig::get_instance();
        $message = $twig->template
            ->loadTemplate('modules/request_message.twig')
            ->render(['request' => $request, 'fields' => $fields]);
        mail($request['mail'], $request['title'], $headers, $message);
        echo json_encode(array('data' => array(), 'error' => 0));
    }

    //кастомные запросы разных заказчиков
    public function mmc_tracker()
    {
        require_once './vendors/phpQuery.php';
        $tracker = $_POST['tracker'];
        $content = file_get_contents('https://www.mcc.com.sg/tracking?trackingNumber=' . $tracker);
        $result = [];
        $document = phpQuery::newDocument($content);
        $items = $document->find('#shipment-tracking-table');
        foreach ($items as $key => $value) {
            $table = pq($value);
            $cells = $table->find('.shipment-tracking-table__row td');
            foreach ($cells as $cell) {
                $cell = pq($cell);
                $cell->find('img')->remove();
                $result[] = $cell->html();
            }
        }
        $result = array_map('trim', $result);
        $result = array_map(function ($el) {
            return str_replace('<br>', ' - ', $el);
        }, $result);
        $result = array_filter($result);
        echo json_encode($result);
    }

    public function get_cities()
    {
        $region_id = $this->params['id'];
        $cities = Api_regions::get_cities_by_region_id($region_id);
        echo json_encode($cities);
    }

    public function create_review()
    {
        if (!isset($_SESSION['on_reviews'])
            or (time() - $_SESSION['on_reviews'] < self::TIME_TO_PROCESS)
            or (empty($_POST['message']))
            or (strlen($_POST['message']) < 6)
        ) {
            $_POST['error'] = $_SESSION['on_reviews'] . '-' . time();
            echo json_encode($_POST);
        } else {
            $auth = Auth_auth::get_instance();
            $user = $auth->getUser();
            $data = [
                'user_name' => $user['type'] == 'registered' ? $user['name'] : $_POST['name'],
                'user_type' => $user['type'],
                'user_id' => $user['id'],
                'staff_id' => (int)$_POST['staff_id'],
                'catalogue_id' => (int)$_POST['catalogue_id'],
                'message' => $_POST['message']
            ];
            unset($_SESSION['on_reviews']);
            if (Models_reviews::create($data)) {
                echo json_encode($_POST);
            } else {
                echo json_encode(['error' => 'Ошибка добавления отзыва']);
            }
        }
    }

    public function update_review()
    {
        $id = $this->params['id'];
        $review = Models_reviews::get_by_id($id);
        $data = [
            'id' => $id,
            'approved' => isset($_POST['approved']) ? ($review['approved'] == 0 ? 1 : 0) : $review['approved'],
            'message' => isset($_POST['message']) ? $_POST['message'] : $review['message'],
            'answer' => isset($_POST['answer']) ? $_POST['answer'] : $review['answer']
        ];
        if (Models_reviews::update($data)) {
            echo json_encode($_POST);
        } else {
            echo json_encode(['error' => 'Ошибка редактирования отзыва']);
        }
    }

    public function update_discount()
    {
        $id = $this->params['id'];
        $discount = (int)$_POST['discount'];
        if (Models_users::update_discount($id, $discount)) {
            echo json_encode($_POST);
        } else {
            echo json_encode(['error' => 'Ошибка редактирования скидки']);
        }
    }

    public function check_email() 
    {
        $is_not_unique = null;
        if (isset($_GET['email'])) {
            $db = DataBase::getDB();
            $is_not_unique = $db->selectCell("select count(*) from `users` where `email`={?} and `verified`=1", [$_GET['email']]);
        }
        echo ($is_not_unique != 0) ? json_encode('Данный email не уникален и уже содержится в нашей базе. Забыли пароль?') : json_encode(true);
    }
} 