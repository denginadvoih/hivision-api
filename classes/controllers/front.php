<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_front
{
    public $url; //текущий урл
    public $url_array; //текущий массив url
    public $params; //параметры search, page и прочее
    public $twig;
    public $structure_template;
    public $content;
    public $branch;
    public $catalog_id = 1;
    public $current_catalog;
    public $catalog;
    public $object;

    function __construct($url_array, $params, $url, $object)
    {
        $this->params = $params;
        $this->url_array = $url_array;
        $this->url = $url;
        $this->object = $object;
        $this->twig = new Twig();
    }

    public function index()
    {
        //если не нашли соответствующей структуры то переходим на 404 ошибку
        if (!is_array($this->branch)) {
            header("HTTP/1.0 404 Not Found");
            echo 'Страница не найдена';
            exit;
        }
    }

    public function core()
    {
        //если не нашли соответствующей структуры то переходим на 404 ошибку
        
    }

    public function not_work()
    {
        $this->structure_template = 'not_work.html';
        $this->content = 'Сайт в разработке!';
        $this->current_catalog = $this->catalog_id = 1;
    }

    function __destruct()
    {

        //Сначала разрешим принимать и отправлять запросы на сервер А
       // header('Access-Control-Allow-Origin: *', true);
//Установим типы запросов, которые следует разрешить (все неуказанные будут отклоняться)
//Разрешим передавать Cookie и Authorization заголовки для указанновго в Origin домена
        header('Access-Control-Allow-Credentials: true', true);
//Установим заголовки, которые можно будет обрабатывать
        header('Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Accept, X-PINGOTHER, Content-Type', true);
        header('Content-Type: application/json', true);
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE', true);
            if (!empty($this->content)) {
            if (is_array($this->content)) {
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($this->content);
            } else {
                echo $this->content;
            }
        }
        exit;
    }
} 