<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */

class Controllers_front_basket_ajax
{
    public $branch; //массив элементов каталога, от текущего до корневого
    public $url; //текущий урл
    public $params; //параметры search, page и прочее
    public $current_catalog; //текущий каталог
    public $object; //url извлекаемого объекта


    public function __construct($branch, $params, $url, $object)
    {
        $this->branch = $branch;
        $this->params = $params;
        $this->current_catalog = end($this->branch);
        $this->url = $url;
        $this->object = $object;
    }

    //добавление в корзину
    public function add()
    {
        $db = DataBase::getDB();
        $capitalized = (int)$_POST['capitalized'];
        $quantity = (int)$_POST['quantity'];

        $id = (int)$this->params['id'];
        //TODO надо какойто ошибочный респонс для ajax запросов оформить чтобы самому хедер не писать
        if (empty($id)) {
            header('HTTP/1.1 400 Bad Request');
            die(json_encode(['message' => 'Не верные входные параметры']));
        }
        $staff = current(Api_staff::get_staff_by_ids([$id]));
        if (count($staff['capitalized']) > 0 && empty($capitalized)) {
            header('HTTP/1.1 400 Bad Request');
            die(json_encode(['message' => 'Не верные входные параметры']));
        }
        $auth = Auth_auth::get_instance();
        $user = $auth->getUser();
        if (empty($user['id'])) {
            header('HTTP/1.1 401 Unauthorized');
            die(json_encode(['message' => 'Ошибка аутентификации']));
        }
        $query = "insert into `basket`
        (`staff_id`, `stock_id`, `quantity`, `user_id`, `user_type`)
        values
        ({?},{?}, {?}, {?}, {?})
        ";
        $db->query($query, [$id, $capitalized, $quantity, $user['id'], $user['type']]);
        $basket = Api_Basket::basket_state();
        echo json_encode($basket);
    }

    //удаление элемента из корзины
    public function remove()
    {
        $db = DataBase::getDB();
        $id = (int)$this->params['id'];
        if (empty($id)) {
            header('HTTP/1.1 400 Bad Request');
            die(json_encode(['message' => 'Не верные входные параметры']));
        }
        $query = "delete from `basket` where `id`={?}";
        $db->query($query, [$id]);
        $basket = Api_Basket::basket_state();
        echo json_encode($basket);
    }

    //изменение количества положенных в корзину сущностей
    public function change()
    {
        $db = DataBase::getDB();
        $id = (int)$this->params['id'];
        $change = $this->params['change'] == 'plus' ? '+' : '-';
        if (empty($id)) {
            header('HTTP/1.1 400 Bad Request');
            die(json_encode(['message' => 'Не верные входные параметры']));
        }
        $query = "update `basket` set `quantity`=`quantity`"  . $change . " 1 where `id`={?}";
        $db->query($query, [$id]);
        $basket = Api_Basket::basket_state();
        echo json_encode($basket);
    }
} 