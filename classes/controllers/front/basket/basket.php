<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_front_basket_basket extends Controllers_front
{
    public function prepare()
    {
        if ($this->current_catalog['template'] == '') $this->current_catalog['template'] = 'ss_second_fullsize.html';
        if ($this->current_catalog['patriarh'] == '') $this->current_catalog['patriarh'] = '1';

        if (isset($this->current_catalog['meta'])) {
            $this->current_catalog['meta'] = unserialize($this->current_catalog['meta']);
        }

        $this->structure_template = 'ss_second_fullsize.html';
    }

    public function index()
    {
        $this->prepare();
        $twig = Twig::get_instance();
        $items = Api_Basket::current_customer();
        $sum = 0;
        $discount = Models_shop::discount();
        //определяем сумму
        foreach ($items as $item) {
            $sum += $item['quantity'] * $item['staff_item']['price'];
        }
        $_size_discount_value = 0;
        if (is_array($discount['size'])) {
            foreach ($discount['size'] as $size_discount) {
                if ($sum > $size_discount['sum']) {
                    $_size_discount_value = $size_discount['value'];
                }
            }
            $discount['size'] = $_size_discount_value;
        }
        //определяем максимальную скидку
        $currentDiscount = ['value' => max($discount)];
        $currentDiscount['type'] = array_search($currentDiscount['value'], $discount);
        if ($currentDiscount['type'] == 'size') {
            $discountSum = $sum - ($sum/100*$currentDiscount['value']);
        } else {
            $discountSum = 0;
            foreach ($items as $item) {
                $discountSum += ($item['old_price'] > $item['price'])
                    ? ($item['quantity'] * $item['staff_item']['price'])
                    : ($item['quantity'] * $item['staff_item']['price']) - (($item['quantity'] * $item['staff_item']['price'])/100*$currentDiscount['value']);
            }
        }
        $this->content = $twig->template
            ->loadTemplate('basket/list.twig')
            ->render([
                'items' => $items,
                'sum' => $sum,
                'discountSum' => $discountSum,
                'discount' => $currentDiscount
            ]);
    }

    public function confirm_form()
    {
        $this->prepare();
        $twig = Twig::get_instance();
        $auth = Auth_auth::get_instance();
        $user = $auth->getUser();
        $regions = Api_regions::get_regions();
        $cities = [];
        if ($user['type'] == 'registered') {
            $userContacts = $auth->getUserContacts($user['id']);
            $userContacts['email'] = $auth->getEmail();
            $_temp = Helpers_arr::array_func_search($regions, function ($val) use ($userContacts) {
                //echo $val['title'] . ' ' . $userContacts['region'] . '<br>';
                if ($val['title'] == $userContacts['region']) {
                    return true;
                } else {
                    return false;
                }
            });
            if ($_temp) {
                $userContacts['region_id'] = $regions[$_temp]['id'];
                $cities = Api_regions::get_cities_by_region_id($userContacts['region_id']);
                $_temp = Helpers_arr::array_func_search($cities, function ($val) use ($userContacts) {
                    if ($val['title'] == $userContacts['city']) {
                        return true;
                    } else {
                        return false;
                    }
                });
                if ($_temp) {
                    $userContacts['city_id'] = $cities[$_temp]['id'];
                }
            }
        } else {
            $userContacts = [];
        }
        //определяем не кастомный ли город
        if (!empty($userContacts['city']) && empty($userContacts['city_id'])) {
            $customCity = true;
        } else {
            $customCity = false;
        }
        //получаем товары из корзины
        $items = Api_Basket::current_customer();
        $sum = 0;
        $discount = Models_shop::discount();
        foreach ($items as $item) {
            $sum += $item['quantity'] * $item['staff_item']['price'];
        }
        if (is_array($discount['size']))$_size_discount_value = 0;
        if (is_array($discount['size'])) {
            foreach ($discount['size'] as $size_discount) {
                if ($sum > $size_discount['sum']) {
                    $_size_discount_value = $size_discount['value'];
                }
            }
            $discount['size'] = $_size_discount_value;
        }
        $currentDiscount = ['value' => max($discount)];
        $currentDiscount['type'] = array_search($currentDiscount['value'], $discount);
        if ($currentDiscount['type'] == 'size') {
            $discountSum = $sum - ($sum/100*$currentDiscount['value']);
        } else {
            $discountSum = 0;
            foreach ($items as $item) {
                $discountSum += ($item['old_price'] > $item['price'])
                    ? ($item['quantity'] * $item['staff_item']['price'])
                    : ($item['quantity'] * $item['staff_item']['price']) - (($item['quantity'] * $item['staff_item']['price'])/100*$currentDiscount['value']);
            }
        }
        $shipping = Api_Shipping::get(['is_deleted' => 0]);
        $this->content = $twig->template
            ->loadTemplate('basket/confirm.twig')
            ->render([
                'items' => $items,
                'sum' => $sum,
                'discountSum' => $discountSum,
                'discount' => $currentDiscount,
                'regions' => $regions,
                'cities' => $cities,
                'contacts' => $userContacts,
                'customCity' => $customCity,
                'shipping' => $shipping
            ]);
    }

    public function confirm()
    {
        $auth = Auth_auth::get_instance();
        $user = $auth->getUser();
        $params = [
            'family' => $_POST['family'],
            'name' => $_POST['name'],
            'surname' => $_POST['surname'],
            'email' => $_POST['email'],
            'phone' => isset($_POST['phone']) ? preg_replace('/[^0-9]/', '', $_POST['phone']) : '',
            'postindex' => isset($_POST['postindex']) ? $_POST['postindex'] : '',
            'street' => isset($_POST['street']) ? $_POST['street'] : '',
            'house' => isset($_POST['house']) ? $_POST['house'] : '',
            'flat' => isset($_POST['flat']) ? $_POST['flat'] : ''
        ];
        $shipping = isset($_POST['shipping']) ? $_POST['shipping'] : null;
        $region = isset($_POST['region']) ? $_POST['region'] : '';
        if (is_numeric($region)) {
            $_region = Api_regions::get_region($region);
            if (is_array($_region)) {
                $params['region'] = $_region['title'];
            }
        }
        if (isset($_POST['city_is_custom'])) {
            $params['city'] = isset($_POST['custom_city']) ? $_POST['custom_city'] : '';
        } else {
            $city = isset($_POST['city']) ? $_POST['city'] : '';
            if (is_numeric($city)) {
                $_city = Api_regions::get_city($city);
                if (is_array($_city)) {
                    $params['city'] = $_city['title'];
                }
            }
        }
        $order_id = Api_Basket::create_order($user, $shipping, $params);
        Api_Orders::send_customer_mail($order_id);
        Api_Orders::send_admin_mail($order_id);
        header('HTTP/1.1 200 OK');
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/orders/send');
    }

    public function send()
    {
        $this->prepare();
        $twig = Twig::get_instance();
        $this->content = $twig->template
            ->loadTemplate('basket/send.twig')
            ->render([]);
    }
}