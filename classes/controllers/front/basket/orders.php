<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_front_basket_orders extends Controllers_front
{
    private $mrh_login = "avantishoes";
    private $mrh_pass1 = "avantipassto1";
    private $mrh_pass2 = "avantipassout2";
    private $mrh_pass_test1 = "avantipassto1test";
    private $mrh_pass_test2 = "avantipassout2test";
    private $is_test = 1;
    // code of goods
    private $shp_item = "2";
    // предлагаемая валюта платежа
    private $in_curr = "PCR";
    // language
    private $culture = "ru";

    //host
    private $server = "https://merchant.roboxchange.com/Index.aspx";

    //test host
    private $test_server = "http://test.robokassa.ru/Index.aspx";


    public function prepare()
    {
        if ($this->current_catalog['template'] == '') $this->current_catalog['template'] = 'ss_second.html';
        if ($this->current_catalog['patriarh'] == '') $this->current_catalog['patriarh'] = '1';

        if (isset($this->current_catalog['meta'])) {
            $this->current_catalog['meta'] = unserialize($this->current_catalog['meta']);
        }

        $this->structure_template = 'ss_second.html';
    }

    /**
     * Форма оплаты заказа
     */
    public function pay()
    {
        $this->prepare();
        $twig = Twig::get_instance();
        $_order = Api_Orders::get(['id' => $this->params['id']]);
        $order = $_order['orders'][$this->params['id']];

        // generate signature
        if ($this->is_test == 1) {
            $pass1 = $this->mrh_pass_test1;
            $pass2 = $this->mrh_pass_test2;
        } else {
            $pass1 = $this->mrh_pass1;
            $pass2 = $this->mrh_pass2;
        }
        $crc = md5($this->mrh_login . ':' . $order['sum'] . ':' . $order['id'] . ':' . $pass1);
        //\Pr::p_($order['orders'][$this->params['id']]);
        $this->content = $twig->template
            ->loadTemplate('orders/pay.twig')
            ->render([
                    'order' => $order,
                    'inv_desc' => 'Оплата покупок в магазине ' . SITE_NAME,
                    'mhr_login' => $this->mrh_login,
                    'mhr_pass1' => $pass1,
                    'in_curr' => $this->in_curr,
                    'culture' => $this->culture,
                    'crc' => $crc,
                    'server' => $this->server,
                    'isTest' => $this->is_test
                ]
            );
    }

    /**
     * Результат оплаты
     */
    public function pay_result()
    {
        if ($this->is_test == 1) {
            $mrh_pass2 = $this->mrh_pass_test2;
        } else {
            $mrh_pass2 = $this->mrh_pass2;
        }
        // HTTP parameters:
        $out_summ = $_REQUEST["OutSum"];
        $order_id = $_REQUEST["InvId"];
        $crc = strtoupper($_REQUEST["SignatureValue"]);
        // build own CRC
        $my_crc = strtoupper(md5("$out_summ:$order_id:$mrh_pass2"));
        if ($my_crc != $crc)
        {
            echo "bad sign\n";
            exit();
        }
        // print OK signature
        echo "OK$order_id\n";
        // perform some action (change order state to paid)
        $_order = Api_Orders::get(['id' => $order_id]);
        $order = $_order['orders'][$order_id];
        if ($out_summ == $order['sum']) {
            Api_Orders::change_status($order_id, 'payed');
        }
    }

    /**
     * Удачная оплата SuccessUrl
     */
    public function pay_is_ok()
    {
        $this->prepare();
        $twig = Twig::get_instance();
        $this->content = $twig->template
            ->loadTemplate('orders/pay_is_ok.twig')
            ->render([]);
    }
}