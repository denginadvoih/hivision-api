<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_front_catalogue extends Controllers_front
{
    public $page_size = 20;
    public $default_gallery_catalog = 1391;

    public function prepare()
    {
        $this->branch = Models_catalogue::get_structure_by_url($this->url_array);
        if (is_array($this->branch)) {
            $this->current_catalog = end($this->branch);
        } else {
            $this->current_catalog = [];
        }
        if (!is_array($this->branch)) {
            header("HTTP/1.0 404 Not Found");
            echo 'Страница не найдена';
            exit;
        }
    }

    public function viewcontent()
    {
        $this->prepare();

        //TODO впилить получени получение полей в данную функцию, или выпилить из второй
        $_SESSION['view_content_start'] = time();
        $staff = Api_staff::get_staff_by(
            [
                'catalog_id' => $this->current_catalog['id'],
                'fields' => isset($_GET['fields']) ? $_GET['fields'] : [],
                'cfields' => isset($_GET['cfields']) ? $_GET['cfields'] : [],
                'search' => isset($_GET['search']) ? $_GET['search'] : '',
                'is_deleted' => 0,
                'is_main' => true,
                'scheme' => array_merge(['staff_prev', 'rectangle_medium'], isset($_GET['schemes']) ? explode(',', $_GET['schemes']) : [])
            ],
            $this->params['offset'] ?? 0,
            $this->params['limit'] ?? $this->page_size
        );
        if (is_array($staff['items']) && !empty($staff['items'])) {
            $fields = Api_staff::get_staff_by_ids(array_map(function ($item) {
                return $item['id'];
            }, $staff['items']));
            foreach ($staff['items'] as &$value) {
                $value['date'] = Helpers_common::convert_date($value['date_insert'], 'en');
                if (isset($fields[$value['id']])) {
                    $value['fields'] = $fields[$value['id']]['fields'];
                    $value['capitalized'] = $fields[$value['id']]['capitalized'];
                }
            }
        } else {
            $staff['items'] = array();
        }
        unset($value);
        $total = $staff['total'];
        $query_pieces = explode('&', urldecode($_SERVER['QUERY_STRING']));
        foreach ($query_pieces as $query_piece) {
            list($key, $value) = explode('=', $query_piece);
            if ($key != 'page' && $key != 'route') {
                $url_params[] = $query_piece;
            }
        }


        $query_string = empty($url_params) ? '' : '?' . implode('&', $url_params);
        $this->content = [
            'catalog' => Models_catalogue::get_catalogue_expand($this->current_catalog['id']),
            'children' => Models_catalogue::get_catalogues_by_parent($this->current_catalog['id']),
            'query_string' => $query_string,
            'items' => $staff['items'],
            'total' => $total,
            'params' => $this->params,
            'branch' => $this->branch

        ];
    }

    public function viewobject()
    {
        $this->prepare();
        $schemes = $this->params['schemes'];
        $item = Api_staff::get_one_of_staff_by_label($this->object, $schemes ? explode(',', $schemes) : ['staff_prev', 'staff_prev_4-3', 'rectangle_medium', 'rectangle_long']);
        $this->content = [
            'catalog' => $this->current_catalog,
            'url' => $this->url,
            'item' => $item,
            'branch' => $this->branch
        ];
    }

    public function tree()
    {
        $tag = $this->params['tag'];
        $parent_catalogue = Models_catalogue::get_catalogue_by_tag($tag);
        $this->content = Models_catalogue::full_catalogue_tree(null, $parent_catalogue['id'], 0);
    }
}