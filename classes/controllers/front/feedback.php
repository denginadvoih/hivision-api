<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_front_feedback extends Controllers_front
{

    const MANAGER_EMAIL = 'opt.hivision.russia@gmail.com';
    const MANAGER_SECOND_EMAIL = 'hivision.russia@gmail.com';
    const TEST_EMAIL = 'denginadvoih@gmail.com';

    function __construct($catalog, $params)
    {
        $this->catalog = $catalog;
        $this->params = $params;
    }

    /**
     * Вывод страницы с отзывами
     */
    public function send()
    {
        $twig = Twig::get_instance();
        $request = json_decode(file_get_contents('php://input'), true);
        $_response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . Constants::RECAPTCHA_SECRETKEY . "&response={$request['token']}");
        $captchaData = json_decode($_response, true);
        if (isset($captchaData['success']) && $captchaData['success'] == true) {
            $message = $twig->template
                ->loadTemplate('mail_templates/feedback_mail.twig')
                ->render([
                    'phone' => $request['phone'],
                    'name' => $request['name']
                ]);
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=utf-8\r\n";
            $headers .= "From: " . SITE_NAME . "<feedback@" . SITE_NAME . ">\r\n";
            $subject = 'Форма обратной связи на сайте ' . SITE_NAME;
            //mail(self::MANAGER_EMAIL, $subject, $message, $headers);
            //mail(self::MANAGER_SECOND_EMAIL, $subject, $message, $headers);
            mail(self::TEST_EMAIL, $subject, $message, $headers);
            $this->content = ['Ok'];
        } else {
            $this->content = ['Error'];
        }
    }

    /**
     * Заявка на получение обратной информации
     */
    public function sendRequestForDetailedInfo()
    {
        //check mail
        mail(self::TEST_EMAIL, 'My Subject', 'Check the mail is working');
        $twig = Twig::get_instance();
        $request = json_decode(file_get_contents('php://input'), true);
        $_response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . Constants::RECAPTCHA_SECRETKEY . "&response={$request['token']}");
        $captchaData = json_decode($_response, true);
        if (isset($captchaData['success']) && $captchaData['success'] == true) {
            $message = $twig->template
                ->loadTemplate('mail_templates/feedback_mail_request.twig')
                ->render([
                    'phone' => $request['phone'],
                    'name' => $request['name'],
                    'email' => $request['email'],
                    'city' => $request['city'],
                    'cooperationType' => $request['cooperationType']

                ]);
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=utf-8\r\n";
            $headers .= "From: " . SITE_NAME . "<feedback@" . SITE_NAME . ">\r\n";
            $subject = 'Запрос на детальную информацию о сотрудничестве на сайте ' . SITE_NAME;
            try {
                if ($request['cooperationType'] == 'retailpartner') {
                    mail(self::MANAGER_SECOND_EMAIL, $subject, $message, $headers);
                }
                if ($request['cooperationType'] == 'optpartner' || $request['cooperationType'] == 'dealer') {
                    mail(self::MANAGER_EMAIL, $subject, $message, $headers);
                }
            } catch (Exception $e) {
                $this->content = [$e->getMessage()];
                return;
            }


            $this->content = ['Ok'];
        } else {
            $this->content = ['Error'];
        }


    }
}