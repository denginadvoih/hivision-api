<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_front_fields extends Controllers_front
{
    public function get()
    {
        $field = $this->params['field'];
        $this->content = Models_fields::get_field($field);
    }
}