<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_front_lampreference extends Controllers_front
{
    const TIME_TO_PROCESS = 5;

    public $page_size = 10;

    public function car_models()
    {
        $mark = $this->params['mark'];
        $this->content = json_decode(file_get_contents('http://koitolamp.ru/catalog/children?slug=' . $mark), true);
    }

    public function car_model_numbers()
    {
        $model = $this->params['model'];
        $this->content = json_decode(file_get_contents('http://koitolamp.ru/catalog/children?slug=' . $model), true);
    }

    public function car_model_generations()
    {
        $modelNumber = $this->params['modelNumber'];
        $this->content = json_decode(file_get_contents('http://koitolamp.ru/catalog/items?slug=' . $modelNumber), true);
    }

    public function lamps()
    {
        $modelGeneration = $this->params['modelGeneration'];
        $this->content = json_decode(file_get_contents('http://koitolamp.ru/catalog/item?slug=' . $modelGeneration), true);
    }

    public function parser()
    {
        $db = DataBase::getDB();
        $db->query("TRUNCATE TABLE `lamp_marks`");
        $db->query("TRUNCATE TABLE `lamp_models`");
        $db->query("TRUNCATE TABLE `lamp_model_numbers`");

        $marks = [
            ['value' =>'alfa-romeo', 'title' =>'Alfa Romeo'],
         /*   ['value' =>'audi', 'title' =>'Audi'],
            ['value' =>'bmw', 'title' =>'BMW'],
            ['value' =>'chevrolet', 'title' =>'Chevrolet'],
            ['value' =>'chrysler', 'title' =>'Chrysler'],
            ['value' =>'citroen', 'title' =>'Citroen'],
            ['value' =>'daihatsu', 'title' =>'Daihatsu'],
            ['value' =>'dodge', 'title' =>'Dodge'],
            ['value' =>'fiat', 'title' =>'Fiat'],
            ['value' =>'ford', 'title' =>'Ford'],
            ['value' =>'honda', 'title' =>'Honda'],
            ['value' =>'hyundai', 'title' =>'Hyundai'],
            ['value' =>'infiniti', 'title' =>'Infiniti'],
            ['value' =>'isuzu', 'title' =>'Isuzu'],
            ['value' =>'jaguar', 'title' =>'Jaguar'],
            ['value' =>'jeep', 'title' =>'Jeep'],
            ['value' =>'kia', 'title' =>'Kia'],
            ['value' =>'lada', 'title' =>'Lada'],
            ['value' =>'lancia', 'title' =>'Lancia'],
            ['value' =>'land-rover', 'title' =>'Land Rover'],
            ['value' =>'lexus', 'title' =>'Lexus'],
            ['value' =>'mazda', 'title' =>'Mazda'],
            ['value' =>'mercedes', 'title' =>'Mercedes'],
            ['value' =>'mini', 'title' =>'Mini'],
            ['value' =>'mmc', 'title' =>'MMC'],
            ['value' =>'nissan', 'title' =>'Nissan'],
            ['value' =>'opel', 'title' =>'Opel'],
            ['value' =>'peugeot', 'title' =>'Peugeot'],
            ['value' =>'porsche', 'title' =>'Porsche'],
            ['value' =>'renault', 'title' =>'Renault'],
            ['value' =>'rover', 'title' =>'Rover'],
            ['value' =>'saab', 'title' =>'Saab'],
            ['value' =>'seat', 'title' =>'Seat'],
            ['value' =>'skoda', 'title' =>'Skoda'],
            ['value' =>'ssangyong', 'title' =>'Ssangyong'],
            ['value' =>'subaru', 'title' =>'Subaru'],
            ['value' =>'suzuki', 'title' =>'Suzuki'],
            ['value' =>'toyota', 'title' =>'Toyota'],
            ['value' =>'uaz', 'title' =>'UAZ'],
            ['value' =>'volkswagen', 'title' =>'Volkswagen'],
            ['value' =>'volvo', 'title' =>'Volvo']*/
        ];

        foreach ($marks as $mark) {
            $query = "insert into `lamp_marks` (`title`, `label`) values ({?}, {?})";
            $markId = $db->query($query, [$mark['title'], $mark['value']]);
            $modelsRes = json_decode(file_get_contents('http://koitolamp.ru/catalog/children?slug=' . $mark['value']), true);
            foreach ($modelsRes as $model) {
                $query = "insert into `lamp_models` (`title`, `label`, `mark_id`) values ({?}, {?}, {?})";
                $modelId = $db->query($query, [$model['title'], $model['slug'], $markId]);
                $modelsNumbersRes = json_decode(file_get_contents('http://koitolamp.ru/catalog/children?slug=' . $model['slug']), true);
                foreach ($modelsNumbersRes as $modelNumber) {
                    $query = "insert into `lamp_model_numbers` (`title`, `label`, `model_id`) values ({?}, {?}, {?})";
                    $modelNumberId = $db->query($query, [$modelNumber['title'], $modelNumber['slug'], $modelId]);
                }
                sleep(rand(1,2));
            }
            sleep(rand(1,2));
        }
        $this->content = 'парсинг закончен';
    }

}