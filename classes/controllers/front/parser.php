<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_front_parser extends Controllers_front
{
    const TIME_TO_PROCESS = 5;

    public $page_size = 10;

    public function prepare()
    {
        if ($this->current_catalog['template'] == '') $this->current_catalog['template'] = 'ss_second.html';
        if ($this->current_catalog['patriarh'] == '') $this->current_catalog['patriarh'] = '1';

        if (isset($this->current_catalog['meta'])) {
            $this->current_catalog['meta'] = unserialize($this->current_catalog['meta']);
        }

        if (file_exists('./templates/' . $this->current_catalog['template'])) {
            $this->structure_template = $this->current_catalog['template'];
        } else {
            $this->structure_template = 'ss_second.html';
        }
    }

    /*
     * Выдача формы на регистрацию на сайте
     */
    public function form()
    {
        $this->prepare();
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Expires: " . date("r"));
        $twig = Twig::get_instance();
        $this->content = $twig->template
            ->loadTemplate('parser.twig')
            ->render([]);
    }

    /**
     * выдача тотала
     */
    public function total()
    {
        $db = DataBase::getDB();
        $query = "select count(*) as `total` from `sitestaff`";
        echo json_encode(['total' => $db->selectCell($query)]);
        die();
    }

    /**
     * выдача тотала
     */
    public function parse()
    {
        $debug = [];

        $offset = $this->params['offset'];
        $db = DataBase::getDB();
        $query = "select * from `sitestaff` limit " . $offset . ',1';
        //$query = "select * from `sitestaff` where ssnomer=15770 limit " . $offset . ',1';
        $old_item = $db->selectRow($query);
        $exist = $db->selectCell("select `id` from `staff` where `old_id`=".$old_item['ssnomer']);
        if ($exist) {
            Api_staff::remove($exist);
        }
        $new_catalog_id = $db->selectCell("select `id` from `catalog` where `old_id` = " . $old_item['sspredok']);
        if ($new_catalog_id = $db->selectCell("select `id` from `catalog` where `old_id` = " . $old_item['sspredok'])) {

        } else {
            $new_catalog_id = 12;
            $debug[] = 'Не найден каталог ' . $old_item['sspredok'];
        }
        $queryInsert = "
        insert into `staff`
        (
          `sorting`,
          `catalog_id`,
          `title`,
          `second_title`,
          `label`,
          `price`,
          `cashtype`,
          `description`,
          `content`,
          `link`,
          `discuss`,
          `date_insert`,
          `custom_id`,
          `old_id`
        )
        values
        ({?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?})
        ";
        if (preg_match('/[\sА-Яа-я]{6,}/u', $old_item['tovarname'], $rus)) {
            $title = str_replace($rus[0], '', $old_item['tovarname']);
            if (empty($title)) {
                $title = $old_item['tovarname'];
            }
            $description = $rus[0];
        } else {
            $title = $old_item['tovarname'];
            $description = '';
        }

        //<value>1648</value><value>Да</value>|<value>1920</value><value>--Вишня--Олива--</value>|<value>1657</value><value>Holika Holika</value>


        $queryInsert = $db->getQuery($queryInsert, [
            $old_item['ordernomer'],
            (int)$new_catalog_id,
            $title,
            '',
            Helpers_common::getSafeUrlName($old_item['tovarname']),
            $old_item['prise'],
            'rub',
            empty($old_item['shortinfo']) ? $description : $old_item['shortinfo'],
            $old_item['ssinfo'],
            '',
            1,
            date('Y-m-d H:i:s', $old_item['date']),
            $old_item['artikul'],
            $old_item['ssnomer']
        ]);
        $newstaff = $db->query($queryInsert);
        if ($newstaff) {
            //смотрим файлы
            $file_model = new Api_files();
            for ($i = 1; $i <= 10; $i++) {
                if (!empty($old_item['sspicname' . $i])) {
                    $file = $file_model->upload_that_file('./old_staff/' . $old_item['sspicname' . $i]);
                    $query = "insert into `staff_files` (`file_id`, `staff_id`, `is_main`, `linked_in`) values (
                " . (int)$file['id'] . ",
                 " . $newstaff . ",
                 " . (($i == 1) ? 1 : 0) . ",
                 'self'
                  )";
                    $db->query($query);
                }
            }
            //вставляем поля
            $f = [];
            $fields = explode('|', $old_item['df_values']);
            foreach ($fields as $fieldstring) {
                if (preg_match('/<value>(.+)<\/value><value>(.+)<\/value>/Uu', $fieldstring, $f)) {
                    if ($f[1] == 1657) {
                        if ($reference_item = $db->selectCell("select `id` from `reference_items` where `title` = '" . trim($f[2]) . "' limit 0,1")) {
                            $insert_field = "insert into `staff_fields_values` (`staff_id`, `field_id`, `value_reference`) values ('$newstaff', 21, '$reference_item')";
                            $db->query($insert_field);
                        } else {
                            $debug[] = 'Не найден производитель ' . $f[2];
                        }

                    }
                    if ($f[1] == 1920) {
                        $fvalues = explode('--', $f[2]);
                        foreach ($fvalues as $fvalue) {
                            if (!empty($fvalue)) {
                                $ritem = $db->selectCell("select `id` from `reference_items` where `title` = '" . $fvalue . "' limit 0,1");
                                if (!$ritem) {
                                    $ritem = $db->query("insert into `reference_items` (`reference_id`, `title`, `label`) values (4, '$fvalue', '" . Helpers_common::getSafeUrlName($fvalue) . "')");
                                }
                                $capi_staff_id = $db->query("insert into `staff_capitalized` (`staff_id`, `value`) values ('$newstaff', '100')");
                                $db->query("insert into `staff_fields_values_capitalized` (`capitalized_staff_id`, `field_id`, `value`) values ('$capi_staff_id', 23, '$ritem')");
                            }
                        }
                    }
                }
            }
            echo json_encode(['item' => $old_item, 'debug' => $debug]);
        } else {
            echo json_encode(['query' => $queryInsert, 'error' => $queryInsert]);
        }
        die();
    }

    /**
     * выдача тотала
     */
    public function total_reviews()
    {
        $db = DataBase::getDB();
        $query = "select count(*) as `total` from `gostb`";
        echo json_encode(['total' => $db->selectCell($query)]);
        die();
    }

    /**
     * выдача тотала
     */
    public function parse_reviews()
    {
        $debug = [];
        $offset = $this->params['offset'];
        $db = DataBase::getDB();
        $query = "select * from `gostb` limit " . $offset . ',1';
        $old_review = $db->selectRow($query);
        $new_catalog_id = empty($old_review['katalognomer']) ? 0 : $db->selectCell("select `id` from `catalog` where `old_id` = " . $old_review['katalognomer']);
        $new_staff_id = empty($old_review['staffnomer']) ? 0 : $db->selectCell("select `id` from `staff` where `old_id` = " . $old_review['staffnomer']);

        $queryInsert = "
        insert into `reviews`
        (
          `old_id`,
          `staff_id`,
          `catalogue_id`,
          `user_name`,
          `user_email`,
          `user_id`,
          `user_type`,
          `message`,
          `answer`,
          `approved`,
          `date_insert`,
          `date_update`
        )
        values
        ({?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?})
        ";
        if (preg_match('/^--/u', $old_review['nick'])) {
            $_user = $db->selectRow("select * from `users` where `nick` = '" . str_replace('--', '', $old_review['nick']) . "'");
            $user = ['type' => 'registered', 'id' => $_user['id'], 'nick' => str_replace('--', '', $old_review['nick'])];

        } else {
            $user = ['type' => 'guest', 'id' => null, 'nick' => $old_review['nick']];
        }
        list($old_date['day'], $old_date['month'], $old_date['year']) = explode('.', $old_review['date']);
        $old_review['new_date'] = '20' . $old_date['year'] . '-' . $old_date['month'] . '-' . $old_date['day'] . ' ' . rand(10, 23) . ':' . rand(00, 59) . ':00';
        $queryInsert = $db->getQuery($queryInsert, [
            $old_review['nomer'],
            $new_staff_id,
            $new_catalog_id,
            $user['nick'],
            $old_review['mail'],
            $user['id'],
            $user['type'],
            $old_review['message'],
            $old_review['vitamess'],
            $old_review['approved'],
            $old_review['new_date'],
            $old_review['new_date']
        ]);
        if ($db->query($queryInsert)) {
            echo json_encode(['review' => $old_review]);
        } else {
            echo json_encode(['review' => $old_review, 'error' => $queryInsert]);
        }
        die();
    }

    /**
     * выдача тотала по пользователям
     */
    public function total_users()
    {
        $db = DataBase::getDB();
        $query = "select count(*) as `total` from `old_users` where `pw` is not null";
        echo json_encode(['total' => $db->selectCell($query)]);
        die();
    }

    /**
     * парсинг старых пользователей
     */
    public function parse_users()
    {
        $debug = [];
        $offset = $this->params['offset'];
        $db = DataBase::getDB();
        $query = "select * from `old_users` where `pw` is not null limit " . $offset . ',1';

        $old_user = $db->selectRow($query);
        if (empty($old_user['nomer'])) {
            die('нет нихрена');
        }

        //$query = "INSERT INTO users (email, password, nick, verified, registered) VALUES ({?}, {?}, {?}, {?}, {?})";

        $queryInsert = "
        insert into `users`
        (
          `old_id`,
          `email`,
          `password`,
          `clear_password`,
          `nick`,
          `verified`,
          `registered`
        )
        values
        ({?}, {?}, {?}, {?}, {?}, {?}, {?})
        ";
        $queryInsert = $db->getQuery($queryInsert, [
            $old_user['nomer'],
            $old_user['mail'],
            password_hash($old_user['pw'], PASSWORD_DEFAULT),
            $old_user['pw'],
            $old_user['nick'],
            1,
            time()
        ]);
        if ($new_user = $db->query($queryInsert)) {
            $queryContacts = "
            insert into `users_contacts`
            (`user_id`, `name`, `surname`, `family`, `mailing`, `phone`, `postindex`, `region`, `city`, `street`, `house`, `flat`, `discount`)
            values
            ({?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?})
            ";
            $queryContacts = $db->getQuery($queryContacts, [
                $new_user,
                $old_user['fio_name'],
                $old_user['fio_surname'],
                $old_user['fio'],
                $old_user['spamagree'],
                $old_user['phone'],
                $old_user['index'],
                $old_user['region'],
                $old_user['city'],
                '', '', '',
                $old_user['skidka']
            ]);
            if ($db->query($queryContacts)) {
                echo json_encode(['user' => $old_user]);
            } else {
                echo json_encode(['user' => $old_user, 'error' => $queryContacts]);
            }
        } else {
            echo json_encode(['user' => $old_user, 'error' => $queryInsert]);
        }
        die();
    }

    /**
     * выдача тотала по пользователям
     */
    public function total_orders()
    {
        $db = DataBase::getDB();
        $query = "select count(*) as `total` from `old_orders`";
        echo json_encode(['total' => $db->selectCell($query)]);
        die();
    }

    /**
     * парсинг старых заказов
     */
    public function parse_orders()
    {
        $debug = [];
        $offset = $this->params['offset'];
        $db = DataBase::getDB();

        $shipping = [0 => 2, 1 => 4, 2 => 1];
        $status = [
            0 => 'unchecked',
            1 => 'checked',
            2 => 'payed',
            6 => 'completed'
        ];


        $query = "select * from `old_orders` limit " . $offset . ',1';

        $old_order = $db->selectRow($query);
        if (empty($old_order['order_nomer'])) {
            die('нет нихрена');
        }
        $old_user = $db->selectRow("select * from `old_users` ");
        $user_id = $db->selectCell("select `id` from `users` where `nick` = " . $old_order['order_customer'] . " limit 0,1");
        if ($user_id) {
            $user = Models_users::get_by_id($user_id);
        } else {
            $user = ['type' => 'unregistered'];
        }
                /*$params = [
                    'family' => $_POST['family'],
                    'name' => $_POST['name'],
                    'surname' => $_POST['surname'],
                    'email' => $_POST['email'],
                    'phone' => isset($_POST['phone']) ? preg_replace('/[^0-9]/', '', $_POST['phone']) : '',
                    'postindex' => isset($_POST['postindex']) ? $_POST['postindex'] : '',
                    'street' => isset($_POST['street']) ? $_POST['street'] : '',
                    'house' => isset($_POST['house']) ? $_POST['house'] : '',
                    'flat' => isset($_POST['flat']) ? $_POST['flat'] : ''
                ];*/
        $queryInsert = "
        insert into `orders`
        (
          `old_id`,
          `date`,
          `user_id`,
          `user_type`,
          `user_commence`,
          `info`,
          `shipping`,
          `shipping_price`,          
          `post_id`,
          `status`          
        )
        values
        ({?}, {?}, {?}, {?}, {?}, {?}, {?})
        ";
        $queryInsert = $db->getQuery($queryInsert, [
            $old_order['order_nomer'],
            date('Y-m-d H:i:s', $old_order['order_date']),
            $user['type'] == 'registered' ? $user_id : null,
            $user['type'],
            $old_order['order_info'],
            json_encode([
                'family' => $old_user['fio'],
                'name' => $old_user['fio_name'],
                'surname' => $old_user['fio_surname'],
                'email' => $old_user['mail'],
                'phone' => $old_user['phone'],
                'postindex' => $old_user['index'],
                'city' => $old_user['city'],
                'address' => $old_user['addres']
            ]),
            $shipping[(int)$old_order['paymethod']],
            $old_order['order_cargo_prise'],
            $old_order['post_id'],
            $status[$old_order['order_status']]
        ]);
        if ($new_user = $db->query($queryInsert)) {
            $queryContacts = "
            insert into `users_contacts`
            (`user_id`, `name`, `surname`, `family`, `mailing`, `phone`, `postindex`, `region`, `city`, `street`, `house`, `flat`, `discount`)
            values
            ({?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?})
            ";
            $queryContacts = $db->getQuery($queryContacts, [
                $new_user,
                $old_user['fio_name'],
                $old_user['fio_surname'],
                $old_user['fio'],
                $old_user['spamagree'],
                $old_user['phone'],
                $old_user['index'],
                $old_user['region'],
                $old_user['city'],
                '', '', '',
                $old_user['skidka']
            ]);
            if ($db->query($queryContacts)) {
                echo json_encode(['user' => $old_user]);
            } else {
                echo json_encode(['user' => $old_user, 'error' => $queryContacts]);
            }
        } else {
            echo json_encode(['user' => $old_user, 'error' => $queryInsert]);
        }
        die();
    }


}