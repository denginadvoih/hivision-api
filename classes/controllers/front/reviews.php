<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_front_reviews
{
    const TIME_TO_PROCESS = 5;
    const LIMIT = 10;

    public $page_size = 10;

    public $catalog;
    public $params;

    function __construct($catalog, $params)
    {
        $this->catalog = $catalog;
        $this->params = $params;
    }

    /**
     * Вывод страницы с отзывами
     */
    public function get()
    {
        $twig = Twig::get_instance();
        $reviews = Models_reviews::get([
            'approved' => 1,
            'staff_id' => 0,
            'limit' => self::LIMIT,
            'offset' => (Helpers_arr::get($this->params, 'page', 1) - 1) * self::LIMIT
        ]);
        $_SESSION['on_reviews'] = time();
        return $twig->template
            ->loadTemplate('page_reviews.twig')
            ->render([
                'url' => $this->catalog['path'],
                'reviews' => $reviews['items'],
                'paginator' => 'paginator_reviews.twig',
                'current_page' => Helpers_arr::get($this->params, 'page', 1),
                'total' => $reviews['total'],
                'page_count' => ceil($reviews['total'] / self::LIMIT),
            ]);
    }
}