<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */
class Controllers_front_users extends Controllers_front
{
    const TIME_TO_PROCESS = 5;

    public $page_size = 10;

    public function prepare()
    {
        $this->structure_template = 'ss_second.html';
    }

    /*
     * Авторизация
     */
    public function auth()
    {
        if (isset($_POST['email']) and isset($_POST['password'])) {
            $auth = Auth_auth::get_instance();
            if ($auth->login($_POST['email'], $_POST['password'], ($_POST['remember'] == 1))) {
                if (isset($_POST['url'])) {
                    header("Location: " . $_POST['url']);
                } else {
                    header("Location: /");
                }
                exit;
            } else {
                $this->prepare();
                $twig = Twig::get_instance();
                $this->content = $twig->template
                    ->loadTemplate('users/auth_bad.twig')
                    ->render(['error' => $auth->error]);
            }
        } else {
            $this->prepare();
            $twig = Twig::get_instance();
            $this->content = $twig->template
                ->loadTemplate('users/auth_bad.twig')
                ->render(['error' => ['code' => 'AUTH_DATA_INVALID', 'message' => 'Некорректные данные для авторизации']]);
        }
    }

    /*
     * Выход из профиля
     */
    public function logout()
    {
        $auth = Auth_auth::get_instance();
        $auth->logout();
        header("Location: /");
        exit;
    }

    /*
     * Выдача формы на регистрацию на сайте
     */
    public function add()
    {
        $this->prepare();
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Expires: " . date("r"));
        $_SESSION['user_register_start'] = time();
        $region = Api_regions::get_regions();
        $twig = Twig::get_instance();
        $this->content = $twig->template
            ->loadTemplate('users/register_form.twig')
            ->render([
                'region' => $region
            ]);
    }

    /*
     * Создание нового пользователя
     */
    public function create()
    {
        if (isset($_SESSION['user_register_start']) and (time() - $_SESSION['user_register_start'] > self::TIME_TO_PROCESS)) {
            unset($_SESSION['user_register_start']);
            $auth = Auth_auth::get_instance();
            $userId = $auth->register($_POST['email'], $_POST['password'], $_POST['nick'], function ($selector, $token) {
                //отправляем письмо для подтверждения регистрации
                $twig = Twig::get_instance();
                $message = $twig->template
                    ->loadTemplate('users/mail_confirmation.twig')
                    ->render([
                        'nick' => $_POST['nick'],
                        'name' => $_POST['name'],
                        'password' => $_POST['password'],
                        'domain' => SITE_NAME,
                        'selector' => $selector,
                        'token' => $token
                    ]);
                $headers = "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/html; charset=utf-8\r\n";
                $headers .= "From: Подтверждение регистрации на сайте<mail-service@" . SITE_NAME . ">\r\n";
                $subject = 'Подтвердите свою регистрацию на сайте ' . SITE_NAME;
                mail($_POST['email'], $subject, $message, $headers);
                return;
            });
            if (is_integer($userId)) {
                $db = DataBase::getDB();
                $phone = isset($_POST['phone']) ? preg_replace('/[^0-9]/', '', $_POST['phone']) : '';
                $region = isset($_POST['region']) ? $_POST['region'] : '';
                if (is_numeric($region)) {
                    $_region = Api_regions::get_region($region);
                    if (is_array($_region)) {
                        $region = $_region['title'];
                    }
                }
                if (isset($_POST['city_is_custom'])) {
                    $city = isset($_POST['custom_city']) ? $_POST['custom_city'] : '';
                } else {
                    $city = isset($_POST['city']) ? $_POST['city'] : '';
                    if (is_numeric($city)) {
                        $_city = Api_regions::get_city($city);
                        if (is_array($_city)) {
                            $city = $_city['title'];
                        }
                    }
                }
                $name = isset($_POST['name']) ? $_POST['name'] : '';
                $surname = isset($_POST['surname']) ? $_POST['surname'] : '';
                $family = isset($_POST['family']) ? $_POST['family'] : '';
                $postindex = isset($_POST['postindex']) ? $_POST['postindex'] : '';
                $street = isset($_POST['street']) ? $_POST['street'] : '';
                $house = isset($_POST['house']) ? $_POST['house'] : '';
                $flat = isset($_POST['flat']) ? $_POST['flat'] : '';
                $mailing = isset($_POST['mailing']) ? 1 : 0;
                $query = "insert into `users_contacts`
                (`user_id`, `name`, `surname`, `family`, `phone`, `region`, `city`, `postindex`, `street`, `house`, `flat`, `mailing`)
                values
                ({?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?}, {?})
                ";
                $db->query($query, [$userId, $name, $surname, $family, $phone, $region, $city, $postindex, $street, $house, $flat, $mailing]);
                header("Location: /user/you_are_registered");
                exit;
            } else {
                $this->prepare();
                $twig = Twig::get_instance();
                $this->content = $twig->template
                    ->loadTemplate('users/register_bad.twig')
                    ->render(['error' => $auth->error]);
            }
        } else {
            header("Location: /user/you_are_registered");
            exit;
        }


    }

    /*
 * Выдача формы на обновление контактных данных
 */
    public function edit()
    {
        $this->prepare();
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Expires: " . date("r"));
        $_SESSION['user_update_start'] = time();
        $regions = Api_regions::get_regions();
        $cities = [];
        $auth = Auth_auth::get_instance();
        $user = $auth->getUser();
        $userContacts = $auth->getUserContacts($user['id']);
        $_temp = Helpers_arr::array_func_search($regions, function ($val) use($userContacts) {
            if ($val['title'] == $userContacts['region']) {
                return true;
            } else {
                return false;
            }
        });
        if ($_temp) {
            $userContacts['region_id'] = $regions[$_temp]['id'];
            $cities = Api_regions::get_cities_by_region_id($userContacts['region_id']);
            $_temp = Helpers_arr::array_func_search($cities, function ($val) use($userContacts) {
                if ($val['title'] == $userContacts['city']) {
                    return true;
                } else {
                    return false;
                }
            });
            if ($_temp) {
                $userContacts['city_id'] = $cities[$_temp]['id'];
            }
        }
        //определяем не кастомный ли город
        if (!empty($userContacts['city']) && empty($userContacts['city_id'])) {
            $customCity = true;
        } else {
            $customCity = false;
        }
        $twig = Twig::get_instance();;
        $this->content = $twig->template
            ->loadTemplate('users/update_form.twig')
            ->render([
                'regions' => $regions,
                'cities' => $cities,
                'user' => $user,
                'contacts' => $userContacts,
                'customCity' => $customCity
            ]);
    }

  /*
  * Изменение данных пользователя
  */
    public function save()
    {
        if (isset($_SESSION['user_update_start']) and (time() - $_SESSION['user_update_start'] > self::TIME_TO_PROCESS)) {
            unset($_SESSION['user_register_start']);
            $auth = Auth_auth::get_instance();
            $userId = $auth->getUserId();
            if (is_integer($userId)) {
                $db = DataBase::getDB();
                $phone = isset($_POST['phone']) ? preg_replace('/[^0-9]/', '', $_POST['phone']) : '';
                $region = isset($_POST['region']) ? $_POST['region'] : '';
                if (is_numeric($region)) {
                    $_region = Api_regions::get_region($region);
                    if (is_array($_region)) {
                        $region = $_region['title'];
                    }
                }
                if (isset($_POST['city_is_custom'])) {
                    $city = isset($_POST['custom_city']) ? $_POST['custom_city'] : '';
                } else {
                    $city = isset($_POST['city']) ? $_POST['city'] : '';
                    if (is_numeric($city)) {
                        $_city = Api_regions::get_city($city);
                        if (is_array($_city)) {
                            $city = $_city['title'];
                        }
                    }
                }
                $name = isset($_POST['name']) ? $_POST['name'] : '';
                $surname = isset($_POST['surname']) ? $_POST['surname'] : '';
                $family = isset($_POST['family']) ? $_POST['family'] : '';
                $street = isset($_POST['street']) ? $_POST['street'] : '';
                $house = isset($_POST['house']) ? $_POST['house'] : '';
                $flat = isset($_POST['flat']) ? $_POST['flat'] : '';
                $postindex = isset($_POST['postindex']) ? $_POST['postindex'] : '';
                $mailing = (isset($_POST['mailing']) && $_POST['mailing'] == 1) ? 1 : 0;
                $query = "update `users_contacts`
                set `name` = {?}, `surname` = {?}, `family` = {?}, `phone` = {?}, `region` = {?}, `city` = {?}, `street` = {?}, `house` = {?}, `flat` = {?}, `postindex` = {?}, `mailing` = {?}
                where `user_id` = {?}
                ";
                $query = $db->getQuery($query, [$name, $surname, $family, $phone, $region, $city, $street, $house, $flat, $postindex, $mailing, $userId]);
                $db->query($query);
                header("Location: /");
                exit;
            } else {
                $this->prepare();
                $twig = Twig::get_instance();
                $this->content = $twig->template
                    ->loadTemplate('users/register_bad.twig')
                    ->render(['error' => $auth->error]);
            }
        } else {
            header("Location: /");
            exit;
        }
    }

    /*
     * Уведомление о регистрации пользователя
     */
    public function is_registered()
    {
        $this->prepare();
        $twig = Twig::get_instance();
        $this->content = $twig->template
            ->loadTemplate('users/register_ok.twig')
            ->render([]);
    }

    /**
     * Подтверждение e-mail
     */
    public function confirmation()
    {
        $this->prepare();
        $twig = Twig::get_instance();
        if (isset($_GET['selector']) and isset($_GET['token'])) {
            $auth = Auth_auth::get_instance();
            if ($auth->confirmEmail($_GET['selector'], $_GET['token'])) {
                $this->content = $twig->template
                    ->loadTemplate('users/confirmation_ok.twig')
                    ->render([]);
            } else {
                $this->content = $twig->template
                    ->loadTemplate('users/confirmation_bad.twig')
                    ->render(['error' => $auth->error]);
            }
        }
    }

    /**
     * Форма отправки данных для запуска механизма напоминания пароля
     */
    public function remember_password()
    {
        $this->prepare();
        $twig = Twig::get_instance();
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Expires: " . date("r"));
        $_SESSION['user_remember_password'] = time();
        $this->content = $twig->template
            ->loadTemplate('users/remember_password_form.twig')
            ->render([]);
    }

    /**
     * Создание директивы на восстановление пароля
     */
    public function send_remember_password()
    {
        $this->prepare();
        $twig = Twig::get_instance();
        if (isset($_SESSION['user_remember_password']) and (time() - $_SESSION['user_remember_password'] > self::TIME_TO_PROCESS)) {
            $auth = Auth_auth::get_instance();
            if ($auth->create_restore_password($_POST['email'], $_POST['name'], function ($selector, $token) {
                //отправляем письмо для подтверждения регистрации
                $twig = Twig::get_instance();
                $message = $twig->template
                    ->loadTemplate('users/mail_remember_password.twig')
                    ->render([
                        'name' => $_POST['name'],
                        'domain' => SITE_NAME,
                        'selector' => $selector,
                        'token' => $token
                    ]);
                $headers = "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/html; charset=utf-8\r\n";
                $headers .= "From: " . SITE_NAME . "<mail-service@" . SITE_NAME . ">\r\n";
                $subject = 'Восстановление пароля на сайте ' . SITE_NAME;
                mail($_POST['email'], $subject, $message, $headers);
                return;
            })
            ) {
                $this->content = $twig->template
                    ->loadTemplate('users/restore_send_ok.twig')
                    ->render([]);
            } else {
                $this->content = $twig->template
                    ->loadTemplate('users/restore_send_bad.twig')
                    ->render([]);

            }
        } else {
            $this->content = $twig->template
                ->loadTemplate('users/restore_send_ok.twig')
                ->render([]);
        }
    }

    /**
     * Вывод формы ввода нового пароля
     */
    public function restore_password_form()
    {
        $this->prepare();
        $twig = Twig::get_instance();
        if (isset($_GET['selector']) and isset($_GET['token'])) {
            $auth = Auth_auth::get_instance();
            if ($auth->check_restore_password($_GET['token'], $_GET['selector'])) {
                $this->content = $twig->template
                    ->loadTemplate('users/new_password_form.twig')
                    ->render(['selector' => $_GET['selector'], 'token' => $_GET['token']]);
            } else {
                $this->content = $twig->template
                    ->loadTemplate('users/new_password_form_bad.twig')
                    ->render(['error' => $auth->error]);
            }
        }
    }

    /**
     * Восттановление пароля
     */
    public function restore_password()
    {
        if (isset($_POST['password']) and isset($_POST['token']) and isset($_POST['selector'])) {
            $auth = Auth_auth::get_instance();
            if ($auth->restore_password($_POST['token'], $_POST['selector'], $_POST['password'])) {
                header("Location: /user/new_password_ok");
                exit;
            } else {
                $this->prepare();
                $twig = Twig::get_instance();
                $this->content = $twig->template
                    ->loadTemplate('users/new_password_bad.twig')
                    ->render(['error' => $auth->error]);
            }
        }
    }

    /**
     * Уведомление об удачной смене пароля
     */
    public function new_password_ok()
    {
        $this->prepare();
        $twig = Twig::get_instance();
        $this->content = $twig->template
            ->loadTemplate('users/new_password_ok.twig')
            ->render([]);
    }
}