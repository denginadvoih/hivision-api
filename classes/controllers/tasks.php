<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 10.02.15
 * Time: 1:47
 */

class Controllers_tasks
{
    const MAIL_LIMIT = 10;

    public function __construct($branch, $params, $url, $object)
    {

    }

    private function get_cash_values()
    {
        $val = [];
        $date = date("d/m/Y");
        $link = "http://www.cbr.ru/scripts/XML_daily.asp?date_req=$date";
        // Загружаем HTML-страницу
        if ($fd = fopen($link, "r")) {
            $content = "";
            if ($fd) {
                // Чтение содержимого файла в переменную $content
                while (!feof($fd)) $content .= fgets($fd, 4096);
            } else return '';
            // Закрыть открытый файловый дескриптор
            fclose($fd);
            // Разбираем содержимое, при помощи регулярных выражений
            $pattern = "#<Valute ID=\"([^\"]+)[^>]+>[^>]+>([^<]+)[^>]+>[^>]+>[^>]+>[^>]+>[^>]+>[^>]+>([^<]+)[^>]+>[^>]+>([^<]+)#i";
            preg_match_all($pattern, $content, $out, PREG_SET_ORDER);
            foreach ($out as $cur) {
                if ($cur[2] == 840) $val['USD'] = doubleVal(str_replace(",", ".", $cur[4]));
                if ($cur[2] == 392) $val['YENA'] = doubleVal(str_replace(",", ".", $cur[4]));
                if ($cur[2] == 156) $val['YUAN'] = doubleVal(str_replace(",", ".", $cur[4]));
                if ($cur[2] == 978) $val['EURO'] = doubleVal(str_replace(",", ".", $cur[4]));
            }
        } else {
            $val['USD'] = 24;
            $val['YENS'] = 24;
            $val['YUAN'] = 24;
            $val['EURO'] = 24;
        }
        return $val;
    }

    function cash_update()
    {
        $db = DataBase::getDB();
        $cashValues = $this->get_cash_values();
        $query = "select * from `cash`";
        $old_values = $db->select($query);
        $old_values = Helpers_common::columnAsKey($old_values, 'title');
        if (is_array($old_values) && !empty($cashValues)) {
            $db->query("delete from `cash`");
            foreach ($cashValues as $key => $value) {
                $query = "insert into `cash`
                (`old_date`, `old_value`, `value`, `title`)
                values
                ({?}, {?}, {?}, {?})";
                $db->query($query, [$old_values[$key]['date'], $old_values[$key]['value'], $value, $key]);
            }
        }

    }

/*    public function some_migrate() {
        $db = DataBase::getDB();
        for ($i = 1414; $i <= 1445; $i++) {
            $query = " insert into `catalog_fields` (`catalog_id`, `field_id`, `order_check`) values ('$i', '21', 'N')";
            $db->query($query);
            $query = " insert into `catalog_fields` (`catalog_id`, `field_id`, `order_check`) values ('$i', '23', 'Y')";
            $db->query($query);
        }
    }*/

    public function mailing() {
        $mailings = Models_mailings::get([
            'status' => 'active',
            'date_start' => date('Y-m-d H:i:s', time())
        ]);
        $twig = Twig::get_instance();
        if (is_array($mailings) && !empty($mailings)) {
            foreach ($mailings['items'] as $mailing) {
                $users = Models_users::get(['mailing' => 1, 'offset' => $mailing['progress_step']*self::MAIL_LIMIT, 'limit' => self::MAIL_LIMIT]);
                foreach ($users['items'] as $user) {
                    $message = $twig->template
                        ->loadTemplate('users/mailing.twig')
                        ->render([
                            'user' => $user,
                            'message' => $mailing['message'],
                            'domain' => SITE_NAME
                        ]);
                    $headers = "MIME-Version: 1.0\r\n";
                    $headers .= "Content-type: text/html; charset=utf-8\r\n";
                    $headers .= "From: " . SITE_NAME . "<mail-service@" . SITE_NAME . ">\r\n";
                    $subject = $mailing['subject'];
                    mail($user['email'], $subject, $message, $headers);
                }
                if (empty($users['items'])) {
                    Models_mailings::finish($mailing['id']);
                } else {
                    Models_mailings::update_progress($mailing['id']);
                }
            }

        }

    }
} 