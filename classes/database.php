<?php
class DataBase extends Config {

    private static $db = null; // ������������ ��������� ������, ����� �� ��������� ��������� �����������
    private $mysqli; // ������������� ����������
    private $sym_query = "{?}"; // "������ �������� � �������"

    /* ��������� ���������� ������. ���� �� ��� ����������, �� ������������, ���� ��� �� ����, �� �������� � ������������ (������� Singleton) */
    public static function getDB() {
        if (self::$db == null) self::$db = new DataBase();
        return self::$db;
    }

    /* private-�����������, �������������� � ���� ������, ��������������� ������ � ��������� ���������� */
    private function __construct() {
        $db = $this->get('database');
        $this->mysqli = new mysqli($db['server'], $db['user'], $db['passwd'], $db['db']);
        /*$this->mysqli->query("SET lc_time_names = 'ru_RU'");
        $this->mysqli->query("SET collation_connection = 'utf8_general_ci'");
        $this->mysqli->query("SET character_set_results = 'utf8'");*/
        $this->mysqli->query("SET NAMES 'utf8' COLLATE 'utf8_general_ci'");
        $this->mysqli->query("SET NAMES 'utf8'");
        $this->mysqli->query("SET collation_connection = 'UTF-8_general_ci'");
        $this->mysqli->query("SET collation_server = 'UTF-8_general_ci'");
        $this->mysqli->query("SET character_set_client = 'UTF-8'");
        $this->mysqli->query("SET character_set_connection = 'UTF-8'");
        $this->mysqli->query("SET character_set_results = 'UTF-8'");
        $this->mysqli->query("SET character_set_server = 'UTF-8'");
        $this->mysqli->query("SET SQL_BIG_SELECTS=1");
        //$this->mysqli->query("SET NAMES 'utf8'");
    }

    /* ��������������� �����, ������� �������� "������ �������� � �������" �� ���������� ��������, ������� �������� ����� "������� ������������" */
    public function getQuery($query, $params) {
        if ($params) {
            foreach ($params as $param) {
                $pos = strpos($query, $this->sym_query);
                $arg = "'".$this->mysqli->real_escape_string(trim($param))."'";
                $query = substr_replace($query, $arg, $pos, strlen($this->sym_query));
            }
        }
        return $query;
    }

    /* SELECT-�����, ������������ ������� ����������� */
    public function select($query, $params = false) {
        $result_set = $this->mysqli->query($this->getQuery($query, $params));
        if (!$result_set) return false;
        return $this->resultSetToArray($result_set);
    }

    /* SELECT-�����, ������������ ���� ������ � ����������� */
    public function selectRow($query, $params = false) {
        $result_set = $this->mysqli->query($this->getQuery($query, $params));
        if ($result_set->num_rows != 1) return false;
        else return $result_set->fetch_assoc();
    }

    /* SELECT-�����, ������������ �������� �� ���������� ������ */
    public function selectCell($query, $params = false) {
        $result_set = $this->mysqli->query($this->getQuery($query, $params));
        if ((!$result_set) || ($result_set->num_rows != 1)) return false;
        else {
            $arr = array_values($result_set->fetch_assoc());
            return $arr[0];
        }
    }

    /* ��-SELECT ������ (INSERT, UPDATE, DELETE). ���� ������ INSERT, �� ������������ id ��������� ����������� ������ */
    public function query($query, $params = false) {
        $success = $this->mysqli->query($this->getQuery($query, $params));
        if ($success) {
            if ($this->mysqli->insert_id === 0) return true;
            else return $this->mysqli->insert_id;
        }
        else return false;
    }

    /* �������������� result_set � ��������� ������ */
    private function resultSetToArray($result_set) {
        $array = array();
        while (($row = $result_set->fetch_assoc()) != false) {
            $array[] = $row;
        }
        return $array;
    }

    /* ��� ����������� ������� ����������� ���������� � ����� ������ */
    public function __destruct() {
        if ($this->mysqli) $this->mysqli->close();
    }
}
?>