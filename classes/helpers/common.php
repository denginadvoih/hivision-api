<?php //defined('SYSPATH') or die('No direct script access.');

class Helpers_common
{
    static public function convert_date($timestamp, $mode = 'ru')
    {

        $timestamp = strtotime($timestamp);
        $month = [
            'ru' =>
                [
                    1 => 'января',
                    2 => 'февраля',
                    3 => 'марта',
                    4 => 'апреля',
                    5 => 'мая',
                    6 => 'июня',
                    7 => 'июля',
                    8 => 'августа',
                    9 => 'сентября',
                    10 => 'октября',
                    11 => 'ноября',
                    12 => 'декабря'
                ],
            'en' =>
                [
                    1 => 'january',
                    2 => 'february',
                    3 => 'march',
                    4 => 'april',
                    5 => 'may',
                    6 => 'june',
                    7 => 'july',
                    8 => 'august',
                    9 => 'september',
                    10 => 'october',
                    11 => 'november',
                    12 => 'december'
                ]
        ];
        switch ($mode) {
            case 'ru':
                return date('H:i d', $timestamp) . ' ' . $month['ru'][date('n', $timestamp)] . ' ' . date('Y', $timestamp);
                break;
            case 'en':
                return date('H:i d', $timestamp) . ' ' . $month['en'][date('n', $timestamp)] . ' ' . date('Y', $timestamp);
                break;

            default:
                return date('H:i d.m.Y', $timestamp);
                break;
        }
    }

    /**
     * Меняет ключи в массиве на значение определенного столбца
     * @param array $array
     * @param string $column
     * @return array
     */
    public static function columnAsKey(array $array, $column)
    {

        $tmp = array();

        foreach ($array as $k => $v) {
            if (!empty($v[$column])) {
                $tmp[$v[$column]] = $v;
            } else {
                $tmp[$k] = $v;
            }
        }
        return $tmp;
    }

    /**
     * Берет значения массива по определенному ключу в результирующий массив
     * @param array $array
     * @param string $column
     * @return array
     */
    public static function column_as_value(array $array, $column)
    {

        $tmp = array();

        foreach ($array as $k => $v) {
            if (!empty($v[$column])) {
                $tmp[] = $v[$column];
            }
        }
        return $tmp;
    }


    public static function slice_by_parent($match_id, $catalog)
    {
        if ($catalog['id'] == $match_id) {
            return $catalog['child'] ?? [];
        } else {
            if (empty($catalog['child'])) {
                return false;
            } else {
                foreach ($catalog['child'] as $value) {
                    $checkTree = self::slice_by_parent($match_id, $value);
                    if (is_array($checkTree)) {
                        return $checkTree;
                    }
                }
                return false;
            }
        }
    }

    public static function get_childs_from_tree($catalog)
    {
        $result = array_keys($catalog);
        foreach ($catalog as $value) {
            if (!empty($value['child'])) {
                $childs = self::get_childs_from_tree($value['child']);
                $result = array_merge($result, $childs);
            }

        }
        return $result;
    }

    /**
     * конвертация в псевдокирилицу для файлов
     * @param $str
     * @return mixed|string
     */
    public static function getSafeFileName($str)
    {
        $traslit = function ($str) {
            static $transl = array(
                'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'JO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I',
                'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T',
                'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SHH', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '',
                'Э' => 'E', 'Ю' => 'U', 'Я' => 'JA', 'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'jo',
                'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p',
                'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh',
                'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'u', 'я' => 'ja'
            );
            return strtr($str, $transl);
        };

        $nop = '-';
        $ret = mb_strtolower($traslit($str));
        $ret = preg_replace('/[\*\?%\,"\'–\-]/', '', $ret);
        $ret = strtr(
            $ret,
            array(' ' => $nop,
                '`' => '',
                '#' => $nop,
                ')' => $nop,
                '(' => $nop,
                '[' => $nop,
                '—' => $nop,
                ']' => $nop,
                '/' => $nop,
                '\\' => $nop,
                '&' => 'and',
                '+' => $nop,
                '_' => $nop,
                ';' => $nop,
                ':' => $nop,
                '!' => $nop
            )
        );
        $ret = trim($ret, '-_');
        $ret = preg_replace('/[' . $nop . ']{2,}/', $nop, $ret);
        $ret = filter_var($ret, FILTER_SANITIZE_URL);
        return $ret;
    }

    /**
     * конвертация в псевдокирилицу для url
     * @param $str
     * @return mixed|string
     */
    public static function getSafeUrlName($str)
    {
        $traslit = function ($str) {
            static $transl = array(
                'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'JO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I',
                'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T',
                'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SHH', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '',
                'Э' => 'E', 'Ю' => 'U', 'Я' => 'JA', 'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'jo',
                'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p',
                'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh',
                'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'u', 'я' => 'ja'
            );
            return strtr($str, $transl);
        };

        $nop = '-';
        $ret = mb_strtolower($traslit($str));
        $ret = preg_replace('/[\*\?%\,"\'–\-]/', '', $ret);
        $ret = strtr(
            $ret,
            array(' ' => $nop,
                '`' => '',
                '#' => $nop,
                ')' => $nop,
                '(' => $nop,
                '[' => $nop,
                '—' => $nop,
                ']' => $nop,
                '/' => $nop,
                '\\' => $nop,
                '&' => 'and',
                '+' => $nop,
                '_' => $nop,
                ';' => $nop,
                ':' => $nop,
                '!' => $nop
            )
        );
        $ret = trim($ret, '-_');
        $ret = preg_replace('/[' . $nop . ']{2,}/', $nop, $ret);
        $ret = filter_var($ret, FILTER_SANITIZE_URL);
        return $ret;
    }

    /**
     * Валидация доступного значения
     * @param $value mixed
     * @param $allowed array
     * @param bool $default
     * @return mixed
     */
    public static function validValue($value, $allowed, $default = false)
    {
        if (in_array($value, $allowed)) {
            return $value;
        } else {
            return $default;
        }
    }
}
