<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 07.02.15
 * Time: 17:21
 */
class Models_catalogue
{
    public static function delete($id)
    {
        $db = DataBase::getDB();
        $branch = self::all_child($id);
        $branch[] = $id;
        $query_catalog = "update catalog set `is_deleted` = 1 where `id` in (" . implode(',', $branch) . ")";
        $catalog_delete = $db->query($query_catalog);
        $staffQuery = "select `id` from `staff` where `catalog_id` in (" . implode(',', $branch) . ") and `is_deleted` = 0";
        $staffRes = $db->select($staffQuery);
        foreach ($staffRes as $staff) {
            Models_staff::mark_deleted($staff['id']);
        }
        return ['catalog_delete' => $catalog_delete, 'staff_delete' => $staffRes];
    }

    public static function get_catalogue_by_hmenu($catalog_id = null)
    {
        $db = DataBase::getDB();
        $query = '
        select
            `sk`.`id` as `id`,
            `sk`.`title` as `title`,
            `sk`.`label` as `label`,
            `sk`.`path` as `path`,
            `child`.`id` as `child_id`,
            `child`.`title` as `child_title`,
            `child`.`label` as `child_label`,
            `child`.`path` as `child_path`
        from catalog sk
          left join `catalog` as `child` on (`child`.`parent_id` = `sk`.`id` and `child`.`is_deleted`=0)
        where `sk`.`hmenu`=1 and `sk`.`is_deleted`=0' . ($catalog_id !== null ? (' and `sk`.`parent_id`=' . (int)$catalog_id . ' ') : '') . '
        order by `sk`.`sorting` desc, `child`.`sorting` desc
        ';

        $res = $db->select($query); // Запрос явно должен вывести таблицу, поэтому вызываем метод select()

        $result = array();

        foreach ($res as $key => $value) {
            if (!isset($result[$value['id']])) {
                $result[$value['id']] = array(
                    'id' => $value['id'],
                    'title' => $value['title'],
                    'label' => $value['label'],
                    'path' => $value['path'],
                    'child' => array()
                );
            }
            if (!empty($value['child_id']) && !isset($result[$value['id']]['child'][$value['chicld_id']])) {
                $result[$value['id']]['child'][$value['child_id']] = array(
                    'id' => $value['child_id'],
                    'title' => $value['child_title'],
                    'label' => $value['child_label'],
                    'path' => $value['child_path']
                );
            }
        }
        return $result;
    }

    /**
     * Возвращает информацию о рубрике каталога с указанным ID
     * @param int $catalog_id \d+
     * @return Array
     */
    public static function get_catalogue($catalog_id)
    {
        $db = DataBase::getDB();

        $query = "
        select
            `sk`.`id` as `id`,
            `sk`.`parent_id` as `parent_id`,
            `sk`.`title` as `title`,
            `sk`.`label` as `label`,
            `sk`.`second_title` as `second_title`,
            `sk`.`description` as `description`,
            `sk`.`content` as `content`,
            `sk`.`template` as `template`,
            `sk`.`path` as `path`,
            `sk`.`type` as `type`,
            `sk`.`hmenu` as `hmenu`,
            `sk`.`root` as `root`,
            `sk`.`in_branch` as `in_branch`,
            `sk`.`meta` as `meta`,
            `sk`.`is_visible` as `is_visible`,
            group_concat(`cf`.`field_id`) as `fields`

        from `catalog` as `sk`
        left join `catalog_fields` as `cf` on `cf`.`catalog_id` = `sk`.`id`
        where id='{$catalog_id}'
        group by `sk`.`id`
        ";
        $catalog = $db->selectRow($query);
        $catalog['fields'] = explode(',', $catalog['fields']);
        $catalog['meta'] = json_decode($catalog['meta'], true);
        $catalog['meta']['keywords'] = isset($catalog['meta']['keywords']) ? $catalog['meta']['keywords'] : '';
        $catalog['meta']['description'] = isset($catalog['meta']['description']) ? $catalog['meta']['description'] : '';
        $catalog['meta']['title'] = isset($catalog['meta']['title']) ? $catalog['meta']['title'] : '';
        return $catalog;
    }

    /**
     * Возвращает расширенную информацию о рубрике каталога с указанным ID
     * @param int $catalog_id \d+
     * @return Array
     */
    public static function get_catalogue_expand($catalog_id, $selected_fields = null)
    {
        $db = DataBase::getDB();
        $query = "
        select
            `sk`.*,
            `cfiles`.`file_id` as `file_id`,
            `cfiles`.`appointment` as `file_appointment`,
            `f`.`id` as `field_id`,
            `f`.`title` as `field_title`,
            `f`.`type` as `field_type`,
            `f`.`label` as `field_label`,
            `cf`.`order_check` as `field_order_check`,
            `r`.`id` as `reference_id`,
            `r`.`title` as `reference_title`,
            `rc`.`id` as `second_reference_id`,
            `rc`.`title` as `second_reference_title`,
            `ri`.`id` as `item_id`,
            `ri`.`title` as `item_title`
        from `catalog` as `sk`
        left join `catalogue_files` as `cfiles` on `cfiles`.`catalogue_id` = `sk`.`id`
        left join `catalog_fields` as `cf` on `cf`.`catalog_id` = `sk`.`id`
        left join `fields` as `f` on `f`.`id` = `cf`.`field_id`
        left join `references` as `r` on (`f`.`type` in ('reference', 'double_reference', 'multi_reference') and `r`.`id` = `f`.`reference_id`)
        left join `reference_items` as `ri` on (`ri`.`reference_id` = `r`.`id` and `ri`.`is_deleted`=0)
        left join `references` as `rc` on (`f`.`type` = 'double_reference' and `rc`.`parent_id` = `r`.`id`)
        where `sk`.`id`='{$catalog_id}'
        ";

        $res = $db->select($query);
        $catalog = [];
        if (is_array($res)) {
            $c_res = $res[0];
            $main_keys = [
                'id',
                'parent_id',
                'title',
                'label',
                'second_title',
                'description',
                'content',
                'template',
                'type',
                'hmenu',
                'root',
                'in_branch',
                'meta',
                'is_visible',
                'cols',
                'rows'
            ];
            foreach ($main_keys as $value) {
                $catalog[$value] = $c_res[$value];
            }

            $catalog['files'] = [];
            $catalog['fields'] = [];
            $catalog['meta'] = json_decode($c_res['meta'], true);
            $catalog['meta']['keywords'] = isset($catalog['meta']['keywords']) ? $catalog['meta']['keywords'] : '';
            $catalog['meta']['description'] = isset($catalog['meta']['description']) ? $catalog['meta']['description'] : '';
            $catalog['meta']['title'] = isset($catalog['meta']['title']) ? $catalog['meta']['title'] : '';
            foreach ($res as $value) {
                if ($value['file_id']) {
                    if (!isset($catalog['files'][$value['file_id']])) {
                        $catalog['files'][$value['file_id']] = [
                            'file_id' => $value['file_id'],
                            'appointment' => $value['file_appointment']
                        ];
                    }
                }
                if ($value['field_id']) {
                    if (!isset($catalog['fields'][$value['field_id']])) {
                        $catalog['fields'][$value['field_id']] = [
                            'id' => $value['field_id'],
                            'title' => $value['field_title'],
                            'type' => $value['field_type'],
                            'label' => $value['field_label'],
                            'order_check' => $value['field_order_check']
                        ];
                    }
                    $field = & $catalog['fields'][$value['field_id']];
                    if ($field['type'] == 'reference' or $field['type'] == 'double_reference' or $field['type'] == 'multi_reference') {
                        if (!isset($field['reference'])) {
                            $field['reference'] = [
                                'id' => $value['reference_id'],
                                'title' => $value['reference_title'],
                                'items' => []
                            ];
                        }
                        $reference = & $field['reference'];
                        if ($value['item_id']) {
                            if (!isset($reference['items'][$value['item_id']])) {
                                $reference['items'][$value['item_id']] = [
                                    'id' => $value['item_id'],
                                    'title' => $value['item_title']
                                ];
                            }
                        }
                        if ($field['type'] == 'double_reference') {
                            if (!isset($field['second_reference'])) {
                                $field['second_reference'] = [
                                    'id' => $value['second_reference_id'],
                                    'title' => $value['second_reference_title'],
                                    'items' => []
                                ];
                                //получаем дочерние элементы справочника, если родительский выделен
                                if (isset($selected_fields[$field['id']])) {
                                    foreach ($selected_fields[$field['id']] as $selected_field) {
                                        $_items = Api_fields::get_child_reference_items($selected_field);
                                        if (!empty($_items)) {
                                            $field['second_reference']['items'][$selected_field] = $_items;
                                        }
                                    }


                                }
                            }
                        }
                    }
                }
            }
            if (!empty($catalog['files'])) {
                $fileModel = new Api_files();
                $_files = $fileModel->get_files_by_ids(Helpers_common::column_as_value($catalog['files'], 'file_id'), ['admin_prev', 'catalogue_logo', 'catalogue_header']);
                foreach ($_files as $file) {
                    $catalog['files'][$file['id']] = array_merge($catalog['files'][$file['id']], $file);
                }
            }

        }
        return $catalog;
    }

    /**
     * Возвращает расширенную информацию о дочерних рубриках каталога с указанным ID
     * @param int $parent_id \d+
     * @return array
     */
    public static function get_catalogues_by_parent($parent_id)
    {
        $db = DataBase::getDB();
        $query = "
        select
            `sk`.*,
            `cfiles`.`file_id` as `file_id`,
            `cfiles`.`appointment` as `file_appointment`
        from `catalog` as `sk`
        left join `catalogue_files` as `cfiles` on `cfiles`.`catalogue_id` = `sk`.`id`
        where `sk`.`parent_id`='{$parent_id}' and `sk`.`is_deleted` = 0
        ";
        $res = $db->select($query);
        $main_keys = [
            'id',
            'path',
            'parent_id',
            'title',
            'label',
            'second_title',
            'sorting'
        ];
        $catalogues = [];
        foreach ($res as $row) {
            if (!isset($catalogues[$row['id']])) {
                $catalogues[$row['id']] = Helpers_arr::extract($row, $main_keys);
                $catalogues[$row['id']]['files'] = [];
            }
            $c_temp = &$catalogues[$row['id']];
            if ($row['file_id']) {
                if (!isset($c_temp['files'][$row['file_id']])) {
                    $c_temp['files'][$row['file_id']] = [
                        'file_id' => $row['file_id'],
                        'appointment' => $row['file_appointment']
                    ];
                }
            }
        }
        unset($c_temp);
        $fileModel = new Api_files();
        foreach ($catalogues as &$catalogue) {
            if (!empty($catalogue['files'])) {
                $_files = $fileModel->get_files_by_ids(Helpers_common::column_as_value($catalogue['files'], 'file_id'), ['catalogue_logo', 'catalogue_header']);
                foreach ($_files as $file) {
                    $catalogue['files'][$file['id']] = array_merge($catalogue['files'][$file['id']], $file);
                }
            }
        }
        return $catalogues;
    }

    /**
     * Возвращает информацию о рубрике каталога с указанным TAG
     * @param string $catalog_tag
     * @return Array
     */
    public static function get_catalogue_by_tag($catalog_tag)
    {
        $db = DataBase::getDB();
        $query = "
        select
            `sk`.`id` as `id`,
            `sk`.`title` as `title`,
            `sk`.`label` as `label`,
            `sk`.`second_title` as `second_title`,
            `sk`.`description` as `description`,
            `sk`.`content` as `content`,
            `sk`.`type` as `type`,
            `sk`.`template` as `template`,
            `sk`.`cols` as `cols`,
            `sk`.`rows` as `rows`
        from `catalog` as `sk`
        where `sk`.`tag`='{$catalog_tag}'";
        return $db->selectRow($query);
    }

    /**
     * Возвращает весь каталог для последующего постороения дерева и вычлинения потомков
     * @return array
     */
    public static function full_catalogue()
    {
        $db = DataBase::getDB();

        $query = "
        select
            `sk`.`id` as `id`,
            `sk`.`parent_id` as `parent_id`
        from `catalog` as `sk`
        ";

        return $db->select($query);
    }

    /**
     * Возвращает все дерево каталога
     * @return array
     */
    public static function full_catalogue_tree($catalog_id = null, $parent_catalogue = null, $is_deleted = null)
    {
        $db = DataBase::getDB();
        $query = "
        select
            `sk`.`id` as `id`,
            `sk`.`title` as `title`,
            `sk`.`path` as `path`,
            `sk`.`sorting` as `sort`,
            `sk`.`parent_id` as `parent_id`
        from `catalog` as `sk`
        ";
        if ($is_deleted !== null and ($is_deleted == 0 || $is_deleted == 1)) {
            $query .= " where `sk`.`is_deleted`=" . $is_deleted;
        }
        $query .= " order by `sk`.`sorting` desc";

        $tree = $db->select($query);
        $tree = Helpers_common::columnAsKey($tree, 'id');

        //отмечаем путь от текущего каталога к корню
        if (!empty($catalog_id)) {
            $path = $catalog_id;
            while (!empty($tree[$path])) {
                $tree[$path]['in_path'] = true;
                $path = $tree[$path]['parent_id'];
            }
            if ($path == 0) $tree[0]['in_path'] = true;
            if ($parent_catalogue && isset($tree[$parent_catalogue])) {
                $tree[$parent_catalogue]['in_path'] = true;
            }
        } else {
            $tree[0]['in_path'] = true;
            if ($parent_catalogue && isset($tree[$parent_catalogue])) {
                $tree[$parent_catalogue]['in_path'] = true;
            }
        }

        foreach ($tree as $id => $data) {
            $tree[$data['parent_id']]['child'][$id] =& $tree[$id];
            if ($id == $parent_catalogue) {
                $treeByParent = & $tree[$id];
            }
        }
        return ($parent_catalogue && isset($treeByParent)) ? $treeByParent : $tree[0];
    }

    /**
     * Возвращает дочерние элементы каталога
     * @param $catalog_id
     */
    public static function all_child($catalog_id)
    {
        $catalog_res = self::full_catalogue();
        $catalog = Helpers_common::columnAsKey($catalog_res, 'id');
        foreach ($catalog as $id => $data) {
            $catalog[$data['parent_id']]['child'][$id] =& $catalog[$id];
        }
        $catalog[0]['id'] = 0;
        $result = Helpers_common::slice_by_parent($catalog_id, $catalog[0]);
        if ($result) {
            $result = Helpers_common::get_childs_from_tree($result);
        } else {
            $result = array();
        }
        return $result;
    }

    /**
     * Возвращает путь к каталогу по url с информацией по каталогу
     * @param array $url
     * @return boolean|array
     */
    public static function get_structure_by_url($url)
    {
        $url_length = count($url);

        $db = DataBase::getDB();

        if (empty($url[0])) {
            $res = self::get_catalogue_by_tag('root');
            if (is_array($res)) {
                $result = array($res);
            }
        } else {
            $fields = array();

            for ($i = 0; $i <= ($url_length - 1); $i++) {
                $fields[] = "`sk{$i}`.`id` as `id{$i}`";
                $fields[] = "`sk{$i}`.`title` as `title{$i}`";
                $fields[] = "`sk{$i}`.`parent_id` as `parent_id{$i}`";
                $fields[] = "`sk{$i}`.`label` as `label{$i}`";
                $fields[] = "`sk{$i}`.`path` as `path{$i}`";
            }
            $fields[] = "`sk" . ($url_length - 1) . "`.`content` as `content" . ($url_length - 1) . "`";
            $fields[] = "`sk" . ($url_length - 1) . "`.`description` as `description" . ($url_length - 1) . "`";
            $fields[] = "`sk" . ($url_length - 1) . "`.`type` as `type" . ($url_length - 1) . "`";
            $fields[] = "`sk" . ($url_length - 1) . "`.`template` as `template" . ($url_length - 1) . "`";
            $fields[] = "`sk" . ($url_length - 1) . "`.`meta` as `meta" . ($url_length - 1) . "`";
            $fields[] = "`sk" . ($url_length - 1) . "`.`cols` as `cols" . ($url_length - 1) . "`";
            $fields[] = "`sk" . ($url_length - 1) . "`.`rows` as `rows" . ($url_length - 1) . "`";
            $fields[] = "`sk" . ($url_length - 1) . "`.`path` as `path" . ($url_length - 1) . "`";


            $from_left = array();
            $from_left[] = "from `catalog` as `sk0`";
            for ($i = 1; $i <= ($url_length - 1); $i++) {
                $from_left[] = "left join `catalog` as `sk{$i}` on `sk{$i}`.`parent_id` = `sk" . ($i - 1) . "`.`id`";
            }
            $where = array();
            for ($i = 0; $i <= ($url_length - 1); $i++) {
                $where[] = "`sk{$i}`.`label` = {?}";
                $where[] = "`sk{$i}`.`is_deleted` = 0";
            }
            $query = "
            select
                " . implode(', ', $fields) . "
                " . implode(' ', $from_left) . "
            where
                " . implode(' and ', $where) . "
            limit 0,1
            ";
            $res = $db->selectRow($query, $url);

            $result = array();
            if (is_array($res)) {
                foreach ($res as $key => $value) {
                    if (preg_match('/(\S+)(\d+)/', $key, $match)) {
                        $result[$match[2]][$match[1]] = $value;
                    }
                }

            }

            $target_catalog = &$result[count($result)-1];
            if ($target_catalog['id']) {
                $target_catalog['mode'] = $db->select("select `mode` from `catalogue_mode` where `catalogue_id`=".$target_catalog['id']);
            } else {
                $target_catalog['mode'] = [];
            }
        }
        return (is_array($res) ? $result : false);
    }

    /**
     * Генерим чпу для каждой рубрики
     * @param int $id
     * @param string $path
     */
    public static function generate_path($id = 1, $path = '')
    {
        $db = DataBase::getDB();
        $query = "update catalog set `path` = '" . str_replace('//', '/' , $path) . "' where `id` =" . $id;
        $db->query($query);
        $query = "select `id`, `label` from `catalog` where `parent_id` = " . $id;
        $children = $db->select($query);
        foreach ($children as $child) {
            self::generate_path($child['id'], $path . '/' . $child['label']);
        }

    }

    //изменение сортировки
    public static function sortChange($id, $to)
    {
        $db = DataBase::getDB();
        $current = $db->selectRow("select `parent_id`, `sorting` from `catalog` where `id` = {?}", [$id]);
        $query = ($to == 'up') ? "select `id`, `sorting` from `catalog` where `sorting` > {?} and `parent_id`={?} order by `sorting` asc limit 0,1"
            : "select `id`, `sorting` from `catalog` where `sorting` < {?} and `parent_id`={?} order by `sorting` desc limit 0,1";
        $neighbor = $db->selectRow($query, [$current['sorting'], $current['parent_id']]);
        $updateQuery = "update catalog set `sorting`={?} where `id`={?}";
        $db->query($updateQuery, [$current['sorting'], $neighbor['id']]);
        $db->query($updateQuery, [$neighbor['sorting'], $id]);
    }
}