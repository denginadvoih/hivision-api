<?php
/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 07.02.15
 * Time: 17:21
 */

class Models_fields
{
    public static function get_field($id_label, $full = false)
    {
        //TODO сделать выборку по доубл полям
        $db = DataBase::getDB();
        $query = "
        select
        `f`.*,
         `r`.`title` as `reference_title`,
         `ri`.`id` as `item_id`,
         `ri`.`parent_id` as `item_parent_id`,
         `ri`.`title` as `item_title`,
         `ri`.`is_deleted` as `item_is_deleted`,
         `ri_child`.`id` as `child_item_id`,
         `ri_child`.`parent_id` as `child_item_parent_id`,
         `ri_child`.`title` as `child_item_title`
        from `fields` as `f`
        left join `references` as `r` on (`r`.`id` = `f`.`reference_id`)
        left join `reference_items` as `ri` on (`ri`.`reference_id` = `r`.`id`)
        left join `reference_items` as `ri_child` on (`f`.`type` = 'double_reference' and `ri`.`id` = `ri_child`.`parent_id`)
        where " . (is_numeric($id_label) ? "`f`.`id` = {?}" : "`f`.`label` = {?}");
        $res = $db->select($query, [$id_label]);
        $field = [];
        if (is_array($res)) {
            $field = [
                'id' => $res[0]['id'],
                'title' => $res[0]['title'],
                'label' => $res[0]['label'],
                'type' => $res[0]['type']
            ];
            if ($field['type'] == 'reference' || $field['type'] == 'double_reference') {
                $field['reference_id'] = $res[0]['reference_id'];
                $field['reference']['items'] = [];
                foreach ($res as $value) {
                    if (!isset($field['reference']['items'][$value['item_id']])) {
                        $field['reference']['items'][$value['item_id']] = [
                            'id' => $value['item_id'],
                            'title' => $value['item_title'],
                            'parent_id' => $value['item_parent_id'],
                            'is_deleted' => $value['item_is_deleted'],
                            'items' => []
                        ];
                    }
                    if (!empty($value['child_item_id']) && !isset($field['reference']['items'][$value['child_item_parent_id']]['items'][$value['child_item_id']])) {
                        $field['reference']['items'][$value['child_item_parent_id']]['items'][$value['child_item_id']] = [
                            'id' => $value['child_item_id'],
                            'title' => $value['child_item_title'],
                            'parent_id' => $value['child_item_parent_id']
                        ];
                    }
                }
            }
        }
        return $field;
    }

    /**
     * получение полей
     */
    public static function get_fields($ids = null)
    {
        $db = DataBase::getDB();
        $query = "select * from `fields`" . (!empty($ids) ? ' where `id` in (' . implode(',', $ids) . ')' : '');
        $fields = $db->select($query);
        if (is_array($fields)) {
            $reference_ids = array_map(function ($field) {
                return $field['reference_id'];
            }, $fields);
            $reference_ids = array_filter($reference_ids, function ($id) {
                return !empty($id);
            });
            if (!empty($reference_ids)) {
                $query = "
                select `r`.`id`, `r`.`title`
                from `references` as `r`
                where `r`.`id` in (" . implode(',', $reference_ids) . ")
                ";
                $references = $db->select($query);
                if (is_array($references)) {
                    $references = Helpers_common::columnAsKey($references, 'id');
                    foreach ($fields as &$value) {
                        if ($value['reference_id'] != 0 && isset($references[$value['reference_id']])) {
                            $value['reference_title'] = $references[$value['reference_id']]['title'];
                        }
                    }
                }
            }
        }
        return $fields;
    }

    /**
     * получение справочника
     * @return array|bool
     */
    public static function get_reference($id, $parent_item = null)
    {
        $db = DataBase::getDB();
        $query = "select
        `r`.*,
        `pr`.`id` as `parent_id`,
        `pr`.`title` as `parent_title`,
         `ri`.`title` as `el_title`,
         `ri`.`id` as `el_id`
        from `references` as `r`
        left join `references` as `pr` on (`r`.`parent_id` = `pr`.`id`)
        left join `reference_items` as `ri` on (`r`.`id` = `ri`.`reference_id` " . ($parent_item ? " and `ri`.`parent_id` = " . (int)$parent_item : '') . ")
        where `r`.`id` = {?}";

        $res = $db->select($query, [$id]);
        $reference = [];
        if (is_array($res)) {
            $reference = $res[0];
            unset($reference['el_title']);
            unset($reference['el_id']);
            $reference['items'] = [];
            foreach ($res as $value) {
                $reference['items'][] = ['id' => $value['el_id'], 'title' => $value['el_title']];
            }
        }
        return $reference;
    }

    /**
     * получение справочника по тегу
     * @return array|bool
     */
    public static function get_reference_by_label($label, $parent_item = null)
    {
        $db = DataBase::getDB();
        $query = "select `r`.`id` from `references` as `r` where `r`.`label` = {?}";
        $id = $db->selectCell($query, [$label]);
        $reference = [];
        if (!empty($id)) {
            $reference = self::get_reference($id, $parent_item);
        }
        return $reference;
    }

    /**
     * получение всех справочников
     * @return array|bool
     */
    public static function get_references()
    {
        $db = DataBase::getDB();
        $query = "select `r`.*, `pr`.`title` as `parent_title` from `references` as `r`
        left join `references` as `pr` on (`r`.`parent_id` = `pr`.`id`)
        ";
        return $db->select($query);
    }

    /**
     * получение родительских справочников
     * @return array|bool
     */
    public static function get_parent_references()
    {
        $db = DataBase::getDB();
        $query = "
        select `r`.*, (count(`cr`.`id`) > 0) as `is_parent`
        from `references` as `r`
        left join `references` as `cr` on (`cr`.`parent_id` = `r`.`id`)
        where `r`.`parent_id`=0
        group by `r`.`id`
        ";
        return $db->select($query);
    }

    /**
     * получение дочерних элементов справочника
     * @param integer $child_id
     * @return array|bool
     */
    public static function get_child_reference_items($child_id)
    {
        $db = DataBase::getDB();
        $query = "select * from `reference_items` where `parent_id` = " . (int)$child_id;
        return $db->select($query);
    }

    /*
     * получение элемента справочника по id
     */
    public static function get_reference_item($id)
    {
        $db = DataBase::getDB();
        $query = "select * from `reference_items` where `id` = " . (int)$id;
        return $db->selectRow($query);
    }
    /*
     * получение элементов справочников по параметрам
     */
    public static function get_references_items($params = [])
    {
        $db = DataBase::getDB();
        $where = [];
        if (isset($params['is_deleted'])) {
            $where[] = "`ri`.`is_deleted` = " . (int) $params['is_deleted'];
        }
        $query = "select `ri`.*, `r`.`title` as `reference_title` from `reference_items` as `ri` 
            left join `references` as `r` on (`ri`.`reference_id` = `r`.`id`) "
            . (count($where) ? (' where ' .  implode(' and ', $where)) : '');
        return $db->select($query);
    }

    public static function mark_deleted_ref_item($id)
    {
        $db = DataBase::getDB();
        $query = "update `reference_items` set `is_deleted` = 1 where `id` = {?}";
        return $db->query($query, [$id]);
    }
}