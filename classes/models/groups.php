<?php
/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 07.02.15
 * Time: 17:21
 */

class Models_groups
{
    public static function delete($id)
    {
        $db = DataBase::getDB();
        $query = "delete from `staff_groups` where `group_id` = {?}";
        $db->query($query, [$id]);
        $query = "delete from `groups` where `id` = {?}";
        return $db->query($query, [$id]);
    }
}