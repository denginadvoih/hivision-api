<?php
/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 07.02.15
 * Time: 17:21
 */

class Models_mailings
{
    public static function create($params)
    {
        $db = DataBase::getDB();
        $query = "insert into `mailings` (`date_start`, `subject`, `message`) values ({?}, {?}, {?})";
        //die($db->getQuery($query, [$params['date_start'], $params['subject'], $params['message']]));
        return $db->query($query, [$params['date_start'], $params['subject'], $params['message']]);
    }

    public static function get($params)
    {
        $db = DataBase::getDB();
        $where = [];
        if (isset($params['status'])) {
            $where[] = "`status` = '" . $params['status'] . "'";
        }

        if (isset($params['date_start'])) {
            $where[] = "`date_start` <= '" . $params['date_start'] . "'";
        }

        $query = "select * from `mailings` " . (!empty($where) ? "where " . implode(' and ', $where) : '') . ' order by `id` desc';
        if (isset($params['limit']) && isset($params['offset'])) {
            $query .= ' limit ' . (int)$params['offset'] . ', ' . (int)$params['limit'];
        }
        $items =  $db->select($query);
        if (is_array($items)) {
            foreach ($items as &$item) {
                $item['expired'] = strtotime($item['date_start']) < time();
            }
        }
        $queryTotal = "select count(*) as `total` from `mailings` " . (!empty($where) ? "where " . implode(' and ', $where) : '');
        $total = $db->selectCell($queryTotal);
        return ['total' => $total, 'items' => $items];
    }

    public static function update($mailing)
    {
        $db = DataBase::getDB();
        $query = "update `mailings` set `subject`={?}, `message`={?}, `date_start`={?} where `id`={?} ";
        return $db->query($query, [$mailing['subject'], $mailing['message'], $mailing['date_start'], $mailing['id']]);
    }

    public static function get_by_id($id)
    {
        $db = DataBase::getDB();
        $query = "select * from `mailings` where `id`={?}";
        return $db->selectRow($query, [$id]);
    }

    public static function cancel($id)
    {
        $db = DataBase::getDB();
        $query = "update `mailings` set `status` = 'canceled' where `id`={?}";
        return $db->query($query, [$id]);
    }

    public static function start($id)
    {
        $db = DataBase::getDB();
        $query = "update `mailings` set `date_start` = '" . date('Y-m-d H:i:s', time()) . "' where `id`={?}";
        return $db->query($query, [$id]);
    }

    public static function finish($id)
    {
        $db = DataBase::getDB();
        $query = "update `mailings` set `status` = 'finished' where `id`={?}";
        return $db->query($query, [$id]);
    }

    public static function update_progress($id)
    {
        $db = DataBase::getDB();
        $query = "update `mailings` set `progress_step` = `progress_step` + 1 where `id`={?}";
        return $db->query($query, [$id]);
    }

}