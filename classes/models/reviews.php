<?php
/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 07.02.15
 * Time: 17:21
 */

class Models_reviews
{
    public static function create($review)
    {
        $db = DataBase::getDB();
        $query = "insert into `reviews` (`user_id`, `user_type`, `user_name`, `staff_id`, `catalogue_id`, `message`, `date_insert`)
        values ({?}, {?}, {?}, {?}, {?}, {?}, {?})";
        return $db->query($query,
            [$review['user_id'], $review['user_type'], $review['user_name'], $review['staff_id'], $review['catalogue_id'], $review['message'], date('Y-m-d H:i:s', time())]);
    }

    public static function get($params)
    {
        $db = DataBase::getDB();
        $where = [];
        if (isset($params['catalogue_id'])) {
            $where[] = "`r`.`catalogue_id` = " . (int)$params['catalogue_id'];
        }
        if (isset($params['staff_id'])) {
            $where[] = "`r`.`staff_id` = " . (int)$params['staff_id'];
        }
        if (isset($params['approved'])) {
            $where[] = "`r`.`approved` = " . (int)$params['approved'];
        }
        $query = "select `r`.*, `uc`.`name` from `reviews` as `r`"  . "
        left join `users_contacts` as `uc` on (`r`.`user_type` = 'registered' and `uc`.`user_id` = `r`.`user_id`) "
        . (!empty($where) ? "where " . implode(' and ', $where) : '') . "
        order by `r`.`id` desc";
        if (isset($params['limit']) && isset($params['offset'])) {
            $query .= ' limit ' . (int)$params['offset'] . ', ' . (int)$params['limit'];
        }
        $items =  $db->select($query);
        if (is_array($items)) {
            foreach ($items as &$item) {
                $item['review_date'] = Helpers_common::convert_date($item['date_insert'], 'ru');
                $item['answer_date'] = Helpers_common::convert_date($item['date_update'], 'ru');
            }
        }
        unset($item);
        $queryTotal = "select count(*) as `total` from `reviews` as `r` " . (!empty($where) ? "where " . implode(' and ', $where) : '');
        $total = $db->selectCell($queryTotal);




        return ['total' => $total, 'items' => $items];
    }

    public static function update($review)
    {
        $db = DataBase::getDB();
        $query = "update `reviews` set `approved`={?}, `message`={?}, `answer`={?} where `id`={?} ";
        return $db->query($query, [$review['approved'], $review['message'], $review['answer'], $review['id']]);
    }

    public static function get_by_id($id)
    {
        $db = DataBase::getDB();
        $query = "select * from `reviews` where `id`={?}";
        return $db->selectRow($query, [$id]);
    }

}