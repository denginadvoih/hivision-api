<?php
/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 07.02.15
 * Time: 17:21
 */

class Models_shop
{
    /**
     * Возврат возможных скидок
     * @return array
     */
    public static function discount()
    {
        $auth = Auth_auth::get_instance();
        $user = $auth->getUser();
        if ($user['type'] == 'registered') {
            $userContacts = $auth->getUserContacts($user['id']);
            $userDiscount = $userContacts['discount'];
        } else {
            $userDiscount = 0;
        }
        $globalDiscount = Api_option::get_by_tag('global_discount');
        $globalDiscount = (int)$globalDiscount['content'];
        $sizeDiscount = Api_option::get_by_tag('size_discount', null);
        $sizeDiscount = $sizeDiscount ? json_decode($sizeDiscount['content'], true) : 0;
        usort($sizeDiscount, function($a, $b){
            if($a['sum '] === $b['sum'])
                return 0;

            return $a['sum'] > $b['sum'] ? 1 : -1;
        });

        return [
            'user' => $userDiscount,
            'global' => $globalDiscount,
            'size' => $sizeDiscount
        ];
    }
}