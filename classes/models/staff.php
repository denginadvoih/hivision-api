<?php
/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 07.02.15
 * Time: 17:21
 */

class Models_staff
{
    public static function mark_deleted($id)
    {
        $db = DataBase::getDB();
        $_staff = Api_staff::get_staff_by_ids([$id]);
        $staff = $_staff[$id];
        $file_ids = array_map(function($file) {return $file['id'];}, $staff['files']);
        $fileModel = new Api_files();
        $fileModel->to_be_removed($file_ids);
        $query = "update `staff` set `is_deleted` = 1 where `id` = {?}";
        $db->query($query, [$id]);
    }
}