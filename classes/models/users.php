<?php
/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 07.02.15
 * Time: 17:21
 */

class Models_users
{
    public static function get($params)
    {
        $db = DataBase::getDB();
        $where = [];
        if (isset($params['mailing'])) {
            $where[] = "`uc`.`mailing` = ' " . Helpers_common::validValue($params['mailing'], [0, 1], 1) . "'";
        }
        if (!empty($params['search'])) {
            $sWords = explode(' ', $params['search']);
            $sWordsWhere = [];
            foreach($sWords as $value) {
                if (is_numeric($value)) {
                    $sWordsWhere[] =  "`uc`.`phone` like '%" . trim($value) . "%'";
                } else {
                    if (substr_count($value, '@')) {
                        $sWordsWhere[] =  "`u`.`email` like '%" . trim($value) . "%'";
                    } else {
                        $sWordsWhere[] =  "`uc`.`name` like '%" . trim($value) . "%' or `uc`.`surname` like '%" . trim($value) . "%' or `uc`.`family` like '%" . trim($value) . "%'";
                    }
                }
            }
            if (!empty($sWordsWhere)) {
                $where[] = implode(' and ', $sWordsWhere);
            }
        }
        $query = "select * from `users` as `u`
        left join `users_contacts` as `uc` on (`u`.`id` = `uc`.`user_id`)
        " . (!empty($where) ? "where " . implode(' and ', $where) : '') . ' order by `id` desc';
        if (isset($params['limit']) && isset($params['offset'])) {
            $query .= ' limit ' . (int)$params['offset'] . ', ' . (int)$params['limit'];
        }
        $items =  $db->select($query);
        $queryTotal = "select count(*) as `total` from `users` as `u` left join `users_contacts` as `uc` on (`u`.`id` = `uc`.`user_id`) " . (!empty($where) ? "where " . implode(' and ', $where) : '');
        $total = $db->selectCell($queryTotal);
        return ['total' => $total, 'items' => $items];
    }

    public static function get_by_id($id)
    {
        $db = DataBase::getDB();
        $query = "select * from `users` as `u` left join `users_contacts` as `uc` on (`u`.`id` = `uc`.`user_id`) where `id`={?}";
        return $db->selectRow($query, [$id]);

    }

    public static function update_discount($id, $discount)
    {
        $db = DataBase::getDB();
        $query = "update `users_contacts` set `discount`={?} where `user_id`={?}";
        return $db->query($query, [$discount, $id]);
    }
}