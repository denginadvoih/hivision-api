<?php //defined('SYSPATH') or die('No direct script access.');

class Modules_back
{
    //стандартная форма обратной связи
    public function backmail()
    {
        $twig = Twig::get_instance();
        $mail = Api_option::get_by_tag('email');
        return $twig->template
            ->loadTemplate('backmail.twig')
            ->render(array('email' => $mail['content']));
    }

    //вывод всяческих форма
    public function request_form($id)
    {
        $request = Api_requests::get_request($id);
        return Twig::get_instance()->template
            ->loadTemplate('modules/request-form.twig')
            ->render([
                'request' => $request,
                'input_class' => 'col-md-12',
                'fields_template' => 'modules/field-form.twig'
            ]);
    }

}
