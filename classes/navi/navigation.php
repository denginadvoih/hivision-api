<?php //defined('SYSPATH') or die('No direct script access.');

class Navi_navigation
{
    public function hmenu($catalog_id)
    {
        $twig = Twig::get_instance();
        $hmenu_tree = Models_catalogue::get_catalogue_by_hmenu($catalog_id);
        return $twig->template
            ->loadTemplate('hmenu.twig')
            ->render(['url' => $_SERVER['REQUEST_URI'], 'tree' => $hmenu_tree]);
    }

    public function search_form($catalog_id = 1)
    {
        $twig = Twig::get_instance();
        $catalog = Models_catalogue::get_catalogue($catalog_id);
        return $twig->template
            ->loadTemplate('modules/search-form.twig')
            ->render(['catalog' => $catalog]);
    }

    public static function catalog($parent_catalogue, $catalog_id = null)
    {
        $twig = Twig::get_instance();
        $catalog = Models_catalogue::full_catalogue_tree($catalog_id, $parent_catalogue, 0);
        return $twig->template
            ->loadTemplate('modules/catalogtree.twig')
            ->render(array('tree' => $catalog));
    }

    public static function field_menu($catalog_id, $field_id_label)
    {
        $field = Api_fields::get_field($field_id_label);
        $catalog = Models_catalogue::get_catalogue($catalog_id);
        $navi = [];
        foreach ($field['reference']['items'] as $item) {
            if ($item['is_deleted'] != 1)
            $navi[] = [
                'title' => $item['title'],
                'url' => $catalog['path'] . '?fields[' . $field['id'] . '][] = ' . $item['id'],
                'items' => array_map(function ($c_item) use ($field, $item, $catalog) {
                    return ['title' => $c_item['title'], 'url' => $catalog['path'] . '?fields[' . $field['id'] . '][]=' . $c_item['id'] . '&fields[' . $field['id'] . '][]=' . $item['id']];
                }, $item['items'])
            ];
        }
        //\Pr::p_($navi);
        return Twig::get_instance()->template
            ->loadTemplate('menu-field.twig')
            ->render([
                    'hiddenlevel' => 1,
                    'level' => 0,
                    'field' => $field,
                    'items' => $navi,
                    'parent_id' => null,
                    'selected' => isset($_GET['fields'][$field['id']]) ? $_GET['fields'][$field['id']] : null]
            );
    }

    public static function filter($catalog_id = null, $field_label = null)
    {
        $catalog = Models_catalogue::get_catalogue_expand($catalog_id);

        if (!empty($field_label)) {
            $field_key = Helpers_arr::array_func_search($catalog['fields'], function ($field) use ($field_label) {
                echo $field['label'] . ' ' . $field_label;
                return $field['label'] == $field_label;
            });
            if (empty($field_key)) {
                return '';
            }
            $field = $catalog['fields'][$field_key];
            $catalog['fields'] = [$field['id'] => $field];
        }

        $item['fields'] = [];
        if (isset($_GET['fields'])) {
            foreach ($_GET['fields'] as $field_id => $field) {
                if (is_array($field)) {
                    foreach ($field as $field_value) {
                        $item['fields'][$field_id]['value_ids'][] = $field_value;
                    }
                } else {
                    $item['fields'][$field_id]['value_ids'][] = $field;
                }
            }
        }
        $item['cfields'] = [];
        if (isset($_GET['cfields'])) {
            foreach ($_GET['cfields'] as $field_id => $field) {
                if (is_array($field)) {
                    foreach ($field as $field_value) {
                        $item['cfields'][$field_id]['value_ids'][] = $field_value;
                    }
                } else {
                    $item['cfields'][$field_id]['value_ids'][] = $field;
                }
            }
        }
        return Twig::get_instance()->template
            ->loadTemplate('filter-field-form.twig')
            ->render(['fields' => $catalog['fields'],
                'item' => $item]);
    }
}
