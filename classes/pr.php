<?php
/**
 * User: Takamura
 * Date: 17.11.13 0:06
 */
class Pr
{

    public static function p_()
    {
        echo '<pre>';
        $list = func_get_args();
        foreach ($list as $value) {
            print_r($value);
            echo '<br/>';
        }
        echo '</pre>';
        echo self::bktr();
        die();
    }

    public static function v_()
    {
        echo '<pre>';
        $list = func_get_args();
        foreach ($list as $value) {
            var_dump($value);
            echo '<br/>';
        }
        echo '</pre>';
        echo self::bktr();
        die();
    }

    public static function p()
    {
        echo '<pre>';
        $list = func_get_args();
        foreach ($list as $value) {
            print_r($value);
            echo '<br/>';
        }
        echo '</pre>';
        echo self::bktr();
    }

    public static function pw()
    {
        echo '<pre>';
        $list = func_get_args();
        foreach ($list as $value) {
            print_r($value);
            echo '<br/>';
        }
        echo '</pre>';
    }

    public static function pc()
    {

        $list = func_get_args();
        foreach ($list as $value) {
            print_r($value);
        }

    }

    public static function pc_()
    {

        $list = func_get_args();
        foreach ($list as $value) {
            print_r($value);
        }
        die();

    }

    private static function bktr()
    {
        $traceArr = debug_backtrace();
        $trace = array();
        $i = 0;
        foreach ($traceArr as $key => $tr) {
            $i ++;
            if ($i < 3) {
                continue;
            }
            $trace[] = "[{$key}] " .
                (isset($tr['file']) ? $tr['file'] . "[" . $tr['line'] . "] " : '') .
                (isset($tr['class']) ? $tr['class'] : '') .
                (isset($tr['type']) ? $tr['type'] : '') .
                (isset($tr['function']) ? $tr['function'] : '');
            if ($i > 5) {
                break;
            }
        }
        return implode('<br/>', $trace);
    }
} 