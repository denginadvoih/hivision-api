<?php

/**
 * Created by PhpStorm.
 * User: AlexJ
 * Date: 07.02.15
 * Time: 18:55
 */
class router
{

    public $object_url = null;
    public $url_array = array();
    public $controller = 'Front';
    public $action = 'index';
    public $page = 1;
    public static $params = array();
    public $clear_route = '';

    public function route()
    {
        require './vendors/fastroute/src/bootstrap.php';
        $dispatcher = FastRoute\simpleDispatcher(function (FastRoute\RouteCollector $r) {
            //обновление валюты
            $r->addRoute('GET', null, '/cash_update', 'cash_update');
            //авторизация и выход, админка
            $r->addRoute('POST', null, '/site-construction/auth', 'admin_auth');
            $r->addRoute('GET', null, '/site-construction/logout', 'admin_logout');
            //авторизация и выход, front
            $r->addRoute('POST', null, '/user/make_auth', 'user_auth');
            $r->addRoute('GET', null, '/user/make_logout', 'user_logout');
            $r->addRoute('GET', null, '/user/make_register', 'user_register_form');
            $r->addRoute('POST', null, '/user/make_register', 'user_register');
            $r->addRoute('GET', null, '/user/make_update', 'user_update_form');
            $r->addRoute('POST', null, '/user/make_update', 'user_update');
            $r->addRoute('GET', null, '/user/you_are_registered', 'user_is_registered');
            $r->addRoute('GET', null, '/user/confirmation', 'user_confirmation');
            $r->addRoute('GET', null, '/user/remember_password', 'user_remember_password');
            $r->addRoute('POST', null, '/user/remember_password', 'user_send_password_remember');
            $r->addRoute('GET', null, '/user/restore_password', 'user_restore_password_form');
            $r->addRoute('POST', null, '/user/restore_password', 'user_restore_password');
            $r->addRoute('GET', null, '/user/new_password_ok', 'user_new_password_ok');
            $r->addRoute('GET', null, '/user/check_email', 'ajax/check_email');
            //корзина
            $r->addRoute('POST', null, '/ajax/basket/{id:[0-9]+}', 'basket_add');
            $r->addRoute('DELETE', null, '/ajax/basket/{id:[0-9]+}', 'basket_remove');
            $r->addRoute('POST', null, '/ajax/basket/{id:[0-9]+}/{change}', 'basket_change');
            $r->addRoute('GET', null, '/basket', 'basket');
            $r->addRoute('GET', null, '/basket/confirm', 'basket_confirm_form');
            $r->addRoute('POST', null, '/basket/confirm', 'basket_confirm');
            $r->addRoute('GET', null, '/orders/send', 'basket_send');
            $r->addRoute('GET', null, '/orders/{id:[0-9]+}/pay', 'orders_pay');
            $r->addRoute('POST', null, '/orders/pay', 'front_basket_orders/pay_result');
            $r->addRoute('POST', null, '/orders/pay_is_ok', 'front_basket_orders/pay_is_ok');
            //каталог
            $r->addRoute('GET', ['GOD'], '/site-construction', 'admin_catalog');
            $r->addRoute('GET', ['GOD'], '/site-construction/catalog/{catalog_id:[0-9]+}', 'admin_catalog');
            $r->addRoute('GET', ['GOD'], '/site-construction/catalog/{catalog_id:[0-9]+}/page{page:[0-9]+}', 'admin_catalog');
            $r->addRoute('GET', ['GOD'], '/site-construction/catalog/{catalog_id:[0-9]+}/new', 'admin_catalog_new');
            $r->addRoute('POST', ['GOD'], '/site-construction/catalog/{catalog_id:[0-9]+}/new', 'admin_catalog_create');
            $r->addRoute('GET', ['GOD'], '/site-construction/catalog/{catalog_id:[0-9]+}/edit', 'admin_catalog_edit');
            $r->addRoute('POST', ['GOD'], '/site-construction/catalog/{catalog_id:[0-9]+}/edit', 'admin_catalog_save');
            $r->addRoute('GET', ['GOD'], '/site-construction/catalog/{id:[0-9]+}/sort-{to}', 'admin_ajax_catalog/sort');
            $r->addRoute('GET', ['GOD'], '/site-construction/staff_catalogue', 'admin_staff/catalogue_indexing');
            //отзывы
            $r->addRoute('GET', ['GOD'], '/site-construction/reviews', 'admin_reviews/get');
            //каталог ajax
            $r->addRoute('GET', ['GOD'], '/site-construction/ajax/catalog/{id:[0-9]+}', 'admin_ajax_catalog_get');
            $r->addRoute('GET', ['GOD'], '/site-construction/ajax/catalog/{id:[0-9]+}/expand', 'admin_ajax_catalog_get_expand');
            $r->addRoute('GET', ['GOD'], '/site-construction/ajax/catalog/{id:[0-9]+}/expand_with_field_form', 'admin_ajax_catalog_get_expand_with_field_form');
            $r->addRoute('GET', ['GOD'], '/site-construction/ajax/references/{id:[0-9]+}', 'admin_ajax_reference_get');
            $r->addRoute('DELETE', ['GOD'], '/site-construction/ajax/catalog/{id:[0-9]+}', 'admin_ajax_catalog_delete');
            //работа с группами в админке
            $r->addRoute('GET', ['GOD'], '/site-construction/groups', 'admin_groups/get');
            $r->addRoute('POST', ['GOD'], '/site-construction/groups', 'admin_groups/save');
            $r->addRoute('DELETE', ['GOD'], '/site-construction/groups/{id:[0-9]+}', 'admin_groups/delete');
            $r->addRoute('GET', ['GOD'], '/site-construction/groups/add', 'admin_groups/create_form');
            $r->addRoute('POST', ['GOD'], '/site-construction/groups/add', 'admin_groups/create');
            $r->addRoute('GET', ['GOD'], '/site-construction/groups/{group_id:[0-9]+}/staff/{id:[0-9]+}', 'admin_ajax_groups/linkstaff');
            //настройки
            $r->addRoute('GET', ['GOD'], '/site-construction/settings', 'admin_settings');
            $r->addRoute('POST', ['GOD'], '/site-construction/settings', 'admin_settings_save');
            //рекламный блок
            $r->addRoute('GET', ['GOD'], '/site-construction/adblock/new', 'admin_adblock_new');
            $r->addRoute('POST', ['GOD'], '/site-construction/adblock/new', 'admin_adblock_create');
            $r->addRoute('GET', ['GOD'], '/site-construction/adblocks', 'admin_adblocks');
            $r->addRoute('POST', ['GOD'], '/site-construction/adblocks', 'admin_adblocks_save');
            $r->addRoute('GET', ['GOD'], '/site-construction/adblocks/{id:[0-9]+}', 'admin_adblock');
            $r->addRoute('GET', ['GOD'], '/site-construction/adblocks/banners/{id:[0-9]+}', 'admin_banner_edit');
            $r->addRoute('POST', ['GOD'], '/site-construction/adblocks/banners/{id:[0-9]+}', 'admin_banner_save');
            $r->addRoute('DELETE', ['GOD'], '/site-construction/ajax/adblocks/banners/{id:[0-9]+}', 'admin_banner_delete');
            //каталог ajax
            $r->addRoute('GET', ['GOD'], '/site-construction/ajax/fields/add_staff_value/{id:[0-9]+}', 'admin_ajax_fields_add_staff_value');
            //работа с объектами
            $r->addRoute('GET', ['GOD'], '/site-construction/catalog/{id:[0-9]+}/new-item', 'admin_item_new');
            $r->addRoute('POST', ['GOD'], '/site-construction/catalog/{id:[0-9]+}/new-item', 'admin_item_create');
            $r->addRoute('GET', ['GOD'], '/site-construction/catalog/{catalog_id:[0-9]+}/staff/{id:[0-9]+}', 'admin_item_edit');
            $r->addRoute('POST', ['GOD'], '/site-construction/catalog/{catalog_id:[0-9]+}/staff/{id:[0-9]+}', 'admin_item_save');
            $r->addRoute('GET', ['GOD'], '/site-construction/catalog/{catalog_id:[0-9]+}/staff/{id:[0-9]+}/sort-{to}', 'admin_ajax_staff/sort');
            $r->addRoute('DELETE', ['GOD'], '/site-construction/ajax/staff/{id:[0-9]+}', 'admin_item_delete');
            //работа с файлами в админке
            $r->addRoute('POST', ['GOD'], '/site-construction/ckeditor_upload', 'admin_ckeditor_upload');
            $r->addRoute('GET', ['GOD'], '/site-construction/ckeditor_upload', 'admin_ckeditor_upload');
            $r->addRoute('POST', ['GOD'], '/site-construction/form_files_upload', 'admin_form_files_upload');
            $r->addRoute('DELETE', ['GOD'], '/site-construction/files/{id:[0-9]+}', 'admin_files_delete');
            $r->addRoute('GET', ['GOD'], '/site-construction/files/{id:[0-9]+}/main', 'admin_files_main');
            $r->addRoute('GET', ['GOD'], '/site-construction/files/reset', 'admin_files_reset');
            //работа с дополнительными полями
            $r->addRoute('GET', ['GOD'], '/site-construction/fields', 'admin_fields');
            $r->addRoute('GET', ['GOD'], '/site-construction/fields/add', 'admin_fields_add');
            $r->addRoute('POST', ['GOD'], '/site-construction/fields/add', 'admin_fields_create');
            $r->addRoute('GET', ['GOD'], '/site-construction/fields/{id:[0-9]+}/edit', 'admin_fields_edit');
            $r->addRoute('POST', ['GOD'], '/site-construction/fields/{id:[0-9]+}/edit', 'admin_fields_save');
            $r->addRoute('GET', ['GOD'], '/site-construction/fields/{id:[0-9]+}/delete', 'admin_fields_delete');
            //справочники
            $r->addRoute('GET', ['GOD'], '/site-construction/references', 'admin_references');
            $r->addRoute('GET', ['GOD'], '/site-construction/references/add', 'admin_references_add');
            $r->addRoute('POST', ['GOD'], '/site-construction/references/add', 'admin_references_create');
            $r->addRoute('GET', ['GOD'], '/site-construction/references/{id:[0-9]+}/edit', 'admin_references_edit');
            $r->addRoute('POST', ['GOD'], '/site-construction/references/{id:[0-9]+}/edit', 'admin_references_save');
            $r->addRoute('DELETE', ['GOD'], '/site-construction/references/items/{id:[0-9]+}', 'admin_ajax_fields/delete_reference_item');
            //справочники AJX
            $r->addRoute('GET', null, '/site-construction/ajax/references/items/{child_id:[0-9]+}', 'admin_ajax_reference_items_child');
            //элементы справочников
            $r->addRoute('GET', ['GOD'], '/site-construction/references/items', 'admin_reference_items');
            $r->addRoute('GET', ['GOD'], '/site-construction/references/items/add', 'admin_reference_items_add');
            $r->addRoute('POST', ['GOD'], '/site-construction/references/items/add', 'admin_reference_items_create');
            //управление интернет магазином
            $r->addRoute('GET', ['GOD'], '/site-construction/orders', 'admin_orders');
            $r->addRoute('POST', ['GOD'], '/site-construction/orders', 'admin_orders_save');
            $r->addRoute('GET', ['GOD'], '/site-construction/orders/{id:[0-9]+}', 'admin_orders/show');
            $r->addRoute('POST', ['GOD'], '/site-construction/orders/{id:[0-9]+}', 'admin_orders/update');
            $r->addRoute('GET', ['GOD'], '/site-construction/shipping', 'admin_shipping');
            $r->addRoute('POST', ['GOD'], '/site-construction/shipping', 'admin_shipping_save');
            $r->addRoute('GET', ['GOD'], '/site-construction/shipping/new', 'admin_shipping_add');
            $r->addRoute('POST', ['GOD'], '/site-construction/shipping/new', 'admin_shipping_create');
            $r->addRoute('DELETE', ['GOD'], '/site-construction/shipping/{id:[0-9]+}', 'admin_ajax_shipping_delete');
            $r->addRoute('GET', ['GOD'], '/site-construction/customers', 'admin_customers/get_list');
            $r->addRoute('GET', ['GOD'], '/site-construction/price', 'admin_staff/price_edit');
            $r->addRoute('POST', ['GOD'], '/site-construction/price', 'admin_staff/price_save');
            $r->addRoute('GET', ['GOD'], '/site-construction/discount', 'admin_settings/discount_edit');
            $r->addRoute('POST', ['GOD'], '/site-construction/discount', 'admin_settings/discount_save');
            //управление рассылками
            $r->addRoute('GET', ['GOD'], '/site-construction/mailings', 'admin_mailing/mailings');
            $r->addRoute('POST', ['GOD'], '/site-construction/mailings', 'admin_mailing/save_mailings');
            $r->addRoute('GET', ['GOD'], '/site-construction/mailing/new', 'admin_mailing/add');
            $r->addRoute('POST', ['GOD'], '/site-construction/mailing/new', 'admin_mailing/create');
            $r->addRoute('GET', ['GOD'], '/site-construction/mailing/{id:[0-9]+}', 'admin_mailing/mailing');
            $r->addRoute('POST', ['GOD'], '/site-construction/mailing/{id:[0-9]+}', 'admin_mailing/save_mailing');
            $r->addRoute('POST', ['GOD'], '/site-construction/ajax/mailing/{id:[0-9]+}/cancel', 'admin_ajax_mailing/cancel');
            $r->addRoute('POST', ['GOD'], '/site-construction/ajax/mailing/{id:[0-9]+}/start', 'admin_ajax_mailing/start');
            //публичные ajax запросы
            $r->addRoute('GET', null, '/ajax/{name}', 'ajax_get');
            $r->addRoute('GET', null, '/ajax/cities/{id}', 'ajax_get_cities');
            //публичные ajax запросы POST
            $r->addRoute('POST', null, '/ajax/requestsend', 'ajax_requestsend');
            $r->addRoute('POST', null, '/ajax/reviews', 'ajax/create_review');
            $r->addRoute('POST', ['GOD'], '/ajax/reviews/{id:[0-9]+}', 'ajax/update_review');
            $r->addRoute('POST', ['GOD'], '/ajax/users/{id:[0-9]+}/discount', 'ajax/update_discount');
            $r->addRoute('POST', ['GOD'], '/ajax/orders/post_id', 'admin_ajax_orders/update_post_id');
            $r->addRoute('DELETE', ['GOD'], '/ajax/orders/{id:[0-9]+}', 'admin_ajax_orders/mark_deleted');

            //публичные ajax запросы POST
            $r->addRoute('POST', null, '/ajax/mmctracking', 'ajax_mmctracking');

            //и если что-то нужно сделать
            $r->addRoute('GET', null, '/make_some/{action}', 'make_some');

            $r->addRoute('GET', ['GOD'], '/parser', 'front_parser/form');
            $r->addRoute('GET', ['GOD'], '/parser/total', 'front_parser/total');
            $r->addRoute('GET', ['GOD'], '/parser/{offset:[0-9]+}', 'front_parser/parse');
            $r->addRoute('GET', ['GOD'], '/parser/total/reviews', 'front_parser/total_reviews');
            $r->addRoute('GET', ['GOD'], '/parser/reviews/{offset:[0-9]+}', 'front_parser/parse_reviews');
            $r->addRoute('GET', ['GOD'], '/parser/total/users', 'front_parser/total_users');
            $r->addRoute('GET', ['GOD'], '/parser/users/{offset:[0-9]+}', 'front_parser/parse_users');
            $r->addRoute('GET', ['GOD'], '/parser/total/orders', 'front_parser/total_orders');
            $r->addRoute('GET', ['GOD'], '/parser/orders/{offset:[0-9]+}', 'front_parser/parse_orders');
            
            
            //Api
            $r->addRoute('GET', null, '/catalogue/tree/{tag}', 'front_catalogue/tree');
            $r->addRoute('GET', null, '/fields/{field}', 'front_fields/get');

            $r->addRoute('POST', null, '/feedback', 'front_feedback/send');
            $r->addRoute('POST', null, '/i-want-know-more', 'front_feedback/sendRequestForDetailedInfo');


            //lampReference
            $r->addRoute('GET', null, '/lamp_reference/car_marks', 'front_lampreference/car_marks');
            $r->addRoute('GET', null, '/lamp_reference/car_models/{mark}', 'front_lampreference/car_models');
            $r->addRoute('GET', null, '/lamp_reference/car_model_numbers/{model}', 'front_lampreference/car_model_numbers');
            $r->addRoute('GET', null, '/lamp_reference/car_model_generations/{modelNumber}', 'front_lampreference/car_model_generations');
            $r->addRoute('GET', null, '/lamp_reference/lamps/{modelGeneration}', 'front_lampreference/lamps');
            $r->addRoute('GET', null, '/lamp_reference/parser', 'front_lampreference/parser');
            //lamp_reference/car_models/chevrolet 404 (Not Found)

            $r->addRoute('GET', null, '/mailing', 'tasks/mailing');

            //Core
            //$r->addRoute('OPTIONS', null, null, 'front/core');

        });
        $request_uri = $_SERVER['REQUEST_URI'];
        $request_comps = parse_url($request_uri);
        $this->clear_route = $request_comps['path'];
        $auth = Auth_auth::get_instance();
        $userId = $auth->getUserId();
        $userRoles = $auth->getUserRole($userId);


        //core
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            $this->controller = 'front';
            $this->action = 'core';
        }

        if ($_SERVER['REQUEST_METHOD'] !== 'OPTIONS') {
            $routeInfo = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], $this->clear_route, $userRoles);
            switch ($routeInfo['match']) {
                case FastRoute\Dispatcher::NOT_FOUND:
                    //определяем отключение сайт under construction
                    $working = Api_option::get_by_tag('working');
                    if (!$working['content']) {
                        //роуты связанные с фронтом
                        $this->controller = 'front';
                        $this->action = 'not_work';
                    } else {
                        //параметры GET
                        $this->params = $_GET;
                        $request = substr($request_comps['path'], 1);
                        $request_array = explode('/', $request);
                        $this->controller = 'front';
                        $this->url_array = $request_array;
                        if (preg_match("/(.*)\.html/", $request_array[count($request_array) - 1], $last_piece)) {
                            $this->controller .= '_catalogue';
                            $this->object_url = $last_piece[1];
                            $this->url_array = array_slice($request_array, 0, count($request_array) - 1);
                            $this->clear_route = '/' . implode('/', $this->url_array);
                            $this->action = 'viewobject';
                        } else {
                            $this->controller .= '_catalogue';
                            if (preg_match("/page(\d+)/", $request_array[count($request_array) - 1], $last_piece)) {
                                $this->params['page'] = $last_piece[1];
                                $this->url_array = array_slice($request_array, 0, count($request_array) - 1);
                                $this->clear_route = '/' . implode('/', $this->url_array);
                            } else {
                                $this->params['page'] = 1;
                            }
                            $this->action = 'viewcontent';
                        }
                    }
                    break;
                case FastRoute\Dispatcher::USER_NOT_ALLOWED:
                    $handler = $routeInfo['handler'];
                    $this->params = $routeInfo['vars'];
                    switch ($handler) {
                        default:
                            $this->on_deny_handler($this->clear_route);
                            break;
                    }
                    break;
                case FastRoute\Dispatcher::FOUND:
                    $handler = $routeInfo['handler'];
                    $this->params = $routeInfo['vars'];
                    switch ($handler) {
                        case 'admin_ajax_catalog/sort':
                        case 'ajax/create_review':
                        case 'ajax/update_review':
                        case 'ajax/update_discount':
                        case 'admin_ajax_orders/update_post_id':
                        case 'admin_ajax_orders/mark_deleted':
                        case 'admin_mailing/mailings':
                        case 'admin_mailing/save_mailings':
                        case 'admin_mailing/add':
                        case 'admin_mailing/create':
                        case 'admin_mailing/mailing':
                        case 'admin_mailing/save_mailing':
                        case 'admin_ajax_mailing/cancel':
                        case 'admin_ajax_mailing/start':
                        case 'admin_ajax_staff/sort':
                        case 'admin_orders/show':
                        case 'admin_orders/update':
                        case 'front_parser/form':
                        case 'front_parser/total':
                        case 'front_parser/parse':
                        case 'front_parser/total_reviews':
                        case 'front_parser/parse_reviews':
                        case 'front_parser/total_users':
                        case 'front_parser/parse_users':
                        case 'front_parser/total_orders':
                        case 'front_parser/parse_orders':
                        case 'tasks/mailing':
                        case 'admin_ajax_fields/delete_reference_item':
                        case 'ajax/check_email':
                        case 'admin_staff/price_edit':
                        case 'admin_staff/price_save':
                        case 'admin_settings/discount_edit':
                        case 'admin_settings/discount_save':
                        case 'front_catalogue/tree':
                        case 'front_lampreference/car_marks':
                        case 'front_lampreference/car_models':
                        case 'front_lampreference/car_model_numbers':
                        case 'front_lampreference/car_model_generations':
                        case 'front_lampreference/lamps':
                        case 'front_lampreference/parser':
                        case 'front_feedback/send':
                        case 'front_feedback/sendRequestForDetailedInfo':

                        case 'front/core':
                            list($this->controller, $this->action) = explode('/', $handler);
                            break;
                        case 'ajax_get_cities':
                            $this->controller = 'ajax';
                            $this->action = 'get_cities';
                            break;
                        case 'ajax_requestsend':
                            $this->controller = 'ajax';
                            $this->action = 'requestsend';
                            break;
                        case 'ajax_mmctracking':
                            $this->controller = 'ajax';
                            $this->action = 'mmc_tracker';
                            break;
                        case 'admin_catalog':
                            $this->controller = 'admin_catalog';
                            $this->action = 'index';
                            break;
                        case 'admin_catalog_new':
                            $this->controller = 'admin_catalog';
                            $this->action = 'add';
                            break;
                        case 'admin_catalog_create':
                            $this->controller = 'admin_catalog';
                            $this->action = 'create';
                            break;
                        case 'admin_catalog_edit':
                            $this->controller = 'admin_catalog';
                            $this->action = 'edit';
                            break;
                        case 'admin_catalog_save':
                            $this->controller = 'admin_catalog';
                            $this->action = 'save';
                            break;
                        //catalog ajax
                        case 'admin_ajax_catalog_get':
                            $this->controller = 'admin_ajax_catalog';
                            $this->action = 'get';
                            break;
                        case 'admin_ajax_catalog_get_expand':
                            $this->controller = 'admin_ajax_catalog';
                            $this->action = 'get_expand';
                            break;
                        case 'admin_ajax_catalog_get_expand_with_field_form':
                            $this->controller = 'admin_ajax_catalog';
                            $this->action = 'get_expand_with_field_form';
                            break;
                        case 'admin_ajax_catalog_delete':
                            $this->controller = 'admin_ajax_catalog';
                            $this->action = 'delete';
                            break;
                        //настройки
                        case 'admin_settings':
                            $this->controller = 'admin_settings';
                            $this->action = 'index';
                            break;
                        case 'admin_settings_save':
                            $this->controller = 'admin_settings';
                            $this->action = 'save';
                            break;
                        //поля ajax
                        case 'admin_ajax_reference_get':
                            $this->controller = 'admin_ajax_fields';
                            $this->action = 'get_reference';
                            break;
                        case 'admin_ajax_reference_items_child':
                            $this->controller = 'admin_ajax_fields';
                            $this->action = 'get_child_reference';
                            break;
                        case 'admin_ajax_fields_add_staff_value':
                            $this->controller = 'admin_ajax_fields';
                            $this->action = 'add_staff_value';
                            break;
                        case 'admin_item_new':
                            $this->controller = 'admin_staff';
                            $this->action = 'add';
                            break;
                        case 'admin_item_create':
                            $this->controller = 'admin_staff';
                            $this->action = 'create';
                            break;
                        case 'admin_item_edit':
                            $this->controller = 'admin_staff';
                            $this->action = 'edit';
                            break;
                        case 'admin_item_save':
                            $this->controller = 'admin_staff';
                            $this->action = 'save';
                            break;
                        case 'admin_item_delete':
                            $this->controller = 'admin_ajax_staff';
                            $this->action = 'delete';
                            break;
                        //работа с файлами
                        case 'admin_ckeditor_upload':
                            $this->controller = 'admin_files';
                            $this->action = 'ckupload';
                            break;
                        case 'admin_form_files_upload':
                            $this->controller = 'admin_files';
                            $this->action = 'formfilesupload';
                            break;
                        case 'admin_files_delete':
                            $this->controller = 'admin_files';
                            $this->action = 'delete';
                            break;
                        case 'admin_files_main':
                            $this->controller = 'admin_files';
                            $this->action = 'makemain';
                            break;
                        case 'admin_files_reset':
                            $this->controller = 'admin_files';
                            $this->action = 'reset';
                            break;
                        //поля
                        case 'admin_fields':
                            $this->controller = 'admin_fields';
                            $this->action = 'get';
                            break;
                        case 'admin_fields_add':
                            $this->controller = 'admin_fields';
                            $this->action = 'add';
                            break;
                        case 'admin_fields_create':
                            $this->controller = 'admin_fields';
                            $this->action = 'create';
                            break;
                        case 'admin_fields_edit':
                            $this->controller = 'admin_fields';
                            $this->action = 'edit';
                            break;
                        case 'admin_fields_save':
                            $this->controller = 'admin_fields';
                            $this->action = 'save';
                            break;
                        case 'admin_fields_delete':
                            $this->controller = 'admin_fields';
                            $this->action = 'delete';
                            break;
                        //справочники
                        case 'admin_references':
                            $this->controller = 'admin_fields';
                            $this->action = 'get_references';
                            break;
                        case 'admin_references_add':
                            $this->controller = 'admin_fields';
                            $this->action = 'add_reference';
                            break;
                        case 'admin_references_create':
                            $this->controller = 'admin_fields';
                            $this->action = 'create_reference';
                            break;
                        case 'admin_references_edit':
                            $this->controller = 'admin_fields';
                            $this->action = 'edit_reference';
                            break;
                        case 'admin_references_save':
                            $this->controller = 'admin_fields';
                            $this->action = 'save_reference';
                            break;
                        //элементы справочников
                        case 'admin_reference_items':
                            $this->controller = 'admin_fields';
                            $this->action = 'get_references_items';
                            break;
                        case 'admin_reference_items_add':
                            $this->controller = 'admin_fields';
                            $this->action = 'add_reference_items';
                            break;
                        case 'admin_reference_items_create':
                            $this->controller = 'admin_fields';
                            $this->action = 'create_reference_items';
                            break;
                        //управление интернет магазином
                        case 'admin_orders':
                            $this->controller = 'admin_orders';
                            $this->action = 'get';
                            break;
                        case 'admin_orders_save':
                            $this->controller = 'admin_orders';
                            $this->action = 'save';
                            break;
                        case 'admin_shipping_add':
                            $this->controller = 'admin_shipping';
                            $this->action = 'add';
                            break;
                        case 'admin_shipping_create':
                            $this->controller = 'admin_shipping';
                            $this->action = 'create';
                            break;
                        case 'admin_shipping':
                            $this->controller = 'admin_shipping';
                            $this->action = 'get';
                            break;
                        case 'admin_shipping_save':
                            $this->controller = 'admin_shipping';
                            $this->action = 'save';
                            break;
                        case 'admin_ajax_shipping_delete':
                            $this->controller = 'admin_ajax_shipping';
                            $this->action = 'delete';
                            break;
                        //баннерные блоки
                        case 'admin_adblock':
                            $this->controller = 'admin_ad';
                            $this->action = 'block';
                            break;
                        case 'admin_adblocks':
                            $this->controller = 'admin_ad';
                            $this->action = 'blocks';
                            break;
                        case 'admin_adblocks_save':
                            $this->controller = 'admin_ad';
                            $this->action = 'blocks_save';
                            break;
                        case 'admin_adblock_new':
                            $this->controller = 'admin_ad';
                            $this->action = 'add';
                            break;
                        case 'admin_adblock_create':
                            $this->controller = 'admin_ad';
                            $this->action = 'create';
                            break;
                        case 'admin_banner_delete':
                            $this->controller = 'admin_ajax_ads';
                            $this->action = 'delete';
                            break;
                        case 'admin_banner_edit':
                            $this->controller = 'admin_ad';
                            $this->action = 'edit';
                            break;
                        case 'admin_banner_save':
                            $this->controller = 'admin_ad';
                            $this->action = 'save';
                            break;
                        //работа с группами
                        case 'admin_groups/get':
                        case 'admin_groups/save':
                        case 'admin_groups/delete':
                        case 'admin_groups/create_form':
                        case 'admin_groups/create':
                        case 'admin_ajax_groups/linkstaff':
                        case 'admin_staff/catalogue_indexing':
                        case 'admin_reviews/get':
                        case 'admin_customers/get_list':
                            list($this->controller, $this->action) = explode('/', $handler);
                            break;
                        //авторизация
                        case 'admin_auth':
                            $this->controller = 'admin';
                            $this->action = 'auth';
                            break;
                        case 'admin_logout':
                            $this->controller = 'admin';
                            $this->action = 'logout';
                            break;
                        //обновление валюты
                        case 'cash_update':
                            $this->controller = 'tasks';
                            $this->action = 'cash_update';
                            break;
                        //авторизация на фронте
                        case 'user_auth':
                            $this->controller = 'front_users';
                            $this->action = 'auth';
                            break;
                        case 'user_logout':
                            $this->controller = 'front_users';
                            $this->action = 'logout';
                            break;
                        case 'user_register_form':
                            $this->controller = 'front_users';
                            $this->action = 'add';
                            break;
                        case 'user_register':
                            $this->controller = 'front_users';
                            $this->action = 'create';
                            break;
                        case 'user_update_form':
                            $this->controller = 'front_users';
                            $this->action = 'edit';
                            break;
                        case 'user_update':
                            $this->controller = 'front_users';
                            $this->action = 'save';
                            break;
                        case 'user_is_registered':
                            $this->controller = 'front_users';
                            $this->action = 'is_registered';
                            break;
                        case 'user_confirmation':
                            $this->controller = 'front_users';
                            $this->action = 'confirmation';
                            break;
                        case 'user_remember_password':
                            $this->controller = 'front_users';
                            $this->action = 'remember_password';
                            break;
                        case 'user_send_password_remember':
                            $this->controller = 'front_users';
                            $this->action = 'send_remember_password';
                            break;
                        case 'user_restore_password_form':
                            $this->controller = 'front_users';
                            $this->action = 'restore_password_form';
                            break;
                        case 'user_restore_password':
                            $this->controller = 'front_users';
                            $this->action = 'restore_password';
                            break;
                        case 'user_new_password_ok':
                            $this->controller = 'front_users';
                            $this->action = 'new_password_ok';
                            break;
                        //корзина
                        case 'basket':
                            $this->controller = 'front_basket_basket';
                            $this->action = 'index';
                            break;
                        case 'basket_confirm_form':
                            $this->controller = 'front_basket_basket';
                            $this->action = 'confirm_form';
                            break;
                        case 'basket_confirm':
                            $this->controller = 'front_basket_basket';
                            $this->action = 'confirm';
                            break;
                        case 'basket_add':
                            $this->controller = 'front_basket_ajax';
                            $this->action = 'add';
                            break;
                        case 'basket_remove':
                            $this->controller = 'front_basket_ajax';
                            $this->action = 'remove';
                            break;
                        case 'basket_send':
                            $this->controller = 'front_basket_basket';
                            $this->action = 'send';
                            break;
                        case 'basket_change':
                            $this->controller = 'front_basket_ajax';
                            $this->action = 'change';
                            break;
                        case 'orders_pay':
                            $this->controller = 'front_basket_orders';
                            $this->action = 'pay';
                            break;
                        case 'front_basket_orders/pay_result':
                        case 'front_basket_orders/pay_is_ok':
                            list($this->controller, $this->action) = explode('/', $handler);
                            break;

                        //что-то нужно сделать
                        case 'make_some':
                            $this->controller = 'tasks';
                            $this->action = $this->params['action'];
                            break;
                        default:
                            list($this->controller, $this->action) = explode('/', $handler);
                            break;
                    }
                    break;
            }
        }

    }

    private function on_deny_handler($url)
    {
        $auth = Auth_auth::get_instance();
        $userId = $auth->getUserId();
        if (substr_count($url, ADMIN_SUB_URL) === 1) {
            if (is_integer($userId)) {
/*                $this->controller = 'admin_catalog';
                $this->action = 'index';*/
                header('Location: /');
                exit();
            } else {
                $this->controller = 'admin';
                $this->action = 'auth';
            }
        } else {
            if (is_integer($userId)) {
                $this->controller = 'front_catalog';
                $this->action = 'viewcontent';
            } else {
                $this->controller = 'front_catalog';
                $this->action = 'viewcontent';
            }
        }
    }

}