<?php

class Twig {
    private static $twig = null; // Единственный экземпляр класса, чтобы не создавать множество подключений
    public $template;


    /* Получение экземпляра класса. Если он уже существует, то возвращается, если его не было, то создаётся и возвращается (паттерн Singleton) */
    public static function get_instance() {
        if (self::$twig == null) self::$twig = new Twig();
        return self::$twig;
    }

    public function __construct()
    {
        Twig_Autoloader::register();

        $loader = new Twig_Loader_Filesystem(DOCROOT.'templates');

        $this->template = new Twig_Environment($loader, array(
            //'cache' => DOCROOT.'templates/compilation_cache',
            'charset'=>'utf-8'
        ));
    }
    //заменитель модулей админки
    public function admin_replacer()
    {
        $controllerFilter = new Twig_SimpleFilter('modules', function ($name) {
            $m_params = explode(',', $name);
            switch ($m_params[0]) {
                case 'paginator':
                    return Amodules_navigation::paginator($m_params[1], $m_params[2]);
                    break;
                case 'a_sysmenu':
                    return Amodules_navigation::sysmenu();
                    break;
                case 'a_catalog':
                    return Amodules_navigation::catalog($m_params[1]);
                    break;
                case 'tree_select':
                    return Amodules_form::tree_select(isset($m_params[1]) ? $m_params[1] : null, isset($m_params[2]) ? $m_params[2] : null);
                    break;
                case 'template_select':
                    return Amodules_form::template_select($m_params[1]);
                    break;
            }
        }, array('is_safe' => array('html')));

        $this->template->addFilter($controllerFilter);
    }

    //заменитель модулей фронта
    public function front_replacer($current_catalog)
    {
        $controllerFilter = new Twig_SimpleFilter('modules', function ($name) use($current_catalog) {
            $m_params = explode(',', $name);
            $navi = new Navi_navigation();
            $vidgetsGallery = new Vidgets_gallery();
            $vidgetsText = new Vidgets_text();
            $vidgetsAuth = new Vidgets_auth();
            $back = new Modules_back();
            $ad = new Vidgets_ad();
            $out = '';
            switch ($m_params[0]) {
                case 'auth_info':
                    $out = $vidgetsAuth->auth_info();
                    break;
                case 'basket':
                    $out = $vidgetsText->basket();
                    break;
                case 'hmenu':
                    $out = $navi->hmenu($m_params[1]);
                    break;
                case 'search-form':
                    $out = $navi->search_form($m_params[1]);
                    break;
                case 'catalog':
                    return $navi->catalog($m_params[1], $current_catalog['id']);
                    break;
                case 'gallery-six-and-row':
                    $out = $vidgetsGallery->big_and_row($m_params[1]);
                    break;
                case 'nivo-gallery':
                    $out = $vidgetsGallery->nivo_gallery($m_params[1], $m_params[2]);
                    break;
                case 'ad':
                    $out = $ad->ad($m_params[1]);
                    break;
                case 'vidgets-cataloginfo':
                    $out = $vidgetsText->catalog_info($m_params[1]);
                    break;
                case 'vidgets-option':
                    $out = $vidgetsText->option_info($m_params[1]);
                    break;
                case 'vidgets-cash-informer':
                    $out = $vidgetsText->cash_informer();
                    break;
                case 'staff_filter':
                    $out = $navi->filter(empty($m_params[1]) ? $current_catalog['id'] : $m_params[1], empty($m_params[2]) ? null : $m_params[2]);
                    break;
                case 'field_menu':
                    $out = $navi->field_menu(empty($m_params[1]) ? $current_catalog['id'] : $m_params[1], empty($m_params[2]) ? null : $m_params[2]);
                    break;
                case 'modules-backmail':
                    $out = $back->backmail();
                    break;
                case 'some-form':
                    $out = $back->request_form($m_params[1]);
                    break;
                case 'vidgets-present':
                    $out = $vidgetsText->staff_present($m_params[1], $m_params[2], $m_params[3]);
                    break;
                case 'vidgets-present-group':
                    $out = $vidgetsText->group_present($m_params[1], $m_params[2], $m_params[3]);
                    break;
                case 'catalogname':
                    $out = $current_catalog['title'];
                    break;
                case 'cataloginfo':
                    $out = $current_catalog['content'];
                    break;
                case 'title':
                    $out = $current_catalog['title'].' '.$current_catalog['meta']['title'];
                    break;
                case 'description':
                    $out = $current_catalog['description'].' '.$current_catalog['meta']['description'];
                    break;

                //кастомные модули от разных заказчиков
                case 'mmc_tracking':
                    $out = $this->template
                        ->loadTemplate('modules/custom/mmc-tracking.twig')
                        ->render([]);
                    break;
                case 'content':
                    $out = $this->template
                        ->loadTemplate('catalog-full-description.twig')
                        ->render([
                            'catalog' => $current_catalog
                        ]);
                    break;
                case 'keywords':
                    $out = $current_catalog['meta']['keywords'];
                    break;
            }
            return $out;
        }, array('is_safe' => array('html')));

        $this->template->addFilter($controllerFilter);
    }
} 