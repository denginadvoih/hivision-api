<?php //defined('SYSPATH') or die('No direct script access.');

class Vidgets_ad
{
    public $templates = ['slide' => 'ad/nivo-gallery.twig', 'block' => 'ad/block.twig'];
    /*
    * вывод галереи по файлам
    */
    public function ad($block_id)
    {
        $twig = Twig::get_instance();
        $block = Api_Ads::get_block($block_id);
        $ads = Api_Ads::get_staff_by_block($block_id, ['gallery_main']);
        $gallery_id = rand(0, 1000);
        return $twig->template
            ->loadTemplate($this->templates[$block['type']])
            ->render([
                'ads' => $ads,
                'gallery_id' => $gallery_id,
                'block' => $block
            ]);
    }
}
