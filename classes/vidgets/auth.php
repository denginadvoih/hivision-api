<?php //defined('SYSPATH') or die('No direct script access.');
class Vidgets_auth
{
    /*
     * Выводит информацио о рубрике
     * @param int $catalog_id
     * @return string
     */
    public function auth_info()
    {
        $auth = Auth_auth::get_instance();
        $user = $auth->getUser();
        if ($user['type'] == 'registered') {
            $user['contacts'] = $auth->getUserContacts($user['id']);
        }
        $twig = Twig::get_instance();
        return $twig->template
            ->loadTemplate('vidgets/auth-info.twig')
            ->render(['user' => $user]);
    }
}
