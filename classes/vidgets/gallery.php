<?php //defined('SYSPATH') or die('No direct script access.');

class Vidgets_gallery
{
    public function big_and_row($catalog_id)
    {
        $twig = Twig::get_instance();
        $pics = Api_staff::get_staff_files($catalog_id, 0, 8, 'staff_medium');

        return $twig->template
            ->loadTemplate('big-and-row.twig')
            ->render(array('pics' => $pics));

    }

    /*
     * вывод галереи по id каталога
     */
    public function nivo_gallery($catalog_id, $gallery_id)
    {
        $twig = Twig::get_instance();
        $staff = Api_staff::get_staff_by(['catalog_id' => $catalog_id, 'scheme' => 'gallery_main'], 0, 8);
        return $twig->template
            ->loadTemplate('nivo-gallery.twig')
            ->render([
                'items' => $staff['items'],
                'gallery_id' => $gallery_id
            ]);
    }

    /*
    * вывод галереи по файлам
    */
    public function nivo_gallery_by_files($files, $gallery_id = null)
    {
        $twig = Twig::get_instance();
        if (!$gallery_id) {
            $gallery_id = rand(0, 1000);
        }
        return $twig->template
            ->loadTemplate('nivo-gallery-by-files.twig')
            ->render([
                'files' => $files,
                'gallery_id' => $gallery_id
            ]);
    }
    /*
* вывод галереи с превью, для просмотра объекта
*/
    public function viewobject_gallery_by_files($files, $gallery_id = null)
    {
        $twig = Twig::get_instance();
        if (!$gallery_id) {
            $gallery_id = rand(0, 1000);
        }
        return $twig->template
            ->loadTemplate('owl-gallery-by-files.twig')
            ->render([
                'files' => $files,
                'gallery_id' => $gallery_id
            ]);
    }
}
