<?php //defined('SYSPATH') or die('No direct script access.');

class Vidgets_text
{
    /*
     * Выводит информацио о рубрике
     * @param int $catalog_id
     * @return string
     */
    public function catalog_info($catalog_id)
    {
        $twig = Twig::get_instance();
        $info = Models_catalogue::get_catalogue($catalog_id);
        return $twig->template
            ->loadTemplate('catalog-info.twig')
            ->render(array('info' => $info));
    }

    /* Выводит информацио о рубрике
     * @param int $catalog_id
     * @return string
     */
    public function option_info($option_tag)
    {
        $twig = Twig::get_instance();
        $option = Api_option::get_by_tag($option_tag);
        if (isset($option['content'])) $option['content'] = nl2br($option['content']);
        return $twig->template
            ->loadTemplate('option-info.twig')
            ->render(array('option' => $option));
    }

    /* Выводит некоторое количество последний сущностей из рубрики
     * @param int $catalog_id, limit
     * @return string
     */
    public function staff_present($catalog_id, $limit, $cols)
    {
        $twig = Twig::get_instance();
        $parent = Models_catalogue::get_catalogue($catalog_id);
        $staff = Api_staff::get_staff_by(['catalog_id' => $catalog_id, 'is_deleted' => 0], 0, $limit);
        foreach ($staff['items'] as &$value) {
            $value['date'] = Helpers_common::convert_date($value['date_insert'], 'ru');
        }
        $global_discount = Api_option::get_by_tag('global_discount');
        return $twig->template
            ->loadTemplate('staff-present.twig')
            ->render([
                'items' => $staff['items'],
                'parent' => $parent,
                'cols' => $cols,
                'global_discount' => $global_discount
            ]);
    }

    /* Выводит некоторое количество последний сущностей из рубрики
    * @param int $catalog_id, limit
    * @return string
    */
    public function group_present($group_id, $limit, $cols)
    {
        $twig = Twig::get_instance();
        if (!is_numeric($group_id)) {
            if ($group = Api_Groups::get_by_label($group_id)) {
                $group_id = $group['id'];
            };
        }
        $parent = Api_Groups::get_by_id($group_id);
        $staff = Api_staff::get_staff_by(['group_id' => $group_id, 'is_deleted' => 0], 0, $limit);
        $catalogues = [];
        foreach ($staff['items'] as &$value) {
            $value['date'] = Helpers_common::convert_date($value['date_insert'], 'ru');
            $catalogues[] = $value['catalog_id'];
        }
        $global_discount = Api_option::get_by_tag('global_discount');
        return $twig->template
            ->loadTemplate('staff-present.twig')
            ->render([
                'block_id' => 'group_' . $parent['label'],
                'items' => $staff['items'],
                'parent' => $parent,
                'cols' => $cols,
                'global_discount' => $global_discount
            ]);
    }

    public function cash_informer ($cash_titles = null)
    {
        $db = DataBase::getDB();
        $twig = Twig::get_instance();
        $query = "select * from `cash`" . (empty($cash_titles) ? '' : ' where `title` in (' . implode(',', $cash_titles) . ')');
        $cash = $db->select($query);
        if (is_array($cash)) {
            foreach ($cash as &$value) {
                $value['date'] = Helpers_common::convert_date($value['date']);
                $value['old_date'] = Helpers_common::convert_date($value['old_date']);
            }
        }
        return $twig->template
            ->loadTemplate('vidgets/cash-informer.twig')
            ->render([
                'cash' => $cash
            ]);
    }

    public function basket()
    {
        $basket = Api_Basket::basket_state();
        $twig = Twig::get_instance();
        return $twig->template
            ->loadTemplate('vidgets/basket.twig')
            ->render([
                'basket' => $basket
            ]);
    }
}
