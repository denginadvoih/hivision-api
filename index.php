<?php
define('SYSPATH', realpath('..' . DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR);
define('DOCROOT', realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR);
define('PAGE_SIZE', 10);
define('SITE_NAME', 'hivision.pro');
define('ADMIN_SUB_URL', 'site-construction');
require_once './vendors/Twig-1.18.0/lib/Twig/Autoloader.php';
spl_autoload_register('autoload');
function autoload($className)
{

    $dirs = explode('_', $className);
    $dirs = array_map(function ($el) {
        return strtolower($el);
    }, $dirs);
    if (is_file($file = DOCROOT . 'classes/' . implode('/', $dirs) . '.php')) {
        require $file;
    } else {

    }
}
Auth_auth::get_instance();
$router = new router();

$router->route(); //даем команду роутеру обрабатывать REQUEST_URI

$_controller = 'Controllers_' . $router->controller;
$_action = $router->action;
$controller = new $_controller($router->url_array, $router->params, $router->clear_route, $router->object_url);
$controller->$_action();
?>
