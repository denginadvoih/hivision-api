<?php

namespace FastRoute\Dispatcher;

class GroupCountBased extends RegexBasedAbstract {
    public function __construct($data) {
        list($this->staticRouteMap, $this->variableRouteData, $this->routeRoles) = $data;
    }

    protected function dispatchVariableRoute($routeData, $uri) {
        foreach ($routeData as $data) {
            if (!preg_match($data['regex'], $uri, $matches)) {
                continue;
            }

            list($handler, $varNames) = $data['routeMap'][count($matches)];

            $vars = [];
            $i = 0;
            foreach ($varNames as $varName) {
                $vars[$varName] = $matches[++$i];
            }
            return ['match' => self::FOUND, 'handler' => $handler, 'vars' => $vars];
        }

        return ['match' => self::NOT_FOUND, 'handler' => null, 'vars' => []];
    }
}
