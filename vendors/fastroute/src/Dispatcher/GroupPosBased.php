<?php

namespace FastRoute\Dispatcher;

class GroupPosBased extends RegexBasedAbstract {
    public function __construct($data) {
        list($this->staticRouteMap, $this->variableRouteData, $this->routeRoles) = $data;
    }

    protected function dispatchVariableRoute($routeData, $uri) {
        foreach ($routeData as $data) {
            if (!preg_match($data['regex'], $uri, $matches)) {
                continue;
            }
            // find first non-empty match
            for ($i = 1; '' === $matches[$i]; ++$i);
            list($handler, $varNames) = $data['routeMap'][$i];
            $vars = [];
            foreach ($varNames as $varName) {
                $vars[$varName] = $matches[$i++];
            }
            return ['match' => self::FOUND, 'handler' => $handler, 'vars' => $vars];
        }
        return ['match' => self::NOT_FOUND, 'handler' => null, 'vars' => []];
    }
}
