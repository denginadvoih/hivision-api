<?php

namespace FastRoute\Dispatcher;

use FastRoute\Dispatcher;

abstract class RegexBasedAbstract implements Dispatcher {
    protected $staticRouteMap;
    protected $variableRouteData;
    protected $routeRoles;

    protected abstract function dispatchVariableRoute($routeData, $uri);

    public function dispatch($httpMethod, $uri, $roles) {
        $result = [];
        if (isset($this->staticRouteMap[$uri])) {
            $result =  $this->dispatchStaticRoute($httpMethod, $uri);
        } else {
            $varRouteData = $this->variableRouteData;
            if (isset($varRouteData[$httpMethod])) {
                $result = $this->dispatchVariableRoute($varRouteData[$httpMethod], $uri);
            } else if ($httpMethod === 'HEAD' && isset($varRouteData['GET'])) {
                $result = $this->dispatchVariableRoute($varRouteData['GET'], $uri);
            }
        }
        if (!empty($this->routeRoles[$result['handler']]) and empty(array_intersect($roles, $this->routeRoles[$result['handler']]))) {
            $result['match'] = self::USER_NOT_ALLOWED;
        }
        return $result;
    }

    protected function dispatchStaticRoute($httpMethod, $uri) {
        $routes = $this->staticRouteMap[$uri];
        if (isset($routes[$httpMethod])) {
            return ['match' => self::FOUND, 'handler' => $routes[$httpMethod], 'vars' => []];
        } elseif ($httpMethod === 'HEAD' && isset($routes['GET'])) {
            return ['match' => self::FOUND, 'handler' => $routes['GET'], 'vars' => []];
        } else {
            return [self::NOT_FOUND, 'handler' => null, 'vars' => []];
        }
    }
}
